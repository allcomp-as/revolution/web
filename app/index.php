<?php
// begin with bootstrap
require __DIR__ . '/bootstrap.php';


// moved header from www script
header('Content-Type: text/html; charset=utf-8');
define('ABS_PATH', 'http://localhost/dsc_revolution');

require_once DIR_CONFIG.'/index.php';

$_DICTIONARY = loadLocale(DIR_LOCALES.'/index', $_CONFIG['locale']);


// instantiate Nette Database Core
$db = dbNette($_CONFIG['dbhost'], $_CONFIG['dbuser'], $_CONFIG['dbpass'], $_CONFIG['dbname']);


// start session
session_start();
