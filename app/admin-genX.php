<?php
/*
expects following variables to be set
$db
$select
$from
$joins
$where
$orderDefault
$orderTable
$searchCols
*/

// get URL params
$limit_start = intVal($httpRequest->getQuery('limit_start', '0'));
$limit_count = intVal($httpRequest->getQuery('limit_count', '10'));
$sort = ($httpRequest->getQuery('sort', 'ASC') === 'ASC');
$order = $httpRequest->getQuery('order', $orderDefault);
$genCount = ($httpRequest->getQuery('gen_count') == "true");// intentionally ==
$search = $httpRequest->getQuery('search');

// handle selected columns
if($genCount){
	$select = 'COUNT(*)';
}

// begin with SQL query build
$query = [];
$query[0] = "
	SELECT
		$select
	FROM $from
	$joins
";

// process search parameter
if($search !== null){
	$search = '%' . $search . '%';
	
	foreach($searchCols as $col){
		$where[] = $dbWeb::literal("LOWER($col) LIKE LOWER(?)", $search);
	}
	
	$query[0] .= ' WHERE ?or';
	$query[] = $where;
}

// process order parameter
if(!$genCount){
	if(!in_array($order, array_keys($orderTable), true)){
		$order = $orderDefault;
	}
	
	$query[0] .= '
		ORDER BY
			?
	';
	$query[] = [
		$orderTable[$order] => $sort
	];
}

// process limiting
$query[0] .= '
	LIMIT ?, ?
';
$query[] = $limit_start;
$query[] = $limit_count;

// query DB
$rs = $db->query(...$query);

if($genCount){
	echo $rs->fetchField();
	return null;
}
else {
	return $rs;
}
