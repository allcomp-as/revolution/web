<?php
/* Directories */
define('DIR_APP', __DIR__ );
define('DIR_SYS', dirname(DIR_APP));

define('DIR_CONFIG', DIR_APP.'/config');
define('DIR_LOCALES', DIR_APP.'/locales');


/* Load Composer-maintained packages (including Nette) */
require_once DIR_SYS.'/vendor/autoload.php';


/* Create Nette HTTP Request for input sanitization */
$httpRequestFactory = new Nette\Http\RequestFactory;
$httpRequest = $httpRequestFactory->createHttpRequest();


/**
 * Shortcut for Nette DB instantiation
 */
function dbNette($host, $user, $pass, $dbname){
	return new Nette\Database\Connection(
		'mysql:host='.$host.';dbname='.$dbname,
		$user,
		$pass,
		[
			'charset' => 'utf8',
			'lazy' => 'yes'
		]
	);
} // dbNette


/**
 * Loads specified locale dictionary
 * @param $dir - directory with locales
 * @param $locale - locale to load
 * @param $baseLocale - master with texts, that should be available
 * @return array
 */
function loadLocale($dir, $locale, $baseLocale = 'cs_CZ'){
	$file = file_get_contents($dir . '/' . $baseLocale);
	$dict = json_decode($file, true);
	
	if(!file_exists($dir . '/' . $locale)){
		return $dict;
	}
	
	$file = file_get_contents($dir . '/' . $locale);
	$mainDict = json_decode($file, true);
	
	foreach($mainDict as $key => $val){
		$dict[$key] = $val;
	}
	
	return $dict;
} // loadLocale
