<?php
// begin with bootstrap
require __DIR__ . '/bootstrap.php';


// moved header from www script
header('Content-Type: text/html; charset=utf-8');

require_once DIR_CONFIG.'/admin.php';

$_DICTIONARY = loadLocale(DIR_LOCALES.'/admin', $_CONFIG['locale']);


// instantiate Nette Database Core
$dbServer = dbNette($_CONFIG['server_dbhost'], $_CONFIG['server_dbuser'], $_CONFIG['server_dbpass'], $_CONFIG['server_dbname']);
$dbWeb = dbNette($_CONFIG['web_dbhost'], $_CONFIG['web_dbuser'], $_CONFIG['web_dbpass'], $_CONFIG['web_dbname']);


// start session
session_start();
