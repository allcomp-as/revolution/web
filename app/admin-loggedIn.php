<?php
// begin with standard admin
require __DIR__ . '/admin.php';

// and check if user is logged in
isset($_SESSION["userid"]) or die("Access denied!");
