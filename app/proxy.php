<?php
// disable default Nette URL filters - the SHS supports double slash in path as indication for no JS callback
$httpRequestFactory->urlFilters['path'] = [];
$httpRequest = $httpRequestFactory->createHttpRequest();

// parse URL
$url = $httpRequest->getUrl();
$url = $url->getPathInfo() . (($tmp = $url->getQuery()) ? '?'.$tmp : '');
$url = substr($url, 9);// cut 'proxy.php' from beginning
$url = 'http://' . $_CONFIG['ip'] . ':' . $_CONFIG['port'] . $url;

// prepare HTTP request options
$opts = [
	'http' => [
		'max_redirects' => '0',
		'ignore_errors' => '1',
		'protocol_version' => 1.1,
		'method' => $httpRequest->getMethod(),
		'header' => [],
		'content' => $httpRequest->getRawBody()
	]
];

// transform headers
foreach($httpRequest->getHeaders() as $name => $val){
	if(!in_array($name, ['host', 'connection', 'cookie'], true)){
		$opts['http']['header'][] = Nette\Utils\Strings::capitalize($name) . ': ' . $val;
	}
}

$opts['http']['header'][] = 'Connection: close';

// perform HTTP request
$context = stream_context_create($opts);
$stream = fopen($url, 'r', false, $context);
$response_meta = stream_get_meta_data($stream);
$response = stream_get_contents($stream);
fclose($stream);

// handle errors
if($response_meta === null){
	header('HTTP/1.1 503 SHS Unreachable or Not Responding');
	exit;
}

// return response
foreach($response_meta['wrapper_data'] as $h){
	if(strncmp($h, 'Content-Length:', 15) !== 0){
		header($h);
	}
}

echo $response;
