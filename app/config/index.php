<?php
$_CONFIG['ip'] = 'localhost';
$_CONFIG['port'] = "81";

$_CONFIG['default_permission_level'] = 50;
$_CONFIG['require_login'] = false;
$_CONFIG['not_require_login_in_LAN'] = true;
$_CONFIG['cameras_source'] = "";

$_CONFIG['dbhost'] = "localhost";
$_CONFIG['dbuser'] = "revolution_web";
$_CONFIG['dbpass'] = "rNJ0GcCi1t2z";
$_CONFIG['dbname'] = "revolution_smart_control";

$_CONFIG['design'] = "green";

$_CONFIG['lastUsedControlsLimit'] = 10;

$_CONFIG['locale'] = "en_US_sample";

$_CONFIG['hide_rooms'] = false;
$_CONFIG['hide_macros'] = false;
$_CONFIG['hide_security'] = true;
$_CONFIG['hide_cameras'] = true;
$_CONFIG['hide_holidays'] = true;
