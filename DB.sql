SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

CREATE DATABASE `revolution_smart_control` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `revolution_smart_control`;

CREATE TABLE `controls` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `room` int(8) NOT NULL,
  `name` text COLLATE utf8_czech_ci NOT NULL,
  `outputs` text COLLATE utf8_czech_ci NOT NULL,
  `type` int(8) NOT NULL,
  `icon` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `rank` int(8) NOT NULL DEFAULT '0',
  `last_time_usage` bigint(64) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


CREATE TABLE `macros` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_bin NOT NULL,
  `command` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


CREATE TABLE `rfid_cards` (
  `id` int(16) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(16) NOT NULL DEFAULT '0',
  `rfid1` bigint(64) NOT NULL,
  `rfid2` bigint(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `rooms` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_czech_ci NOT NULL,
  `floor` int(8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


CREATE TABLE `security_systems` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


CREATE TABLE `users` (
  `id` int(16) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) COLLATE utf8_bin NOT NULL,
  `password` char(32) COLLATE utf8_bin NOT NULL,
  `permission_level` int(16) NOT NULL DEFAULT 0,
  `last_ip` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
