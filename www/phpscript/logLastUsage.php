<?php
// begin with standard startup
require_once __DIR__ . '/../../app/index.php';

// and check if user is logged-in
if(!isset($_SESSION["authorized"]) || !$_SESSION["authorized"]){
	die("Access denied!");
}

$db->query(
	"UPDATE controls SET", [
		'last_time_usage' => $httpRequest->getQuery('time')
	],
	"WHERE id=?", $httpRequest->getQuery('id')
);
