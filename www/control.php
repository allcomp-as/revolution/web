<?php
require __DIR__ . '/../app/index.php';

if(!isset($_SESSION["authorized"]) || !$_SESSION["authorized"]){
	header('Location: ' . $httpRequest->url->baseUrl, true, 302);
	exit;
}
?>

<!DOCTYPE html>
<html>
<head>
	<title><?=$_DICTIONARY["title"]?></title>
	<meta charset="utf-8" />
	<script src="jslib/jquery-2.1.4.min.js"></script>
	<script src="jslib/jquery-ui.min.js"></script>
	<script src="jslib/Room.js"></script>
	<script src="jslib/Control.js"></script>
	<script src="jslib/Macro.js"></script>
	<script src="jslib/DataStore.js"></script>
	<script>DataStore.design = "<?=$_CONFIG['design']?>";</script>
	<script>
		var _DICTIONARY = {};
		<?php foreach($_DICTIONARY as $key => $value) :?>
			_DICTIONARY["<?=$key?>"] = "<?=$value?>";
		<?php endforeach; ?>
	</script>
	<link rel="stylesheet" type="text/css" href="css/<?=$_CONFIG['design']?>/main.css" />
	<link rel="stylesheet" type="text/css" href="css/<?=$_CONFIG['design']?>/switches.css" />
	<link rel="stylesheet" type="text/css" href="css/<?=$_CONFIG['design']?>/inputsRange.css" />
	<link rel="stylesheet" type="text/css" href="css/<?=$_CONFIG['design']?>/radioButtons.css" />
	<link rel="stylesheet" type="text/css" href="css/jquery-ui.min.css" />
	<style>
		.ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active, a.ui-button:active, .ui-button:active, .ui-button.ui-state-active:hover {
			background: #2ecc71;
			border: 1px solid #27ae60;
		}
		.ui-state-highlight, .ui-widget-content .ui-state-highlight, .ui-widget-header .ui-state-highlight {
			background: #4D4E4F;
			border: 1px solid #292929;
			color: #E3E3E3;
			font-weight: bold;
		}
	</style>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<script>
<?php
$rsRooms = $db->query("SELECT id, name, floor FROM rooms ORDER BY name ASC");
foreach($rsRooms as $row){
	echo "		DataStore.addRoom(new Room($row[0], '$row[1]', $row[2]));\n";
};

$rsControls = $db->query("SELECT id, room, name, outputs, type, icon FROM controls");
foreach($rsControls as $row){
	echo "		DataStore.addControl(new Control($row[0], $row[1], '$row[2]', '$row[3]', $row[4], '$row[5]'));\n";
};

$rsMacros = $db->query("SELECT id, name, command FROM macros");
foreach($rsMacros as $row){
	echo "		DataStore.addMacro(new Macro($row[0], '$row[1]', '$row[2]'));\n";
};

$rsControls2 = $db->query("SELECT id, room, name, outputs, type, icon FROM controls ORDER BY last_time_usage DESC LIMIT 0," . $_CONFIG['lastUsedControlsLimit']);
foreach($rsControls2 as $row){
	echo "		DataStore.addLastUsedControl(new Control($row[0], $row[1], '$row[2]', '$row[3]', $row[4], '$row[5]'));\n";
};
?>
	</script>
	<script>
		var CAMERAS_SOURCE = "<?=$_CONFIG['cameras_source']?>";
	</script>
	<script src="jslib/control.php.js"></script>
	
	<!-- load modules -->
	<script src="jslib/jqColorPicker.min.js"></script>
</head>
<body>
        <div id="navigationBar">
		<div class="goBack" onclick="goBack()"><img src="res/<?=$_CONFIG['design']?>/icons/Reply%20All%20Arrow%20Filled-50.png" /></div>
		<div class="location"><span>...</span></div>
		<div class="goHome" onclick="loadMainMenu()"><img src="res/<?=$_CONFIG['design']?>/icons/Home%20Filled-50.png" /></div>
        </div>
        <div class="mainMenu">
			<div class="item" onclick="loadRooms()"<?php if($_CONFIG['hide_rooms']) echo(" style=\"display: none;\""); ?>>
				<div class="img"><img src="res/<?=$_CONFIG['design']?>/icons/Room%20Filled-100.png" /></div>
				<div class="label"><?=$_DICTIONARY["rooms"]?></div>
			</div>
			<div class="item" onclick="loadMacros()"<?php if($_CONFIG['hide_macros']) echo(" style=\"display: none;\""); ?>>
				<div class="img"><img src="res/<?=$_CONFIG['design']?>/icons/TV%20Filled-100.png" /></div>
				<div class="label"><?=$_DICTIONARY["macros"]?></div>
			</div>
			<div class="item" onclick="loadSecuritySystem()"<?php if($_CONFIG['hide_security']) echo(" style=\"display: none;\""); ?>>
				<div class="img"><img src="res/<?=$_CONFIG['design']?>/icons/Web%20Shield%20Filled-100.png" /></div>
				<div class="label"><?=$_DICTIONARY["security"]?></div>
			</div>
			<div class="item" onclick="loadSecurityCameras()"<?php if($_CONFIG['hide_cameras']) echo(" style=\"display: none;\""); ?>>
				<div class="img"><img src="res/<?=$_CONFIG['design']?>/icons/Bullet%20Camera%20Filled-100.png" /></div>
				<div class="label"><?=$_DICTIONARY["cameras"]?></div>
			</div>
			<div class="item" onclick="loadSimulator()"<?php if($_CONFIG['hide_holidays']) echo(" style=\"display: none;\""); ?>>
				<div class="img"><img src="res/<?=$_CONFIG['design']?>/icons/Beach%20Filled-100.png" /></div>
				<div class="label"><?=$_DICTIONARY["holidays"]?></div>
		</div>
        </div>
	<div class="lastUsedControls"></div>
        <div class="rooms"></div>
        <div class="macros"></div>
        <div class="roomControls"></div>
	<div class="securityCameras"><iframe src=""></iframe></div>
	<div class="simulator">
		<table>
			<tr>
				<td><?=$_DICTIONARY["dtm_start"]?>:</td>
				<td style="text-align: right;"><input type="text" id="startdatepicker" style="text-align: right;" class="date" readonly /></td>
			</tr>
			<tr>
				<td><?=$_DICTIONARY["dtm_end"]?>:</td>
				<td style="text-align: right;"><input type="text" id="enddatepicker" style="text-align: right;" class="date" readonly /></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td style="text-align: right;"><button id="btnlastweek" class="macrobtn" style="margin-top: 10px; margin-bottom: 20px; margin-right: 5px;"><?=$_DICTIONARY["last_week"]?></button></td>
			</tr>
		</table>
		<table>
			<tr>
				<td style="width: 60%;"><?=$_DICTIONARY["simulation"]?></td>
				<td style="text-align: right; width: 40%;">
					<div style="text-align: right; transform: translate(9px,0);">
						<label>
						<input type="checkbox" class="ios-switch" value="0" id="switchEnableSimulator" />
						<div class="switch"></div>
						</label>
					</div>
				</td>
			</tr>
		</table>
		<script>
			$(function() {
				$("#startdatepicker").datepicker({
					firstDay: 1,
					monthNames: [
						_DICTIONARY["mon_january"],
						_DICTIONARY["mon_february"],
						_DICTIONARY["mon_march"],
						_DICTIONARY["mon_april"],
						_DICTIONARY["mon_may"],
						_DICTIONARY["mon_june"],
						_DICTIONARY["mon_july"],
						_DICTIONARY["mon_august"],
						_DICTIONARY["mon_september"],
						_DICTIONARY["mon_october"],
						_DICTIONARY["mon_november"],
						_DICTIONARY["mon_december"]
					],
					dateFormat: "dd.mm.yy",
					dayNamesMin: [
						_DICTIONARY["daysc_sunday"],
						_DICTIONARY["daysc_monday"],
						_DICTIONARY["daysc_tuesday"],
						_DICTIONARY["daysc_wednesday"],
						_DICTIONARY["daysc_thursday"],
						_DICTIONARY["daysc_friday"],
						_DICTIONARY["daysc_saturday"]
					],
					duration: "slow"
				});
				$("#startdatepicker").val(_DICTIONARY["set_date"]);
				$("#enddatepicker").datepicker({
					firstDay: 1,
					monthNames: [
						_DICTIONARY["mon_january"],
						_DICTIONARY["mon_february"],
						_DICTIONARY["mon_march"],
						_DICTIONARY["mon_april"],
						_DICTIONARY["mon_may"],
						_DICTIONARY["mon_june"],
						_DICTIONARY["mon_july"],
						_DICTIONARY["mon_august"],
						_DICTIONARY["mon_september"],
						_DICTIONARY["mon_october"],
						_DICTIONARY["mon_november"],
						_DICTIONARY["mon_december"]
					],
					dateFormat: "dd.mm.yy",
					dayNamesMin: [
						_DICTIONARY["daysc_sunday"],
						_DICTIONARY["daysc_monday"],
						_DICTIONARY["daysc_tuesday"],
						_DICTIONARY["daysc_wednesday"],
						_DICTIONARY["daysc_thursday"],
						_DICTIONARY["daysc_friday"],
						_DICTIONARY["daysc_saturday"]
					],
					duration: "slow"
				});
				$("#enddatepicker").val(_DICTIONARY["set_date"]);
				$("#btnlastweek").click(function() {
					var dateBefore = new Date();
					dateBefore.setDate(dateBefore.getDate() - 7);
					var dateNow = new Date();
					$("#startdatepicker").val($.datepicker.formatDate('dd.mm.yy', dateBefore));
					$("#enddatepicker").val($.datepicker.formatDate('dd.mm.yy', dateNow));
				});
				$("#switchEnableSimulator").click(function(event) {
					event.preventDefault();
					if(this.checked) {
						//turn on simulator
						var startDate=$("#startdatepicker").val();
						startDate=startDate.split(".");
						var newStartDate=startDate[1]+"/"+startDate[0]+"/"+startDate[2];
						var startDayInt = new Date(newStartDate).getTime();
						var endDate=$("#enddatepicker").val();
						endDate=endDate.split(".");
						var newEndDate=endDate[1]+"/"+endDate[0]+"/"+endDate[2];
						var endDayInt = new Date(newEndDate).getTime();
						if(isNaN(startDayInt)) {
							notification(_DICTIONARY["dtm_start_invalid"]);
							return;
						}
						if(isNaN(endDayInt)) {
							notification(_DICTIONARY["dtm_end_invalid"]);
							return;
						}
						requestSimulatorStart(startDayInt, endDayInt);
					} else {
						//turn off simulator
						requestSimulatorStop();
					}
				});
			});
		</script>
	</div>
        <div class="security">
		<div class="state"></div>
		<div class="numkeyboard"><!--
			--><div class="value"><span>&nbsp;</span><div><!--
				--><button>7</button><!--
				--><button>8</button><!--
				--><button>9</button><!--
				--><button>4</button><!--
				--><button>5</button><!--
				--><button>6</button><!--
				--><button>1</button><!--
				--><button>2</button><!--
				--><button>3</button><!--
				--><button class="activate" style="visibility: hidden;">&nbsp;</button><!--
				--><button>0</button><!--
				--><button style="background-color: #a3a3a3;">&#8592;</button><!--
				--></div>
			</div>
		</div>
		<div class="securityitems">
			<?php $rsSecItems = $db->fetchAll("SELECT * FROM security_systems"); ?>
			<?php foreach($rsSecItems as $row){ ?>
			<div class="sitem sitemred" data-sid="<?=($row[0]) ?>"><?=($row[1]) ?></div>
			<script>securitySystems.push(<?=($row[0]) ?>);</script>
			<?php }; ?>
		</div>
		<div class="activesecurityinputs">
			<div class="title"><img src="res/<?=$_CONFIG['design']?>/icons/Warning%20Shield%20Filled-50.png" /></div>
			<?php foreach($rsSecItems as $row){ ?>
			<div class="securitygroup" data-sid="<?=($row[0]) ?>">
				<div class="securitytitle"><?=($row[1]) ?></div>
				<div class="inputs">
				</div>
			</div>
			<?php }; ?>
		</div>
	</div>
</body>
</html>
