<?php
require __DIR__ . '/../app/index.php';


// check if user is logged in
if(!isset($_SESSION["authorized"]) || !$_SESSION["authorized"]){
	header('HTTP/1.1 401 Unauthorized');
	exit;
}


require __DIR__ . '/../app/proxy.php';
