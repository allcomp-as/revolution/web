/**
 * Created by Samuel on 13.08.2015.
 */

/*
Control types:
	0 	->	switch
	1	->	thermostat
	2	->	slider
	3	->	upDownStopSwitch
	4	->	upDownStopSwitchWithPulseButtons
	5	->	switchWithIndicator
*/

var Control = function(id, room, name, outputs, type, icon) {
    this.id = id;
    this.room = room;
    this.name = name;
    this.outputs = outputs.split(",");
    this.type = type;
    this.icon = icon;
};
