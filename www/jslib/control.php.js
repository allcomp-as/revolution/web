var currentMenu = 0;
var rangeUpdateCounter = -1;
var securitySystems = [];

/* String decoder */
var decoderMap = {};
decoderMap["?chid=0?"] = "á";
decoderMap["?chid=0L?"] = "á";
decoderMap["?chid=1?"] = "č";
decoderMap["?chid=1L?"] = "Č";
decoderMap["?chid=2?"] = "ď";
decoderMap["?chid=2L?"] = "Ď";
decoderMap["?chid=3?"] = "ě";
decoderMap["?chid=3L?"] = "Ě";
decoderMap["?chid=4?"] = "é";
decoderMap["?chid=4L?"] = "É";
decoderMap["?chid=5?"] = "í";
decoderMap["?chid=5L?"] = "Í";
decoderMap["?chid=6?"] = "ň";
decoderMap["?chid=6L?"] = "Ň";
decoderMap["?chid=7?"] = "ó";
decoderMap["?chid=7L?"] = "Ó";
decoderMap["?chid=8?"] = "ř";
decoderMap["?chid=8L?"] = "Ř";
decoderMap["?chid=9?"] = "š";
decoderMap["?chid=9L?"] = "Š";
decoderMap["?chid=10?"] = "ť";
decoderMap["?chid=10L?"] = "Ť";
decoderMap["?chid=11?"] = "ú";
decoderMap["?chid=11L?"] = "Ú";
decoderMap["?chid=12?"] = "ů";
decoderMap["?chid=12L?"] = "Ů";
decoderMap["?chid=13?"] = "ž";
decoderMap["?chid=13L?"] = "Ž";
decoderMap["?chid=14?"] = "ý";
decoderMap["?chid=14L?"] = "Ý";

function serverRequest(callbackName, request) {
    var script = document.createElement('script');
    script.src = 'proxy.php/' + callbackName + '/' + request;
    var bodyelem = document.getElementsByTagName('head')[0];
    bodyelem.appendChild(script);
    setTimeout(function() {
        bodyelem.removeChild(script);
    }, 2000);
}

function wstr_decode(str) {
    for(var i = 0; i < 10; i++)
        for(var key in decoderMap)
            str = str.replace(key, decoderMap[key]);
    return str;
}
/*-------------------------*/

function RGBtoRGBW(RGB, lum) {
    var min_col = Math.min(RGB.R, RGB.G, RGB.B);
    var W = min_col*3;
    var R = RGB.R - min_col;
    var G = RGB.G - min_col;
    var B = RGB.B - min_col;

    var min_lum = Math.min(lum.R, lum.G, lum.B, lum.W);
    var oW = W * min_lum / lum.W;
    var oR = R * min_lum / lum.R;
    var oG = G * min_lum / lum.G
    var oB = B * min_lum / lum.B;

    if(oW > 255) oW = 255;
    if(oR > 255) oR = 255;
    if(oG > 255) oG = 255;
    if(oB > 255) oB = 255;

    return {
        R: Math.floor(oR),
        G: Math.floor(oG),
        B: Math.floor(oB),
        W: Math.floor(oW)
    };
}

function RGBWtoRGB(RGBW, lum) {
    var min_lum = Math.min(lum.R, lum.G, lum.B, lum.W);
    var iW = RGBW.W * lum.W / min_lum;
    var iR = RGBW.R * lum.R / min_lum;
    var iG = RGBW.G * lum.G / min_lum;
    var iB = RGBW.B * lum.B / min_lum;

    var min_col = iW / 3;
    var R = iR + min_col;
    var G = iG + min_col;
    var B = iB + min_col;

    return {
        R: Math.floor(R),
        G: Math.floor(G),
        B: Math.floor(B)
    };
}

function RGBtoRGB(RGB, lum) { //returns corrected RGB
    var min_lum = Math.min(lum.R, lum.G, lum.B);
    var R = RGB.R * lum.R / min_lum;
    var G = RGB.G * lum.G / min_lum;
    var B = RGB.B * lum.B / min_lum;

    if(R > 255) R = 255;
    if(G > 255) G = 255;
    if(B > 255) B = 255;

    return {
        R: Math.floor(R),
        G: Math.floor(G),
        B: Math.floor(B)
    };
}

function RGBfromRGB(RGB, lum) {
    var min_lum = Math.min(lum.R, lum.G, lum.B);
    var R = RGB.R * min_lum / lum.R;
    var G = RGB.G * min_lum / lum.G;
    var B = RGB.B * min_lum / lum.B;

    return {
        R: Math.floor(R),
        G: Math.floor(G),
        B: Math.floor(B)
    };
}

function requestUpdateOutputs() {
    var outputString = "";

    var roomControlItemElems = $("div.roomControls div.item div.controls input").toArray();

    for(var i = 0; i < roomControlItemElems.length; i++) {
        var input = $(roomControlItemElems[i]);
        /*if($(input).attr("type") == "range") {
            rangeUpdateCounter++;
            if(rangeUpdateCounter > 5)
                rangeUpdateCounter = 0;
            if(rangeUpdateCounter != 0)
                continue;
        }*/

        if($(input).attr("type") == "radio") {
            var inputs = $(input).parent().parent().find("div.radioContainer").toArray();
            if(inputs.length == 3) {
                var out1 = $(inputs[0]).attr("data-output");
                var out2 = $(inputs[2]).attr("data-output");
                outputString += "-" + out1 + "-" + out2;
            }
            continue;
        }
        if($(input).attr("type") == "number")
            continue;
        if($(input).parent().parent().attr("class") == "tempactive")
            continue;
        var output = $(input).attr("data-output");
        outputString += "-" + output;
    }

    var RGBelems = $("div.roomControls div.item div.controls div.rgb-PWM").toArray();
    for(var i = 0; i < RGBelems.length; i++) {
        var input = $(RGBelems[i]);
        outputString += "-" + input.data("r-color");
        outputString += "-" + input.data("g-color");
        outputString += "-" + input.data("b-color");
    }

    var RGBWelems = $("div.roomControls div.item div.controls div.rgbw-PWM").toArray();
    for(var i = 0; i < RGBWelems.length; i++) {
        var input = $(RGBWelems[i]);
        outputString += "-" + input.data("r-color");
        outputString += "-" + input.data("g-color");
        outputString += "-" + input.data("b-color");
        outputString += "-" + input.data("w-color");
    }

    if(outputString == "")
        return;

    outputString = outputString.substr(1);
    serverRequest("updateOutputs", "states/" + outputString);
}

function requestUpdateOutputsLastUsed() {
    var outputString = "";

    var roomControlItemElems = $("div.lastUsedControls div.item div.controls input").toArray();

    for(var i = 0; i < roomControlItemElems.length; i++) {
        var input = $(roomControlItemElems[i]);
        /*if($(input).attr("type") == "range") {
            rangeUpdateCounter++;
            if(rangeUpdateCounter > 5)
                rangeUpdateCounter = 0;
            if(rangeUpdateCounter != 0)
                continue;
        }*/
        if($(input).attr("type") == "radio") {
            var inputs = $(input).parent().parent().find("div.radioContainer").toArray();
            if(inputs.length == 3) {
                var out1 = $(inputs[0]).attr("data-output");
                var out2 = $(inputs[2]).attr("data-output");
                outputString += "-" + out1 + "-" + out2;
            }
            continue;
        }
        if($(input).attr("type") == "number")
            continue;
        if($(input).parent().parent().attr("class") == "tempactive")
            continue;
        var output = $(input).attr("data-output");
        outputString += "-" + output;
    }

    var RGBelems = $("div.lastUsedControls div.item div.controls div.rgb-PWM").toArray();
    for(var i = 0; i < RGBelems.length; i++) {
        var input = $(RGBelems[i]);
        outputString += "-" + input.data("r-color");
        outputString += "-" + input.data("g-color");
        outputString += "-" + input.data("b-color");
    }

    var RGBWelems = $("div.lastUsedControls div.item div.controls div.rgbw-PWM").toArray();
    for(var i = 0; i < RGBWelems.length; i++) {
        var input = $(RGBWelems[i]);
        outputString += "-" + input.data("r-color");
        outputString += "-" + input.data("g-color");
        outputString += "-" + input.data("b-color");
        outputString += "-" + input.data("w-color");
    }

    if(outputString == "")
        return;

    outputString = outputString.substr(1);
    serverRequest("updateOutputs", "states/" + outputString);
}

function requestUpdateControlSources() {
    var outputString = "";

    var roomControlItemElems = $("div.roomControls div.item div.controls label input").toArray();

    for(var i = 0; i < roomControlItemElems.length; i++) {
        var input = $(roomControlItemElems[i]);
        var output = $(input).attr("data-output");
        outputString += "-" + output;
    }

    if(outputString == "")
        return;

    outputString = outputString.substr(1);
    serverRequest("updateControlSources", "controlsources/" + outputString);
}

function requestUpdateControlSourcesLastUsed() {
    var outputString = "";

    var roomControlItemElems = $("div.lastUsedControls div.item div.controls label input").toArray();

    for(var i = 0; i < roomControlItemElems.length; i++) {
        var input = $(roomControlItemElems[i]);
        var output = $(input).attr("data-output");
        outputString += "-" + output;
    }

    if(outputString == "")
        return;

    outputString = outputString.substr(1);
    serverRequest("updateControlSources", "controlsources/" + outputString);
}

function requestUpdateIndicators() {
    var outputString = "";

    var roomControlItemElems = $("div.roomControls div.item div.controls div.garageDoorsWithIndicator").toArray();

    for(var i = 0; i < roomControlItemElems.length; i++) {
        var input = $(roomControlItemElems[i]);
        var outputs = $(input).attr("data-output").split(",");
        outputString += "-" + outputs[1];
    }

    var roomControlItemElems2 = $("div.roomControls div.item div.controls div.indicator").toArray();

    for(var i = 0; i < roomControlItemElems2.length; i++) {
        var input = $(roomControlItemElems2[i]);
        var output = $(input).find("span").attr("data-output");
        outputString += "-" + output;
    }

    if(outputString == "")
        return;

    outputString = outputString.substr(1);
    serverRequest("updateIndicators", "states/" + outputString);
}

function requestUpdateIndicatorsLastUsed() {
    var outputString = "";

    var roomControlItemElems = $("div.lastUsedControls div.item div.controls div.garageDoorsWithIndicator").toArray();

    for(var i = 0; i < roomControlItemElems.length; i++) {
        var input = $(roomControlItemElems[i]);
        var outputs = $(input).attr("data-output").split(",");
        outputString += "-" + outputs[1];
    }

    var roomControlItemElems2 = $("div.lastUsedControls div.item div.controls div.indicator").toArray();

    for(var i = 0; i < roomControlItemElems2.length; i++) {
        var input = $(roomControlItemElems2[i]);
        var output = $(input).find("span").attr("data-output");
        outputString += "-" + output;
    }

    if(outputString == "")
        return;

    outputString = outputString.substr(1);
    serverRequest("updateIndicators", "states/" + outputString);
}

function requestUpdateActiveTherms() {
    var outputString = "";

    var roomControlItemElems = $("div.roomControls div.item div.controls input").toArray();

    for(var i = 0; i < roomControlItemElems.length; i++) {
        var input = $(roomControlItemElems[i]);

        if($(input).parent().parent().attr("class") == "tempactive") {
            if($(input).attr("type") == "checkbox") {
                var output = $(input).attr("data-output");
                outputString += "-" + output;
            }
        }
    }

    outputString = outputString.substr(1);

    if(outputString == "")
        return;

    serverRequest("updateActiveTherms", "thermsactiveget/" + outputString);
}

function requestUpdateActiveThermsLastUsed() {
    var outputString = "";

    var roomControlItemElems = $("div.lastUsedControls div.item div.controls input").toArray();

    for(var i = 0; i < roomControlItemElems.length; i++) {
        var input = $(roomControlItemElems[i]);

        if($(input).parent().parent().attr("class") == "tempactive") {
            if($(input).attr("type") == "checkbox") {
                var output = $(input).attr("data-output");
                outputString += "-" + output;
            }
        }
    }

    outputString = outputString.substr(1);

    if(outputString == "")
        return;

    serverRequest("updateActiveTherms", "thermsactiveget/" + outputString);
}

function requestUpdateThermsTarget() {
    var outputString = "";

    var roomControlItemElems = $("div.roomControls div.item div.controls input").toArray();

    for(var i = 0; i < roomControlItemElems.length; i++) {
        var input = $(roomControlItemElems[i]);

        if($(input).parent().attr("class") == "tempactive") {
            if($(input).attr("type") == "number") {
                var output = $(input).attr("data-output");
                outputString += "-" + output;
            }
        }
    }

    outputString = outputString.substr(1);

    if(outputString == "")
        return;

    serverRequest("updateThermsTarget", "thermsget/" + outputString);
}

function requestUpdateThermsTargetLastUsed() {
    var outputString = "";

    var roomControlItemElems = $("div.lastUsedControls div.item div.controls input").toArray();

    for(var i = 0; i < roomControlItemElems.length; i++) {
        var input = $(roomControlItemElems[i]);

        if($(input).parent().attr("class") == "tempactive") {
            if($(input).attr("type") == "number") {
                var output = $(input).attr("data-output");
                outputString += "-" + output;
            }
        }
    }

    outputString = outputString.substr(1);

    if(outputString == "")
        return;

    serverRequest("updateThermsTarget", "thermsget/" + outputString);
}

function requestSecurityEnable(code,sid) {
    serverRequest("securityEnableResult", "securityenable/" +  code + "/" + sid);
}

function requestSecurityDisable(code,sid) {
    serverRequest("securityDisableResult", "securitydisable/" +  code + "/" + sid);
}

function requestSecurityStates() {
    if(currentMenu != 4)
        return;

    serverRequest("securityStatesResult", "securitystatesget");
}

function requestOutputChange(output, value) {
    serverRequest("voidCatch", "output/" + output + "/" + value);
}

function requestOutputsChange(values) {
    serverRequest("voidCatch", "outputs/" + values);
}

function requestPulsingStart(output, frequency, dutyCycle) {
    serverRequest("voidCatch", "startpulsing/" + output + "/" + frequency + "/" + dutyCycle);
}

function requestPulsingStop(output) {
    serverRequest("voidCatch", "stoppulsing/" + output);
}

function requestThermActiveChange(output, value) {
    serverRequest("voidCatch", "thermactive/" + output + "/" + value);
}

function requestThermTargetChange(output, value) {
    serverRequest("voidCatch", "thermset/" + output + "/" + value);
}

function requestSecurityInputs() {
    serverRequest("genSecurityInputs", "securitytriggersget");
}

function requestActiveSecurityInputs() {
    serverRequest("updateActiveSecurityInputs", "securityactivetriggersget");
}

function requestUpdateTemperatures() {
    var outputString = "";
    var roomControlItemElems = $("div.roomControls div.item div.controls div.tempval").toArray();

    for(var i = 0; i < roomControlItemElems.length; i++) {
        var divValue = $(roomControlItemElems[i]);
        var output = $(divValue).attr("data-output");
        outputString += "-" + output;
    }

    outputString = outputString.substr(1);

    if(outputString == "")
        return;

    serverRequest("updateTemperatures", "temps/" + outputString);
}

function requestUpdateTemperaturesLastUsed() {
    var outputString = "";
    var roomControlItemElems = $("div.lastUsedControls div.item div.controls div.tempval").toArray();

    for(var i = 0; i < roomControlItemElems.length; i++) {
        var divValue = $(roomControlItemElems[i]);
        var output = $(divValue).attr("data-output");
        outputString += "-" + output;
    }

    outputString = outputString.substr(1);

    if(outputString == "")
        return;

    serverRequest("updateTemperatures", "temps/" + outputString);
}

function requestSimulatorStop() {
    serverRequest("voidCatch", "simulationstop");
}

function requestSimulatorStart(startTime,endTime) {
    serverRequest("voidCatch", "simulationstart" + startTime + "/" + endTime);
}

function requestSimulatorState() {
    serverRequest("updateSimulatorState", "simulationstate");
}

function requestSimulatorTimes() {
    serverRequest("updateSimulatorTimes", "simulationgettimes");
}

function updateSimulatorState(res) {
    if(res=='1') {
        $("div.simulator input#switchEnableSimulator").prop("checked", true);
        requestSimulatorTimes();
    } else
        $("div.simulator input#switchEnableSimulator").prop("checked", false);
}

function updateSimulatorTimes(res) {
    if(res.indexOf(",,") > -1) {
        var times = res.split(",,");
        $("#startdatepicker").val($.datepicker.formatDate('dd.mm.yy', new Date(Number(times[0]))));
        $("#enddatepicker").val($.datepicker.formatDate('dd.mm.yy', new Date(Number(times[1]))));
    }
}

function updateTemperatures(res) {
    var pairs = res.split("-");
    for(var i = 0; i < pairs.length; i++) {
        var pair = pairs[i].split(":");
        var divsValue = $("div.roomControls div.item div.controls div.tempval[data-output='" + pair[0] +"']").toArray();
        if(divsValue.length == 1)
            divsValue[0].innerHTML = (parseFloat(pair[1]).toFixed(1)) + " °C";
        var divsValue2 = $("div.lastUsedControls div.item div.controls div.tempval[data-output='" + pair[0] +"']").toArray();
        if(divsValue2.length == 1)
            divsValue2[0].innerHTML = (parseFloat(pair[1]).toFixed(1)) + " °C";
    }
}

function genSecurityInputs(res) {
    var systemStrings = res.split("?#system?");
    for(var i = 0; i < systemStrings.length; i++) {
        var system = systemStrings[i].split("?#scon?");
        var sid = system[0];
        var inputs = system[1].split("?#ins?");
        var inputGroup = $("div.security div.activesecurityinputs div.securitygroup[data-sid='" + sid +"'] div.inputs");
        inputGroup.parent().css("display","none");
        inputGroup.html("");
        for(var j = 0; j < inputs.length; j++) {
            var input = inputs[j].split("?#inv?");
            var inputId = input[0];
            var inputDesc = wstr_decode(input[1]);
            inputGroup.append("<div class='securityinput' style='display: none;' data-swid='" + inputId +"'>[<b>" + inputId +"</b>] " + inputDesc +"</div>");
        }
    }
}

function updateActiveSecurityInputs(res) {
    var systemStrings = res.split("&");
    var inputCounter = 0;
    for(var i = 0; i < systemStrings.length; i++) {
        var system = systemStrings[i].split(":");
        var sid = system[0];
        var inputGroup = $("div.security div.activesecurityinputs div.securitygroup[data-sid='" + sid +"'] div.inputs");
        var activeInputs = system[1].split("-");
        if(system[1] != "")
            inputGroup.parent().css("display","block");
        else
            inputGroup.parent().css("display","none");
        inputGroup.children().css("display","none");
        if(system[1] != "")
            inputCounter++;
        for(var j = 0; j < activeInputs.length; j++)
            inputGroup.find("div.securityinput[data-swid='"+activeInputs[j]+"']").css("display","inline-block");
    }
    if(inputCounter > 0) {
        $("div.security div.activesecurityinputs").css("display","block");
        $("div.security div.securityitems").css("margin-bottom","0");
    } else {
        $("div.security div.activesecurityinputs").css("display","none");
        $("div.security div.securityitems").css("margin-bottom","30px");
    }
}

function voidCatch(c) {}

function securityStatesResult(res) {
    var pairs = res.split("-");
    for(var i = 0; i < pairs.length; i++) {
        var pair = pairs[i].split(":");
        if(parseInt(pair[1])==1 || parseInt(pair[1])==2) {
            var item = $("div.security div.securityitems").find("[data-sid='" + pair[0] + "']");
            if(!item.hasClass("sitemred"))
                item.addClass("sitemred");
        } else {
            var item = $("div.security div.securityitems").find("[data-sid='" + pair[0] + "']");
            if(item.hasClass("sitemred"))
                item.removeClass("sitemred");
        }
    }
}

function securityEnableResult(res) {
    if(res=="$OK")
        notification(_DICTIONARY["activ_succ"]);
    else
        notification(_DICTIONARY["activ_fault"]);
}

function securityDisableResult(res) {
    if(res=="$OK")
        notification(_DICTIONARY["deactiv_succ"]);
    else
        notification(_DICTIONARY["deactiv_fault"]);
}

function updateOutputs(res) {
    var pairs = res.split(" ");

    var RGBs = [];
    var RGBelems = $("div.roomControls div.item div.controls div.rgb-PWM").toArray();
    for(var i = 0; i < RGBelems.length; i++) {
        $elm = $(RGBelems[i]);
        RGBs.push({
            elm: $elm,
            Rout: $elm.data("r-color"),
            Gout: $elm.data("g-color"),
            Bout: $elm.data("b-color"),
            R: 0, G: 0, B: 0
        });
    }

    var RGBWs = [];
    var RGBWelems = $("div.roomControls div.item div.controls div.rgbw-PWM").toArray();
    for(var i = 0; i < RGBWelems.length; i++) {
        $elm = $(RGBWelems[i]);
        RGBWs.push({
            elm: $elm,
            Rout: $elm.data("r-color"),
            Gout: $elm.data("g-color"),
            Bout: $elm.data("b-color"),
            Wout: $elm.data("w-color"),
            R: 0, G: 0, B: 0, W: 0
        });
    }

    var RGBs2 = [];
    var RGBelems2 = $("div.lastUsedControls div.item div.controls div.rgb-PWM").toArray();
    for(var i = 0; i < RGBelems2.length; i++) {
        $elm = $(RGBelems2[i]);
        RGBs2.push({
            elm: $elm,
            Rout: $elm.data("r-color"),
            Gout: $elm.data("g-color"),
            Bout: $elm.data("b-color"),
            R: 0, G: 0, B: 0
        });
    }

    var RGBWs2 = [];
    var RGBWelems2 = $("div.lastUsedControls div.item div.controls div.rgbw-PWM").toArray();
    for(var i = 0; i < RGBWelems2.length; i++) {
        $elm = $(RGBWelems2[i]);
        RGBWs2.push({
            elm: $elm,
            Rout: $elm.data("r-color"),
            Gout: $elm.data("g-color"),
            Bout: $elm.data("b-color"),
            Wout: $elm.data("w-color"),
            R: 0, G: 0, B: 0, W: 0
        });
    }

    for(var i = 0; i < pairs.length; i++) {
        var pair = pairs[i].split(":");
        
        var checkboxes = $("div.roomControls div.item div.controls label input[data-output='" + pair[0] +"']").toArray();
        if(checkboxes.length == 1)
            checkboxes[0].checked = pair[1] != "0";
        
        var checkboxes2 = $("div.lastUsedControls div.item div.controls label input[data-output='" + pair[0] +"']").toArray();
        if(checkboxes2.length == 1)
            checkboxes2[0].checked = pair[1] != "0";
        
        var ranges = $("div.roomControls div.item div.controls input[data-output='" + pair[0] +"']").toArray();
        if(ranges.length == 1) {
            $range_elem = $(ranges[0]);
            var attr = $range_elem.attr('data-non-update');
            if(!(typeof attr !== typeof undefined && attr !== false))
                $range_elem.val(parseInt(pair[1]));
        }
        
        var ranges2 = $("div.lastUsedControls div.item div.controls input[data-output='" + pair[0] +"']").toArray();
        if(ranges2.length == 1) {
            $range_elem = $(ranges2[0]);
            var attr = $range_elem.attr('data-non-update');
            if(!(typeof attr !== typeof undefined && attr !== false))
                $range_elem.val(parseInt(pair[1]));
        }
        
        var radioContainers = $("div.roomControls div.item div.controls div.radioContainer[data-output='" + pair[0] +"']").toArray();
        if(radioContainers.length == 1) {
            var radioButton = $(radioContainers[0]).find("input[type='radio']").toArray()[0];
            radioButton.checked = pair[1] != "0";
        }

        var radioContainers2 = $("div.lastUsedControls div.item div.controls div.radioContainer[data-output='" + pair[0] +"']").toArray();
        if(radioContainers2.length == 1) {
            var radioButton = $(radioContainers2[0]).find("input[type='radio']").toArray()[0];
            radioButton.checked = pair[1] != "0";
        }

        for(var j = 0; j < RGBs.length; j++) {
            var col = RGBs[j];
            if(col.Rout == pair[0])
                col.R = Math.floor(parseFloat(pair[1])*255/100);
            if(col.Gout == pair[0])
                col.G = Math.floor(parseFloat(pair[1])*255/100);
            if(col.Bout == pair[0])
                col.B = Math.floor(parseFloat(pair[1])*255/100);
        }

        for(var j = 0; j < RGBs2.length; j++) {
            var col = RGBs2[j];
            if(col.Rout == pair[0])
                col.R = Math.floor(parseFloat(pair[1])*255/100);
            if(col.Gout == pair[0])
                col.G = Math.floor(parseFloat(pair[1])*255/100);
            if(col.Bout == pair[0])
                col.B = Math.floor(parseFloat(pair[1])*255/100);
        }

        for(var j = 0; j < RGBWs.length; j++) {
            var col = RGBWs[j];
            if(col.Rout == pair[0])
                col.R = Math.floor(parseFloat(pair[1])*255/100);
            if(col.Gout == pair[0])
                col.G = Math.floor(parseFloat(pair[1])*255/100);
            if(col.Bout == pair[0])
                col.B = Math.floor(parseFloat(pair[1])*255/100);
            if(col.Wout == pair[0])
                col.W = Math.floor(parseFloat(pair[1])*255/100);
        }

        for(var j = 0; j < RGBWs2.length; j++) {
            var col = RGBWs2[j];
            if(col.Rout == pair[0])
                col.R = Math.floor(parseFloat(pair[1])*255/100);
            if(col.Gout == pair[0])
                col.G = Math.floor(parseFloat(pair[1])*255/100);
            if(col.Bout == pair[0])
                col.B = Math.floor(parseFloat(pair[1])*255/100);
            if(col.Wout == pair[0])
                col.W = Math.floor(parseFloat(pair[1])*255/100);
        }

/*
        var RGBs = $("div.roomControls div.item div.controls div.rgb-PWM").toArray();
        for(var i = 0; i < RGBs.length; i++) {
            var $elm = $(RGBs[i]);
            var RGBraw = $elm.val().replace(/[^\d,]/g, '').split(',');
            var RGB = {
                R: Math.floor(parseFloat(RGBraw[0])*255/100),
                g: Math.floor(parseFloat(RGBraw[1])*255/100),
                B: Math.floor(parseFloat(RGBraw[2])*255/100)
            };
            
            if($elm.data("r-color") == pair[0]) {
                var newColor = "rgb("+pair[1]+", "+RGB.G+", "+RGB.B+")";
                $elm.css("background-color", newColor);
                $elm.val(newColor);
            }
            
            if($elm.data("g-color") == pair[0]) {
                var newColor = "rgb("+RGB.R+", "+pair[1]+", "+RGB.B+")";
                $elm.css("background-color", newColor);
                $elm.val(newColor);
            }
            
            if($elm.data("b-color") == pair[0]) {
                var newColor = "rgb("+RGB.R+", "+RGB.G+", "+pair[1]+")";
                $elm.css("background-color", newColor);
                $elm.val(newColor);
            }
        }

        var RGBWs = $("div.roomControls div.item div.controls div.rgbw-PWM").toArray();
        for(var i = 0; i < RGBWs.length; i++) {
            var $elm = $(RGBWs[i]);
            var RGBWraw = $elm.val().replace(/[^\d,]/g, '').split(',');
            
            if($elm.data("r-color") == pair[0]) {
                var newColor = "rgb("+pair[1]+", "+RGBWraw[1]+", "+RGBWraw[2]+")";
                $elm.css("background-color", newColor);
                $elm.val(newColor);
            }
            
            if($elm.data("g-color") == pair[0]) {
                var newColor = "rgb("+RGBWraw[0]+", "+pair[1]+", "+RGBWraw[2]+")";
                $elm.css("background-color", newColor);
                $elm.val(newColor);
            }
            
            if($elm.data("b-color") == pair[0]) {
                var newColor = "rgb("+RGBWraw[0]+", "+RGBWraw[1]+", "+pair[1]+")";
                $elm.css("background-color", newColor);
                $elm.val(newColor);
            }
        }*/
    }
    
    var radioButtonsStop = $("div.roomControls div.item div.controls div.radioContainer input.btnStop").toArray();
    for(var i = 0; i < radioButtonsStop.length; i++) {
        var radioBtns = $(radioButtonsStop[i]).parent().parent().find("input[type='radio']");
        if(radioBtns.length == 3) {
            var out1 = radioBtns[0].checked;
            var out2 = radioBtns[2].checked;
            radioButtonsStop[i].checked = !(out1 || out2);
        }
    }
    
    var radioButtonsStop2 = $("div.lastUsedControls div.item div.controls div.radioContainer input.btnStop").toArray();
    for(var i = 0; i < radioButtonsStop2.length; i++) {
        var radioBtns = $(radioButtonsStop2[i]).parent().parent().find("input[type='radio']");
        if(radioBtns.length == 3) {
            var out1 = radioBtns[0].checked;
            var out2 = radioBtns[2].checked;
            radioButtonsStop2[i].checked = !(out1 || out2);
        }
    }

    for(var i = 0; i < RGBs.length; i++) {
        var col = RGBs[i];
        var $elm = col.elm;
        var $btn = $elm.find("button.pick-color");

        var attr = $btn.attr('data-non-update');
        if(!(typeof attr !== typeof undefined && attr !== false)) {
            var RGB = RGBfromRGB(col, {
                R: parseFloat($elm.data("r-lum")),
                G: parseFloat($elm.data("g-lum")),
                B: parseFloat($elm.data("b-lum"))
            });
    
            var newColor = "rgb("+RGB.R+", "+RGB.G+", "+RGB.B+")";
            $btn.css("background-color", newColor);
            $btn.val(newColor);
        }
    }

    for(var i = 0; i < RGBs2.length; i++) {
        var col = RGBs2[i];
        var $elm = col.elm;
        var $btn = $elm.find("button.pick-color");

        var attr = $btn.attr('data-non-update');
        if(!(typeof attr !== typeof undefined && attr !== false)) {
            var RGB = RGBfromRGB(col, {
                R: parseFloat($elm.data("r-lum")),
                G: parseFloat($elm.data("g-lum")),
                B: parseFloat($elm.data("b-lum"))
            });

            var newColor = "rgb("+RGB.R+", "+RGB.G+", "+RGB.B+")";
            $btn.css("background-color", newColor);
            $btn.val(newColor);
        }
    }

    for(var i = 0; i < RGBWs.length; i++) {
        var col = RGBWs[i];
        var $elm = col.elm;
        var $btn = $elm.find("button.pick-color");

        var attr = $btn.attr('data-non-update');
        if(!(typeof attr !== typeof undefined && attr !== false)) {
            var RGB = RGBWtoRGB(col, {
                R: parseFloat($elm.data("r-lum")),
                G: parseFloat($elm.data("g-lum")),
                B: parseFloat($elm.data("b-lum")),
                W: parseFloat($elm.data("w-lum"))
            });

            var newColor = "rgb("+RGB.R+", "+RGB.G+", "+RGB.B+")";
            $btn.css("background-color", newColor);
            $btn.val(newColor);
        }
    }
    
    for(var i = 0; i < RGBWs2.length; i++) {
        var col = RGBWs2[i];
        var $elm = col.elm;
        var $btn = $elm.find("button.pick-color");

        var attr = $btn.attr('data-non-update');
        if(!(typeof attr !== typeof undefined && attr !== false)) {
            var RGB = RGBWtoRGB(col, {
                R: parseFloat($elm.data("r-lum")),
                G: parseFloat($elm.data("g-lum")),
                B: parseFloat($elm.data("b-lum")),
                W: parseFloat($elm.data("w-lum"))
            });

            var newColor = "rgb("+RGB.R+", "+RGB.G+", "+RGB.B+")";
            $btn.css("background-color", newColor);
            $btn.val(newColor);
        }
    }
}

function updateControlSources(res) {
    var pairs = res.split(" ");
    for(var i = 0; i < pairs.length; i++) {
        var pair = pairs[i].split(":");
        var checkboxes = $("div.roomControls div.item div.controls label input[data-output='" + pair[0] +"']").toArray();
        var checkboxes2 = $("div.lastUsedControls div.item div.controls label input[data-output='" + pair[0] +"']").toArray();
        var values = pair[1].split(">");

        if(values[0] == "1" || values[0] == "3") {
            if(checkboxes.length == 1) {
                $(checkboxes[0]).addClass("redswitch");
                $(checkboxes[0]).parent().parent().find("span[class='timer']").html("");
            }
            if(checkboxes2.length == 1) {
                $(checkboxes2[0]).addClass("redswitch");
                $(checkboxes2[0]).parent().parent().find("span[class='timer']").html("");
            }
        } else if(values[0] == "0") {
            if(checkboxes.length == 1) {
                $(checkboxes[0]).removeClass("redswitch");
                $(checkboxes[0]).parent().parent().find("span[class='timer']").html("");
            }
            if(checkboxes2.length == 1) {
                $(checkboxes2[0]).removeClass("redswitch");
                $(checkboxes2[0]).parent().parent().find("span[class='timer']").html("");
            }
        } else if(values[0] == "2") {
            var date = new Date();
            var currTime = date.getTime();
            var stopTime = Number(values[1]);
            var remainingTime = stopTime - currTime;
            remainingTime = Math.floor(remainingTime/1000);
            if(remainingTime < 0)
                remainingTime = 0;
            var days = Math.floor(remainingTime / 86400);
            var hours = Math.floor((remainingTime-days*86400) / 3600);
            var minutes = Math.floor((remainingTime-days*86400-hours*3600) / 60);
            var seconds = Math.floor(remainingTime-days*86400-hours*3600-minutes*60);
            var timerText = "";
            var wasValue = false;
            if(days != 0) {
                timerText += days + "d ";
                wasValue = true;
            }
            if(hours != 0 || wasValue) {
                timerText += hours + "h ";
                wasValue = true;
            }
            if(minutes != 0 || wasValue) {
                timerText += minutes + "m ";
                wasValue = true;
            }
            if(seconds != 0 || wasValue) timerText += seconds + "s";

            if(checkboxes.length == 1) {
                $(checkboxes[0]).removeClass("redswitch");
                $(checkboxes[0]).parent().parent().find("span[class='timer']").html(timerText);
            }
            if(checkboxes2.length == 1) {
                $(checkboxes2[0]).removeClass("redswitch");
                $(checkboxes2[0]).parent().parent().find("span[class='timer']").html(timerText);
            }
        } else {
            if(checkboxes.length == 1)
                $(checkboxes[0]).parent().parent().find("span[class='timer']").html("");
            if(checkboxes2.length == 1)
                $(checkboxes2[0]).parent().parent().find("span[class='timer']").html("");
        }
    }
}

function updateIndicators(res) {
    var pairs = res.split(" ");
    for(var i = 0; i < pairs.length; i++) {
        var pair = pairs[i].split(":");

        var indicators = $("div.roomControls div.item div.controls div.garageDoorsWithIndicator span[data-output='" + pair[0] +"']").toArray();
        var indicators2 = $("div.lastUsedControls div.item div.controls div.garageDoorsWithIndicator span[data-output='" + pair[0] +"']").toArray();
        var indicators3 = $("div.roomControls div.item div.controls div.indicator span[data-output='" + pair[0] +"']").toArray();
        var indicators4 = $("div.lastUsedControls div.item div.controls div.indicator span[data-output='" + pair[0] +"']").toArray();
        if(indicators.length == 1) {
            if(pair[1] == "1")
                indicators[0].setAttribute("class", "green");
            else
                indicators[0].setAttribute("class", "red");
        }
        if(indicators2.length == 1) {
            if(pair[1] == "1")
                indicators2[0].setAttribute("class", "green");
            else
                indicators2[0].setAttribute("class", "red");
        }
        if(indicators3.length == 1) {
            if(pair[1] == "1")
                indicators3[0].setAttribute("class", "green");
            else
                indicators3[0].setAttribute("class", "red");
        }
        if(indicators4.length == 1) {
            if(pair[1] == "1")
                indicators4[0].setAttribute("class", "green");
            else
                indicators4[0].setAttribute("class", "red");
        }
    }
}

function updateActiveTherms(res) {
    var pairs = res.split("-");
    for(var i = 0; i < pairs.length; i++) {
        var pair = pairs[i].split(":");
        var checkboxes = $("div.roomControls div.item div.controls div.tempactive label input[data-output='" + pair[0] +"']").toArray();
        if(checkboxes.length == 1)
            checkboxes[0].checked = pair[1] == "1";
        var checkboxes2 = $("div.lastUsedControls div.item div.controls div.tempactive label input[data-output='" + pair[0] +"']").toArray();
        if(checkboxes2.length == 1)
            checkboxes2[0].checked = pair[1] == "1";
    }
}

function updateThermsTarget(res) {
    var pairs = res.split(" ");
    for(var i = 0; i < pairs.length; i++) {
    var pair = pairs[i].split(":");
    var numberElems = $("div.roomControls div.item div.controls div.tempactive input[type='number'][data-output='" + pair[0] +"']").toArray();
    if(numberElems.length == 1)
        $(numberElems[0]).val(parseInt(pair[1]));
    var numberElems2 = $("div.lastUsedControls div.item div.controls div.tempactive input[type='number'][data-output='" + pair[0] +"']").toArray();
    if(numberElems2.length == 1)
        $(numberElems2[0]).val(parseInt(pair[1]));
    }
}

function executeMacro(command) {
    serverRequest("voidCatch", "macro/" + command);
}

$(document).ready(function() {
    $("div.roomControls").fadeOut(0);
    $("div.rooms").fadeOut(0);
    $("div.mainMenu").fadeOut(0);
    $("div.security").fadeOut(0);
    $("div.securityCameras").fadeOut(0);
    loadMainMenu();

    var counter = 0;
    setInterval(function() {
        if(currentMenu == 2) {
            requestUpdateOutputs();
            requestUpdateControlSources();
            requestUpdateIndicators();
            requestUpdateTemperatures();
            requestUpdateActiveTherms();
            if(counter % 6 == 0)
                requestUpdateThermsTarget();
        }
        if(currentMenu == 0) {
            requestUpdateOutputsLastUsed();
            requestUpdateControlSourcesLastUsed();
            requestUpdateIndicatorsLastUsed();
            requestUpdateTemperaturesLastUsed();
            requestUpdateActiveThermsLastUsed();
            if(counter % 6 == 0)
                requestUpdateThermsTargetLastUsed();
        }
        if(currentMenu == 4) {
            requestSecurityStates();
            if(counter % 2 == 0)
                requestActiveSecurityInputs();
        }
        if(currentMenu == 6)
            requestSimulatorState();
        counter++;
    }, 500);

    $("div.numkeyboard button").click(function() {
        var val = $(this).html();
        if(val == "←") {
            $("div.numkeyboard div.value span").html("&nbsp;");
            $("div.numkeyboard div.value span").css("-webkit-text-security","none");
            $("div.numkeyboard div.value span").css("-moz-text-security","none");
            $("div.numkeyboard div.value span").css("text-security","none");
        } /*else if(val == "AKTIVOVAT" || val=="DEAKTIVOVAT") {
            if(securityState == 1) {
                requestSecurityDisable($("div.numkeyboard div.value span").html());
            } else {
                requestSecurityEnable($("div.numkeyboard div.value span").html());
            }
            $("div.numkeyboard div.value span").html("&nbsp;");
        }*/ else {
            $("div.numkeyboard div.value span").css("-webkit-text-security","disc");
            $("div.numkeyboard div.value span").css("-moz-text-security","disc");
            $("div.numkeyboard div.value span").css("text-security","disc");
            if($("div.numkeyboard div.value span").html() == "&nbsp;")
                $("div.numkeyboard div.value span").html("");
            $("div.numkeyboard div.value span").html($("div.numkeyboard div.value span").html()+val);
        }
    });

    $("div.security div.securityitems div.sitem").click(function() {
        if($(this).data("sid") != '') {
            if($(this).hasClass("sitemred"))
                requestSecurityDisable($("div.numkeyboard div.value span").html(),$(this).data("sid"));
            else
                requestSecurityEnable($("div.numkeyboard div.value span").html(),$(this).data("sid"));
        }
    });
});

function loadMainMenu() {
    $("div#navigationBar div.location").html("<span>" + _DICTIONARY["main_menu"] + "</span>");

    //when leaving security system section, clear typed password
    if(currentMenu == 4) {
        $("div.numkeyboard div.value span").html("&nbsp;");
        $("div.numkeyboard div.value span").css("-webkit-text-security","none");
        $("div.numkeyboard div.value span").css("-moz-text-security","none");
        $("div.numkeyboard div.value span").css("text-security","none");
    }

    currentMenu = 0;
    $("div.roomControls").fadeOut(100);
    $("div.rooms").fadeOut(100);
    $("div.macros").fadeOut(100);
    $("div.security").fadeOut(100);
    $("div.securityCameras").fadeOut(100);
    $("div.simulator").fadeOut(100);

    loadLastUsedControls();

    setTimeout(function() {
        $("div.mainMenu").fadeIn(100);
        $("div.lastUsedControls").fadeIn(100);
    }, 100);
}

function loadSecuritySystem() {
    $("div#navigationBar div.location").html("<span>" + _DICTIONARY["security_system"] + "</span>");
    requestSecurityInputs();

    currentMenu = 4;
    $("div.roomControls").fadeOut(100);
    $("div.rooms").fadeOut(100);
    $("div.macros").fadeOut(100);
    $("div.mainMenu").fadeOut(100);
    $("div.securityCameras").fadeOut(100);
    $("div.simulator").fadeOut(100);
    $("div.lastUsedControls").fadeOut(100);

    setTimeout(function() {
        $("div.security").fadeIn(100);
    }, 100);
}

function loadMacros() {
    $("div#navigationBar div.location").html("<span>" + _DICTIONARY["macros"] + "</span>");

    currentMenu = 3;
    $("div.mainMenu").fadeOut(100);
    $("div.roomControls").fadeOut(100);
    $("div.rooms").fadeOut(100);
    $("div.security").fadeOut(100);
    $("div.securityCameras").fadeOut(100);
    $("div.simulator").fadeOut(100);
    $("div.lastUsedControls").fadeOut(100);
    var macros = $("div.macros");
    macros.toArray()[0].innerHTML = "";

    var rc = DataStore.macros;

    for(var i = 0; i < rc.length; i++) {
        macros.toArray()[0].appendChild(DataStore.generateMacrosItemElement(i, rc[i]));
    }

    setTimeout(function() {
        macros.fadeIn(100);
    }, 100);
}

function loadRooms() {
    $("div#navigationBar div.location").html("<span>" + _DICTIONARY["rooms"] + "</span>");

    currentMenu = 1;
    $("div.mainMenu").fadeOut(100);
    $("div.roomControls").fadeOut(100);
    $("div.macros").fadeOut(100);
    $("div.security").fadeOut(100);
    $("div.securityCameras").fadeOut(100);
    $("div.simulator").fadeOut(100);
    $("div.lastUsedControls").fadeOut(100);
    var rooms = $("div.rooms");
    rooms.toArray()[0].innerHTML = "";

    var rc = DataStore.rooms;

    for(var i = 0; i < rc.length; i++) {
        rooms.toArray()[0].appendChild(DataStore.generateRoomsItemElement(rc[i]));
    }

    setTimeout(function() {
        rooms.fadeIn(100);
    }, 100);
}

function loadSecurityCameras() {
    $("div#navigationBar div.location").html("<span>" + _DICTIONARY["cameras"] + "</span>");

    currentMenu = 5;
    $("div.roomControls").fadeOut(100);
    $("div.rooms").fadeOut(100);
    $("div.macros").fadeOut(100);
    $("div.mainMenu").fadeOut(100);
    $("div.security").fadeOut(100);
    $("div.simulator").fadeOut(100);
    $("div.lastUsedControls").fadeOut(100);

    setTimeout(function() {
        if($("div.securityCameras iframe").attr("src") == "")
            $("div.securityCameras iframe").attr("src", CAMERAS_SOURCE);
        $("div.securityCameras").fadeIn(100);
    }, 100);

    var winh= $(window).height()-65;
    $("div.securityCameras iframe").css("height",winh+'px');
}

function loadSimulator() {
    $("div#navigationBar div.location").html("<span>" + _DICTIONARY["holidays"] + "</span>");

    currentMenu = 6;
    $("div.roomControls").fadeOut(100);
    $("div.rooms").fadeOut(100);
    $("div.macros").fadeOut(100);
    $("div.mainMenu").fadeOut(100);
    $("div.security").fadeOut(100);
    $("div.securityCameras").fadeOut(100);
    $("div.lastUsedControls").fadeOut(100);

    setTimeout(function() {
        $("div.simulator").fadeIn(100);
    }, 100);

    setTimeout(function() {
        requestSimulatorTimes();
    }, 1000);
}

function loadControlsByRoomId(id) {
    $("div#navigationBar div.location").html('<span>'+DataStore.getRoomById(id).name+'</span>');

    currentMenu = 2;
    $("div.mainMenu").fadeOut(100);
    $("div.rooms").fadeOut(100);
    $("div.macros").fadeOut(100);
    $("div.security").fadeOut(100);
    $("div.securityCameras").fadeOut(100);
    $("div.lastUsedControls").fadeOut(100);
    var roomControls = $("div.roomControls");
    roomControls.toArray()[0].innerHTML = "";

    var rc = DataStore.getControlsByRoom(id);

    for(var i = 0; i < rc.length; i++) {
        roomControls.toArray()[0].appendChild(DataStore.generateRoomControlsItemElement(rc[i]));
    }

    var switches = document.querySelectorAll('div.roomControls input[type="checkbox"].ios-switch');

    for(var i = 0, sw; sw = switches[i++]; ) {
        var div = document.createElement('div');
        div.className = 'switch';
        sw.parentNode.insertBefore(div, sw.nextSibling);
    }

    var color_pickers_rgbw = document.querySelectorAll('div.roomControls div.rgbw-PWM button.pick-color');

    for(var i = 0, cp; cp = color_pickers_rgbw[i++]; ) {
        $(color_pickers_rgbw).colorPicker({
            body: document.body,
            forceAlpha: false,
            opacity: false,
            renderCallback: function($elm, toggled) {
                if(toggled == true) { // opened
                    $elm.attr("data-non-update", true);
                } else if(toggled === false) { // closed
                    setTimeout(function() { //prevent too early update from server
                        $elm.removeAttr("data-non-update");
                    }, 500);
                    $parent = $elm.parent();
                    var RGBraw = $elm.val().replace(/[^\d,]/g, '').split(',');
                    var luminance = {
                        R: parseFloat($parent.data("r-lum")),
                        G: parseFloat($parent.data("g-lum")),
                        B: parseFloat($parent.data("b-lum")),
                        W: parseFloat($parent.data("w-lum"))
                    };
                    var RGBW = RGBtoRGBW({
                        R: parseFloat(RGBraw[0]),
                        G: parseFloat(RGBraw[1]),
                        B: parseFloat(RGBraw[2])
                    }, luminance);
                    var RGB = RGBWtoRGB(RGBW, luminance);
                    $elm.css("background-color", "rgb("+RGB.R+", "+RGB.G+", "+RGB.B+")");
                    $elm.val("rgb("+RGB.R+", "+RGB.G+", "+RGB.B+")");

                    requestOutputsChange(
                        $parent.data("r-color")+":"+Math.floor(RGBW.R*100/255)
                        +"-"+ $parent.data("g-color")+":"+Math.floor(RGBW.G*100/255)
                        +"-"+ $parent.data("b-color")+":"+Math.floor(RGBW.B*100/255)
                        +"-"+ $parent.data("w-color")+":"+Math.floor(RGBW.W*100/255)
                    );
                    updateOutputLastUsage($parent.attr("data-dbid"));
                } else {} // color changed
            }
        });
    }

    var color_pickers_rgb = document.querySelectorAll('div.roomControls div.rgb-PWM button.pick-color');

    for(var i = 0, cp; cp = color_pickers_rgb[i++]; ) {
        $(color_pickers_rgb).colorPicker({
            body: document.body,
            forceAlpha: false,
            opacity: false,
            renderCallback: function($elm, toggled) {
                if(toggled == true) { // opened
                    $elm.attr("data-non-update", true);
                } else if(toggled === false) { // closed
                    setTimeout(function() { //prevent too early update from server
                        $elm.removeAttr("data-non-update");
                    }, 500);
                    $parent = $elm.parent();
                    var RGBraw = $elm.val().replace(/[^\d,]/g, '').split(',');
                    var luminance = {
                        R: parseFloat($parent.data("r-lum")),
                        G: parseFloat($parent.data("g-lum")),
                        B: parseFloat($parent.data("b-lum"))
                    };
                    var RGB1 = {
                        R: parseFloat(RGBraw[0]),
                        G: parseFloat(RGBraw[1]),
                        B: parseFloat(RGBraw[2])
                    };
                    var RGB2 = RGBtoRGB(RGB1, luminance);
                    
                    requestOutputsChange(
                        $parent.data("r-color")+":"+Math.floor(RGB2.R*100/255)
                        +"-"+ $parent.data("g-color")+":"+Math.floor(RGB2.G*100/255)
                        +"-"+ $parent.data("b-color")+":"+Math.floor(RGB2.B*100/255)
                    );
                    updateOutputLastUsage($parent.attr("data-dbid"));
                } else {} // color changed
            }
        });
    }

    setTimeout(function() {
        roomControls.fadeIn(100);
    }, 100);

    $("input[type='checkbox'].ios-switch").click(function(event) {
        event.preventDefault();
        var check = this;
        requestOutputChange($(check).attr("data-output"), check.checked ? 1 : 0);
        updateOutputLastUsage($(check).attr("data-dbid"));
    });

    $("div.tempactive input[type='checkbox'].ios-switch").click(function(event) {
        event.preventDefault();
        var check = this;
        requestThermActiveChange($(check).attr("data-output"), check.checked ? 1 : 0);
        updateOutputLastUsage($(check).attr("data-dbid"));
    });

    $("input[type='range']").change(function(event) {
        event.preventDefault();
        var check = this;
        requestOutputChange($(check).attr("data-output"), $(check).val());
        updateOutputLastUsage($(check).attr("data-dbid"));
    });

    //var isTouchDevice = 'ontouchstart' in document.documentElement;

    $("input[type='range']").mousedown(function(event) {
        if(event.which == 1) //left click
            $(event.target).attr("data-non-update", true);
    });

    $("input[type='range']").mouseup(function(event) {
        setTimeout(function() { //prevent too early update from server
            $(event.target).removeAttr("data-non-update");
        }, 2000);
    });

    $("input[type='range']").on("touchstart", function(event) {
        $(event.target).attr("data-non-update", true);
    });

    $("input[type='range']").on("touchend", function(event) {
        setTimeout(function() { //prevent too early update from server
            $(event.target).removeAttr("data-non-update");
        }, 2000);
    });

    $("input[type='number']").change(function(event) {
        event.preventDefault();
        var check = this;
        requestThermTargetChange($(check).attr("data-output"), $(check).val());
        updateOutputLastUsage($(check).attr("data-dbid"));
    });

    $("div.radioContainer input[type='radio']").click(function(event) {
        event.preventDefault();
        var parent = $(this).parent();
        var cssClass = $(this).attr("class");
        if(cssClass == "btnDown" || cssClass == "btnUp") {
            requestOutputChange(parent.attr("data-output"), 1);
            updateOutputLastUsage(parent.attr("data-dbid"));
        } else if(cssClass == "btnStop") {
            var outputs = parent.attr("data-output").split(",");
            updateOutputLastUsage(parent.attr("data-dbid"));
            requestOutputChange(outputs[0], 0);
            setTimeout(function() {
                requestOutputChange(outputs[1], 0);
            }, 200);
        }
    });

    $("div.garageDoorsWithIndicator button").click(function(event) {
        event.preventDefault();
        var parent = $(this).parent();
        var outputs = parent.attr("data-output").split(",");
        updateOutputLastUsage(parent.attr("data-dbid"));
        requestOutputChange(outputs[0], 1);
        setTimeout(function() {
            requestOutputChange(outputs[0], 0);
        }, 1000);
        notification(_DICTIONARY["pressed"]);
    });

    $("div.rgbw-PWM button.darkness").click(function(event) {
        var $parent = $(this).parent();
        var $btn = $parent.find("button.pick-color");
        $btn.css("background-color", "rgb(0, 0, 0)");
        $btn.val("rgb(0, 0, 0)");

        requestOutputsChange(
            $parent.data("r-color")+":0"
            +"-"+ $parent.data("g-color")+":0"
            +"-"+ $parent.data("b-color")+":0"
            +"-"+ $parent.data("w-color")+":0"
        );
        updateOutputLastUsage($parent.attr("data-dbid"));
    });

    $("div.rgb-PWM button.darkness").click(function(event) {
        var $parent = $(this).parent();
        var $btn = $parent.find("button.pick-color");
        $btn.css("background-color", "rgb(0, 0, 0)");
        $btn.val("rgb(0, 0, 0)");

        requestOutputsChange(
            $parent.data("r-color")+":0"
            +"-"+ $parent.data("g-color")+":0"
            +"-"+ $parent.data("b-color")+":0"
        );
        updateOutputLastUsage($parent.attr("data-dbid"));
    });
}

function loadLastUsedControls() {
    var roomControls = $("div.lastUsedControls");
    roomControls.toArray()[0].innerHTML = "<div class='separator'>" + _DICTIONARY["last_used"] + "</div>";

    var rc = DataStore.getLastUsedControls();
    var usedRooms = [];

    for(var i = 0; i < rc.length; i++) {
        if(!(usedRooms.indexOf(rc[i].room) > -1))
        usedRooms.push(rc[i].room);
    }

    for(var roomindex = 0; roomindex < usedRooms.length; roomindex++) {
        var currentRoom = usedRooms[roomindex];
        var roomName = DataStore.getRoomById(currentRoom).name;
        roomControls.toArray()[0].innerHTML+= "<div class='roomName'>"+roomName+"</div>";

        for(var i = 0; i < rc.length; i++)
            if(rc[i].room == currentRoom)
                roomControls.toArray()[0].appendChild(DataStore.generateRoomControlsItemElement(rc[i]));
    }

    var switches = document.querySelectorAll('div.lastUsedControls input[type="checkbox"].ios-switch');

    for (var i=0, sw; sw = switches[i++]; ) {
        var div = document.createElement('div');
        div.className = 'switch';
        sw.parentNode.insertBefore(div, sw.nextSibling);
    }

    var color_pickers_rgbw = document.querySelectorAll('div.lastUsedControls div.rgbw-PWM button.pick-color');

    for(var i = 0, cp; cp = color_pickers_rgbw[i++]; ) {
        $(color_pickers_rgbw).colorPicker({
            body: document.body,
            forceAlpha: false,
            opacity: false,
            renderCallback: function($elm, toggled) {
                if(toggled == true) { // opened
                    $elm.attr("data-non-update", true);
                } else if(toggled === false) { // closed
                    setTimeout(function() { //prevent too early update from server
                        $elm.removeAttr("data-non-update");
                    }, 500);
                    $parent = $elm.parent();
                    var RGBraw = $elm.val().replace(/[^\d,]/g, '').split(',');
                    var luminance = {
                        R: parseFloat($parent.data("r-lum")),
                        G: parseFloat($parent.data("g-lum")),
                        B: parseFloat($parent.data("b-lum")),
                        W: parseFloat($parent.data("w-lum"))
                    };
                    var RGBW = RGBtoRGBW({
                        R: parseFloat(RGBraw[0]),
                        G: parseFloat(RGBraw[1]),
                        B: parseFloat(RGBraw[2])
                    }, luminance);
                    var RGB = RGBWtoRGB(RGBW, luminance);
                    $elm.css("background-color", "rgb("+RGB.R+", "+RGB.G+", "+RGB.B+")");
                    $elm.val("rgb("+RGB.R+", "+RGB.G+", "+RGB.B+")");

                    requestOutputsChange(
                        $parent.data("r-color")+":"+Math.floor(RGBW.R*100/255)
                        +"-"+ $parent.data("g-color")+":"+Math.floor(RGBW.G*100/255)
                        +"-"+ $parent.data("b-color")+":"+Math.floor(RGBW.B*100/255)
                        +"-"+ $parent.data("w-color")+":"+Math.floor(RGBW.W*100/255)
                    );
                    updateOutputLastUsage($parent.attr("data-dbid"));
                } else {} // color changed
            }
        });
    }

    var color_pickers_rgb = document.querySelectorAll('div.lastUsedControls div.rgb-PWM button.pick-color');

    for(var i = 0, cp; cp = color_pickers_rgb[i++]; ) {
        $(color_pickers_rgb).colorPicker({
            body: document.body,
            forceAlpha: false,
            opacity: false,
            renderCallback: function($elm, toggled) {
                if(toggled == true) { // opened
                    $elm.attr("data-non-update", true);
                } else if(toggled === false) { // closed
                    setTimeout(function() { //prevent too early update from server
                        $elm.removeAttr("data-non-update");
                    }, 500);
                    $parent = $elm.parent();
                    var RGBraw = $elm.val().replace(/[^\d,]/g, '').split(',');
                    var luminance = {
                        R: parseFloat($parent.data("r-lum")),
                        G: parseFloat($parent.data("g-lum")),
                        B: parseFloat($parent.data("b-lum"))
                    };
                    var RGB = RGBtoRGB({
                        R: parseFloat(RGBraw[0]),
                        G: parseFloat(RGBraw[1]),
                        B: parseFloat(RGBraw[2])
                    }, luminance);
                    
                    requestOutputsChange(
                        $parent.data("r-color")+":"+Math.floor(RGB.R*100/255)
                        +"-"+ $parent.data("g-color")+":"+Math.floor(RGB.G*100/255)
                        +"-"+ $parent.data("b-color")+":"+Math.floor(RGB.B*100/255)
                    );
                    updateOutputLastUsage($parent.attr("data-dbid"));
                } else {} // color changed
            }
        });
    }

    $("input[type='checkbox'].ios-switch").click(function(event) {
        event.preventDefault();
        var check = this;
        requestOutputChange($(check).attr("data-output"), check.checked ? 1 : 0);
        updateOutputLastUsage($(check).attr("data-dbid"));
    });

    $("div.tempactive input[type='checkbox'].ios-switch").click(function(event) {
        event.preventDefault();
        var check = this;
        requestThermActiveChange($(check).attr("data-output"), check.checked ? 1 : 0);
        updateOutputLastUsage($(check).attr("data-dbid"));
    });

    $("input[type='range']").change(function(event) {
        event.preventDefault();
        var check = this;
        requestOutputChange($(check).attr("data-output"), $(check).val());
        updateOutputLastUsage($(check).attr("data-dbid"));
    });

    $("input[type='range']").mousedown(function(event) {
        if(event.which == 1) //left click
            $(event.target).attr("data-non-update", true);
    });

    $("input[type='range']").mouseup(function(event) {
        setTimeout(function() { //prevent too early update from server
            $(event.target).removeAttr("data-non-update");
        }, 2000);
    });

    $("input[type='range']").on("touchstart", function(event) {
        $(event.target).attr("data-non-update", true);
    });

    $("input[type='range']").on("touchend", function(event) {
        setTimeout(function() { //prevent too early update from server
            $(event.target).removeAttr("data-non-update");
        }, 2000);
    });

    $("input[type='number']").change(function(event) {
        event.preventDefault();
        var check = this;
        requestThermTargetChange($(check).attr("data-output"), $(check).val());
        updateOutputLastUsage($(check).attr("data-dbid"));
    });

    $("div.radioContainer input[type='radio']").click(function(event) {
        event.preventDefault();
        var parent = $(this).parent();
        var cssClass = $(this).attr("class");
        if(cssClass == "btnDown" || cssClass == "btnUp") {
            requestOutputChange(parent.attr("data-output"), 1);
            updateOutputLastUsage(parent.attr("data-dbid"));
        } else if(cssClass == "btnStop") {
            var outputs = parent.attr("data-output").split(",");
            updateOutputLastUsage(parent.attr("data-dbid"));
            requestOutputChange(outputs[0], 0);
            setTimeout(function() {
                requestOutputChange(outputs[1], 0);
            }, 200);
        }
    });

    $("div.sunblindpulse button.controlUp").click(function(event) {
        var out = $(this).attr("data-output");
        requestPulsingStart(out, 1, 100);
        setTimeout(function(){
            requestPulsingStop(out);
        }, 500);
        updateOutputLastUsage($(this).attr("data-dbid"));
    });

    $("div.sunblindpulse button.controlDown").click(function(event) {
        var out = $(this).attr("data-output");
        requestPulsingStart(out, 1, 100);
        setTimeout(function(){
            requestPulsingStop(out);
        }, 500);
        updateOutputLastUsage($(this).attr("data-dbid"));
    });

    $("button.controlUp").click(function() {
        var parent = $(this).parent();
        var num = parent.find("input[type=number]");
        var newVal = parseInt(num.val())+1;
        requestThermTargetChange(num.attr("data-output"), newVal);
        num.val(newVal);
        updateOutputLastUsage(num.attr("data-dbid"));
    });

    $("button.controlDown").click(function() {
        var parent = $(this).parent();
        var num = parent.find("input[type=number]");
        var newVal = parseInt(num.val())-1;
        requestThermTargetChange(num.attr("data-output"), newVal);
        num.val(newVal);
        updateOutputLastUsage(num.attr("data-dbid"));
    });

    $("div.garageDoorsWithIndicator button").click(function(event) {
        event.preventDefault();
        var parent = $(this).parent();
        var outputs = parent.attr("data-output").split(",");
        updateOutputLastUsage(parent.attr("data-dbid"));
        requestOutputChange(outputs[0], 1);
        setTimeout(function() {
            requestOutputChange(outputs[0], 0);
        }, 1000);
        notification(_DICTIONARY["pressed"]);
    });

    $("div.rgbw-PWM button.darkness").click(function(event) {
        var $parent = $(this).parent();
        var $btn = $parent.find("button.pick-color");
        $btn.css("background-color", "rgb(0, 0, 0)");
        $btn.val("rgb(0, 0, 0)");

        requestOutputsChange(
            $parent.data("r-color")+":0"
            +"-"+ $parent.data("g-color")+":0"
            +"-"+ $parent.data("b-color")+":0"
            +"-"+ $parent.data("w-color")+":0"
        );
        updateOutputLastUsage($parent.attr("data-dbid"));
    });

    $("div.rgb-PWM button.darkness").click(function(event) {
        var $parent = $(this).parent();
        var $btn = $parent.find("button.pick-color");
        $btn.css("background-color", "rgb(0, 0, 0)");
        $btn.val("rgb(0, 0, 0)");

        requestOutputsChange(
            $parent.data("r-color")+":0"
            +"-"+ $parent.data("g-color")+":0"
            +"-"+ $parent.data("b-color")+":0"
        );
        updateOutputLastUsage($parent.attr("data-dbid"));
    });
}

function goBack() {
    //when leaving security system section, clear typed password
    if(currentMenu == 4) {
        $("div.numkeyboard div.value span").html("&nbsp;");
        $("div.numkeyboard div.value span").css("-webkit-text-security","none");
        $("div.numkeyboard div.value span").css("-moz-text-security","none");
        $("div.numkeyboard div.value span").css("text-security","none");
    }

    switch(currentMenu) {
        case 1:
        case 3:
        case 4:
        case 5:
        case 6:
            loadMainMenu();
            break;
        case 2:
            loadRooms();
            break;
        default:
            break;
    }
}

function updateOutputLastUsage(itemId) {
    var timeNow = (new Date()).getTime();
    $.ajax({
        method: "GET",
        url: "phpscript/logLastUsage.php",
        data: { id: itemId, time: timeNow }
    });
}

function notification(text) {
    var notifications = $("body").toArray()[0];
    var notification = document.createElement("div");
    notification.setAttribute("class", "notification");
    notification.innerHTML = "<div class='notifcenter'>" + text + "</div>";
    notifications.appendChild(notification);
    $(notification).fadeIn(100);
    setTimeout(function() {
        $(notification).fadeOut(100);
        setTimeout(function() {
            notifications.removeChild(notification);
        }, 100);
    }, 1100);
}
