<?php
require __DIR__ . '/../../app/admin.php';


// check if user is logged in
if(!isset($_SESSION["userid"])){
	header('HTTP/1.1 401 Unauthorized');
	exit;
}


require __DIR__ . '/../../app/proxy.php';
