<?php
$controlID = $httpRequest->getQuery('id');
$row = $dbWeb->fetch("
	SELECT
		room,
		name,
		outputs,
		type,
		icon
	FROM controls
	WHERE
		id = ?
	",
	$controlID
);

if(!$row){
	return;
}

$rsCUs = $dbServer->fetchAll("SELECT software_address, description FROM control_units ORDER BY software_address ASC");
?>
<style>
button {
	background-color: #555;
	color: #dbd9d9;
	width: 130px;
	height: 40px;
	line-height: 30px;
	text-align: center;
	border: none;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
	font-size: 15px;
	margin-top: 15px;
	cursor: pointer;
	margin-right: 5px;
	-webkit-transition: all 0.25s linear;
	-webkit-box-shadow: inset 0px -3px 33px 0px rgba(0,0,0,0.75);
	-moz-box-shadow: inset 0px -3px 33px 0px rgba(0,0,0,0.75);
	box-shadow: inset 0px -3px 33px 0px rgba(0,0,0,0.75);
}

button:hover {
	color: #ffffff;
	background-color: #333;
}

input[type="number"], textarea, input[type="text"] {
	color: #ffffff;
	background-color: #383b3f;
	width: 100px;
	border: none;
	padding: 10px 15px;
	font-size: 12px;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
}

select, select option {
	font-size: 12px;
	border: none;
	color: #ffffff;
	background-color: #383b3f;
	padding: 10px 15px;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
}

textarea {
	width: 413px;
	height: 50px;
}

table#options_table {
	border: none;
	width: 100%;
	border-collapse: separate;
	border-spacing: 5px;
	-webkit-touch-callout: none;
	-webkit-user-select: none;
	-khtml-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

table#options_table tr td {
	vertical-align: top;
}

table#options_table tr td:nth-child(1) {
	width: 30%;
}

table#options_table tr td:nth-child(2) {
	width: 70%;
}

img#in_icon {
	cursor: pointer;
	width: 50px;
	height: 50px;
	border: 2px solid #222;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
	padding: 10px;
}

div#iconbox {
	display: none;
	border: 2px solid #222;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
	width: 400px;
	height: auto;
	padding: 10px;
	position: absolute;
	background-color: #ffffff;
}

img.in_icon_select {
	border: 1px solid #222;
	width: 32px;
	height: 32px;
	margin-left: 4px;
	cursor: pointer;
}
</style>

<?php
	$room = $row[0];
	$name = $row[1];
	$outputs = $row[2];
	$type = $row[3];
	$icon = $row[4];
?>
<div>
	<table id="options_table">
		<tr>
			<td><?=$_DICTIONARY["name"]?>: </td>
			<td><textarea id="in_name"><?=$name?></textarea></td>
		</tr>
		<tr>
			<td><?=$_DICTIONARY["room"]?>: </td>
			<td><select id="in_room">
				<?php $rsRoomsX = $dbWeb->query("SELECT id, name, floor FROM rooms ORDER BY name ASC"); ?>
				<?php foreach($rsRoomsX as $rowX){ ?>
					<option value="<?=$rowX[0]?>"><?=$rowX[1]?> (<?=($rowX[2]=="0" ? "Přízemí" : "Patro ".$rowX[2])?>)</option>
				<?php }; ?>
			</select></td>
		</tr>
		<tr>
			<td><?=$_DICTIONARY["control_type"]?>: </td>
			<td><select id="in_control_type">
				<option value="0"><?=$_DICTIONARY["switch"]?></option>
				<option value="1"><?=$_DICTIONARY["thermostat"]?></option>
				<option value="2"><?=$_DICTIONARY["slider"]?></option>
				<option value="3"><?=$_DICTIONARY["sunblind"]?></option>
				<option value="4"><?=$_DICTIONARY["sunblind_with_buttons"]?></option>
				<option value="5"><?=$_DICTIONARY["button_with_indicator"]?></option>
				<option value="6"><?=$_DICTIONARY["indicator"]?></option>
				<option value="7"><?=$_DICTIONARY["color_rgb"]?></option>
				<option value="8"><?=$_DICTIONARY["color_rgbw"]?></option>
			</select></td>
		</tr>
		<tr>
			<td><?=$_DICTIONARY["icon"]?>: </td>
			<td><img id="in_icon" data-name="<?=$icon?>" src="../res/green/icons/controls/<?=$icon?>" /><div id="iconbox">
				<?php foreach (new DirectoryIterator('../res/green/icons/controls') as $fileInfo) {
					if($fileInfo->isFile())
						echo "<img class=\"in_icon_select\" data-name=\"".$fileInfo->getFilename()."\" src=\"../res/green/icons/controls/" . $fileInfo->getFilename() . "\" />";
				} ?>
			</div></td>
		</tr>
		<tr id="tr_in_cu">
			<td><?=$_DICTIONARY["control_unit"]?>: </td>
			<td><select id="in_cu">
				<?php foreach($rsCUs as $rowX){ ?>
					<option value="<?=$rowX[0]?>"><?=$rowX[0]?><?=($rowX[1]=="" ? "" : " > ".$rowX[1])?></option>
				<?php }; ?>
			</select></td>
		</tr>
		<tr id="tr_in_motor_up" class="hidden">
			<td><?=$_DICTIONARY["output_up"]?>: </td>
			<td><select id="in_motor_up">
				<?php foreach($rsCUs as $rowX){ ?>
					<option value="<?=$rowX[0]?>"><?=$rowX[0]?><?=($rowX[1]=="" ? "" : " > ".$rowX[1])?></option>
				<?php }; ?>
			</select></td>
		</tr>
		<tr id="tr_in_motor_down" class="hidden">
			<td><?=$_DICTIONARY["output_down"]?>: </td>
			<td><select id="in_motor_down">
				<?php foreach($rsCUs as $rowX){ ?>
					<option value="<?=$rowX[0]?>"><?=$rowX[0]?><?=($rowX[1]=="" ? "" : " > ".$rowX[1])?></option>
				<?php }; ?>
			</select></td>
		</tr>
		<tr id="tr_in_indicator" class="hidden">
			<td><?=$_DICTIONARY["indicator"]?>: </td>
			<td><select id="in_indicator">
				<?php foreach($rsCUs as $rowX){ ?>
					<option value="<?=$rowX[0]?>"><?=$rowX[0]?><?=($rowX[1]=="" ? "" : " > ".$rowX[1])?></option>
				<?php }; ?>
			</select></td>
		</tr>
		<tr id="tr_in_cu_color_r" class="hidden">
			<td><?=$_DICTIONARY["out_color_r"]?>: </td>
			<td><select id="in_cu_color_r">
				<?php foreach($rsCUs as $row){ ?>
					<option value="<?=$row[0]?>"><?=$row[0]?><?=($row[1]=="" ? "" : " > ".$row[1])?></option>
				<?php }; ?>
			</select></td>
		</tr>
		<tr id="tr_in_cu_color_g" class="hidden">
			<td><?=$_DICTIONARY["out_color_g"]?>: </td>
			<td><select id="in_cu_color_g">
				<?php foreach($rsCUs as $row){ ?>
					<option value="<?=$row[0]?>"><?=$row[0]?><?=($row[1]=="" ? "" : " > ".$row[1])?></option>
				<?php }; ?>
			</select></td>
		</tr>
		<tr id="tr_in_cu_color_b" class="hidden">
			<td><?=$_DICTIONARY["out_color_b"]?>: </td>
			<td><select id="in_cu_color_b">
				<?php foreach($rsCUs as $row){ ?>
					<option value="<?=$row[0]?>"><?=$row[0]?><?=($row[1]=="" ? "" : " > ".$row[1])?></option>
				<?php }; ?>
			</select></td>
		</tr>
		<tr id="tr_in_cu_color_w" class="hidden">
			<td><?=$_DICTIONARY["out_color_w"]?>: </td>
			<td><select id="in_cu_color_w">
				<?php foreach($rsCUs as $row){ ?>
					<option value="<?=$row[0]?>"><?=$row[0]?><?=($row[1]=="" ? "" : " > ".$row[1])?></option>
				<?php }; ?>
			</select></td>
		</tr>
		<tr id="tr_in_r_lum" class="hidden">
			<td><?=$_DICTIONARY["lum_r"]?>: </td>
			<td><input id="in_r_lum" type="number" min="0.01" step="0.01" value="1" onchange="if(this.value < 0.1) this.value = 0.1;" /></td>
		</tr>
		<tr id="tr_in_g_lum" class="hidden">
			<td><?=$_DICTIONARY["lum_g"]?>: </td>
			<td><input id="in_g_lum" type="number" min="0.01" step="0.01" value="1" onchange="if(this.value < 0.1) this.value = 0.1;" /></td>
		</tr>
		<tr id="tr_in_b_lum" class="hidden">
			<td><?=$_DICTIONARY["lum_b"]?>: </td>
			<td><input id="in_b_lum" type="number" min="0.01" step="0.01" value="1" onchange="if(this.value < 0.1) this.value = 0.1;" /></td>
		</tr>
		<tr id="tr_in_w_lum" class="hidden">
			<td><?=$_DICTIONARY["lum_w"]?>: </td>
			<td><input id="in_w_lum" type="number" min="0.01" step="0.01" value="1" onchange="if(this.value < 0.1) this.value = 0.1;" /></td>
		</tr>
		<tr>
			<td></td>
			<td><button id="in_save"><?=$_DICTIONARY["save"]?></button><button id="in_cancel"><?=$_DICTIONARY["cancel"]?></button></td>
		</tr>
	</table>
</div>
<script>
$(document).ready(function() {
	var updateControlUnits = function() {
		var ctype = Number($("#in_control_type").val());

		switch(ctype) {
			case 0: //switch
			case 1: //thermostat
			case 2: //slider
			case 6: //indicator
				$("#tr_in_cu").removeClass("hidden");
				$("#tr_in_motor_up").addClass("hidden");
				$("#tr_in_motor_down").addClass("hidden");
				$("#tr_in_indicator").addClass("hidden");
                $("#tr_in_cu_color_r").addClass("hidden");
                $("#tr_in_cu_color_g").addClass("hidden");
                $("#tr_in_cu_color_b").addClass("hidden");
                $("#tr_in_cu_color_w").addClass("hidden");
                $("#tr_in_r_lum").addClass("hidden");
                $("#tr_in_g_lum").addClass("hidden");
                $("#tr_in_b_lum").addClass("hidden");
                $("#tr_in_w_lum").addClass("hidden");
				break;
			case 3: //upDownStopSwitch
			case 4: //upDownStopSwitchWithPulseButtons
				$("#tr_in_cu").addClass("hidden");
				$("#tr_in_motor_up").removeClass("hidden");
				$("#tr_in_motor_down").removeClass("hidden");
				$("#tr_in_indicator").addClass("hidden");
                $("#tr_in_cu_color_r").addClass("hidden");
                $("#tr_in_cu_color_g").addClass("hidden");
                $("#tr_in_cu_color_b").addClass("hidden");
                $("#tr_in_cu_color_w").addClass("hidden");
                $("#tr_in_r_lum").addClass("hidden");
                $("#tr_in_g_lum").addClass("hidden");
                $("#tr_in_b_lum").addClass("hidden");
                $("#tr_in_w_lum").addClass("hidden");
				break;
			case 5: //switchWithIndicator
				$("#tr_in_cu").removeClass("hidden");
				$("#tr_in_motor_up").addClass("hidden");
				$("#tr_in_motor_down").addClass("hidden");
				$("#tr_in_indicator").removeClass("hidden");
                $("#tr_in_cu_color_r").addClass("hidden");
                $("#tr_in_cu_color_g").addClass("hidden");
                $("#tr_in_cu_color_b").addClass("hidden");
                $("#tr_in_cu_color_w").addClass("hidden");
                $("#tr_in_r_lum").addClass("hidden");
                $("#tr_in_g_lum").addClass("hidden");
                $("#tr_in_b_lum").addClass("hidden");
                $("#tr_in_w_lum").addClass("hidden");
				break;
            case 7: //color RGB
				$("#tr_in_cu").addClass("hidden");
				$("#tr_in_motor_up").addClass("hidden");
				$("#tr_in_motor_down").addClass("hidden");
				$("#tr_in_indicator").addClass("hidden");
                $("#tr_in_cu_color_r").removeClass("hidden");
                $("#tr_in_cu_color_g").removeClass("hidden");
                $("#tr_in_cu_color_b").removeClass("hidden");
                $("#tr_in_cu_color_w").addClass("hidden");
                $("#tr_in_r_lum").removeClass("hidden");
                $("#tr_in_g_lum").removeClass("hidden");
                $("#tr_in_b_lum").removeClass("hidden");
                $("#tr_in_w_lum").addClass("hidden");
                break;
            case 8: //color RGBW
				$("#tr_in_cu").addClass("hidden");
				$("#tr_in_motor_up").addClass("hidden");
				$("#tr_in_motor_down").addClass("hidden");
				$("#tr_in_indicator").addClass("hidden");
                $("#tr_in_cu_color_r").removeClass("hidden");
                $("#tr_in_cu_color_g").removeClass("hidden");
                $("#tr_in_cu_color_b").removeClass("hidden");
                $("#tr_in_cu_color_w").removeClass("hidden");
                $("#tr_in_r_lum").removeClass("hidden");
                $("#tr_in_g_lum").removeClass("hidden");
                $("#tr_in_b_lum").removeClass("hidden");
                $("#tr_in_w_lum").removeClass("hidden");
                break;
		}
	}

	$("#in_control_type").change(updateControlUnits);

	$("button#in_cancel").click(function() {
		location.href = "?page=controls";
	});

	$("button#in_save").click(function() {
		var name = $("#in_name").val();
		var room = $("#in_room").val();
		var type = $("#in_control_type").val();
		var outputs = "";
		switch(Number(type)) {
			case 0: //switch
			case 1: //thermostat
			case 2: //slider
			case 6: //indicator
				outputs = $("#in_cu").val();
				break;
			case 3: //upDownStopSwitch
			case 4: //upDownStopSwitchWithPulseButtons
				outputs = $("#in_motor_down").val() + "," + $("#in_motor_up").val();
				break;
			case 5: //switchWithIndicator
				outputs = $("#in_cu").val() + "," + $("#in_indicator").val();
				break;
            case 7: //color RGB
                outputs = $("#in_cu_color_r").val() + "," + $("#in_cu_color_g").val()
                    + "," + $("#in_cu_color_b").val() + "," + $("#in_r_lum").val()
                    + "," + $("#in_g_lum").val() + "," + $("#in_b_lum").val();
                break;
            case 8: //color RGBW
                outputs = $("#in_cu_color_r").val() + "," + $("#in_cu_color_g").val()
                    + "," + $("#in_cu_color_b").val() + "," + $("#in_cu_color_w").val()
                    + "," + $("#in_r_lum").val() + "," + $("#in_g_lum").val()
                    + "," + $("#in_b_lum").val() + "," + $("#in_w_lum").val();
                break;
		}
		var icon = $("#in_icon").data("name");
		var urlGetData = "id=<?=$controlID?>&name="+name+"&room="+room+"&outputs="+outputs+"&type="+type+"&icon="+icon;
		$.get("phpscript/updateControl.php?"+urlGetData, function(data) {
			if(data.trim() == "OK")
				location.href = "?page=controls";
			else
				alert("<?=$_DICTIONARY["save_ccontrol_fail"]?>");
		});
	});

	$("img#in_icon").click(function() {
		if($("div#iconbox").is(":visible"))
			$("div#iconbox").fadeOut(300);
		else
			$("div#iconbox").fadeIn(500);
	});

	$("img.in_icon_select").click(function() {
		$("div#iconbox").fadeOut(300);
		$("img#in_icon").data("name", $(this).data("name"));
		$("img#in_icon").attr("src", $(this).attr("src"));
	});

	$("#in_room").val("<?=$room?>");
	$("#in_control_type").val("<?=$type?>");
	switch(<?=$type?>) {
		case 0: //switch
		case 1: //thermostat
		case 2: //slider
		case 6: //indicator
			$("#tr_in_cu").removeClass("hidden");
			$("#tr_in_motor_up").addClass("hidden");
			$("#tr_in_motor_down").addClass("hidden");
			$("#tr_in_indicator").addClass("hidden");
            $("#tr_in_cu_color_r").addClass("hidden");
            $("#tr_in_cu_color_g").addClass("hidden");
            $("#tr_in_cu_color_b").addClass("hidden");
            $("#tr_in_cu_color_w").addClass("hidden");
            $("#tr_in_r_lum").addClass("hidden");
            $("#tr_in_g_lum").addClass("hidden");
            $("#tr_in_b_lum").addClass("hidden");
            $("#tr_in_w_lum").addClass("hidden");

			$("#in_cu").val("<?=$outputs?>");
			break;
		case 3: //upDownStopSwitch
		case 4: //upDownStopSwitchWithPulseButtons
			$("#tr_in_cu").addClass("hidden");
			$("#tr_in_motor_up").removeClass("hidden");
			$("#tr_in_motor_down").removeClass("hidden");
			$("#tr_in_indicator").addClass("hidden");
            $("#tr_in_cu_color_r").addClass("hidden");
            $("#tr_in_cu_color_g").addClass("hidden");
            $("#tr_in_cu_color_b").addClass("hidden");
            $("#tr_in_cu_color_w").addClass("hidden");
            $("#tr_in_r_lum").addClass("hidden");
            $("#tr_in_g_lum").addClass("hidden");
            $("#tr_in_b_lum").addClass("hidden");
            $("#tr_in_w_lum").addClass("hidden");

			var outputVals = ("<?=$outputs?>").split(",");
			$("#in_motor_down").val(outputVals[0]);
			$("#in_motor_up").val(outputVals[1]);
			break;
		case 5: //switchWithIndicator
			$("#tr_in_cu").removeClass("hidden");
			$("#tr_in_motor_up").addClass("hidden");
			$("#tr_in_motor_down").addClass("hidden");
			$("#tr_in_indicator").removeClass("hidden");
            $("#tr_in_cu_color_r").addClass("hidden");
            $("#tr_in_cu_color_g").addClass("hidden");
            $("#tr_in_cu_color_b").addClass("hidden");
            $("#tr_in_cu_color_w").addClass("hidden");
            $("#tr_in_r_lum").addClass("hidden");
            $("#tr_in_g_lum").addClass("hidden");
            $("#tr_in_b_lum").addClass("hidden");
            $("#tr_in_w_lum").addClass("hidden");

			var outputVals = ("<?=$outputs?>").split(",");
			$("#in_cu").val(outputVals[0]);
			$("#in_indicator").val(outputVals[1]);
			break;
        case 7: //color RGB
            $("#tr_in_cu").addClass("hidden");
            $("#tr_in_motor_up").addClass("hidden");
            $("#tr_in_motor_down").addClass("hidden");
            $("#tr_in_indicator").addClass("hidden");
            $("#tr_in_cu_color_r").removeClass("hidden");
            $("#tr_in_cu_color_g").removeClass("hidden");
            $("#tr_in_cu_color_b").removeClass("hidden");
            $("#tr_in_cu_color_w").addClass("hidden");
            $("#tr_in_r_lum").removeClass("hidden");
            $("#tr_in_g_lum").removeClass("hidden");
            $("#tr_in_b_lum").removeClass("hidden");
            $("#tr_in_w_lum").addClass("hidden");

            var outputVals = ("<?=$outputs?>").split(",");
            $("#in_cu_color_r").val(outputVals[0]);
            $("#in_cu_color_g").val(outputVals[1]);
            $("#in_cu_color_b").val(outputVals[2]);
            $("#in_r_lum").val(outputVals[3]);
            $("#in_g_lum").val(outputVals[4]);
            $("#in_b_lum").val(outputVals[5]);
            break;
        case 8: //color RGBW
            $("#tr_in_cu").addClass("hidden");
            $("#tr_in_motor_up").addClass("hidden");
            $("#tr_in_motor_down").addClass("hidden");
            $("#tr_in_indicator").addClass("hidden");
            $("#tr_in_cu_color_r").removeClass("hidden");
            $("#tr_in_cu_color_g").removeClass("hidden");
            $("#tr_in_cu_color_b").removeClass("hidden");
            $("#tr_in_cu_color_w").removeClass("hidden");
            $("#tr_in_r_lum").removeClass("hidden");
            $("#tr_in_g_lum").removeClass("hidden");
            $("#tr_in_b_lum").removeClass("hidden");
            $("#tr_in_w_lum").removeClass("hidden");

            var outputVals = ("<?=$outputs?>").split(",");
            $("#in_cu_color_r").val(outputVals[0]);
            $("#in_cu_color_g").val(outputVals[1]);
            $("#in_cu_color_b").val(outputVals[2]);
            $("#in_cu_color_w").val(outputVals[3]);
            $("#in_r_lum").val(outputVals[4]);
            $("#in_g_lum").val(outputVals[5]);
            $("#in_b_lum").val(outputVals[6]);
            $("#in_w_lum").val(outputVals[7]);
            break;
	}
});
</script>
