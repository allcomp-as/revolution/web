<style>
span input[type=radio] {
	position: absolute;
	visibility: hidden;
}

span label {
	display: inline-block;
	font-size: 12px;
	padding: 10px 15px;
	cursor: pointer;
	-webkit-transition: all 0.25s linear;
	background-color: #555;
	color: #dbd9d9;
	font-style: italic;
}

span:first-child label {
	-webkit-border-top-left-radius: 10px;
	-webkit-border-bottom-left-radius: 10px;
	-moz-border-radius-topleft: 10px;
	-moz-border-radius-bottomleft: 10px;
	border-top-left-radius: 10px;
	border-bottom-left-radius: 10px;
}

span:last-child label {
	-webkit-border-top-right-radius: 10px;
	-webkit-border-bottom-right-radius: 10px;
	-moz-border-radius-topright: 10px;
	-moz-border-radius-bottomright: 10px;
	border-top-right-radius: 10px;
	border-bottom-right-radius: 10px;
}

button {
	background-color: #555;
	color: #dbd9d9;
	width: 130px;
	height: 40px;
	line-height: 30px;
	text-align: center;
	border: none;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
	font-size: 15px;
	margin-top: 15px;
	cursor: pointer;
	margin-right: 5px;
	-webkit-transition: all 0.25s linear;
	-webkit-box-shadow: inset 0px -3px 33px 0px rgba(0,0,0,0.75);
	-moz-box-shadow: inset 0px -3px 33px 0px rgba(0,0,0,0.75);
	box-shadow: inset 0px -3px 33px 0px rgba(0,0,0,0.75);
}

button:hover {
	color: #ffffff;
	background-color: #333;
}

span label:hover {
	color: #ffffff;
}

input[type=radio]:checked ~ label{
	background-color: #333;
	color: #ffffff;
}

input[type="number"], input[type="text"], textarea {
	color: #ffffff;
	background-color: #383b3f;
	width: 100px;
	border: none;
	padding: 10px 15px;
	font-size: 12px;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
}

textarea {
	width: 413px;
	height: 50px;
}

input[type="text"] {
	width: 150px;
}

input[type="checkbox"] {
	display:none;
}

input[type=checkbox]#in_cu_simulate + label {
	background: url('res/img/occupied.png');
	background-size: 20px 20px;
	background-repeat: no-repeat;
	display: inline-block;
	width: 20px;
	height: 20px;
	cursor: pointer;
	-webkit-transition: all 0.25s linear;
}

input[type=checkbox]#in_cu_simulate:checked + label {
	background: url('res/img/ok.png');
	background-size: 20px 20px;
	background-repeat: no-repeat;
}

p.security_check_unit input[type=checkbox] + label {
	background: url('res/img/security.png');
	background-size: 20px 20px;
	background-repeat: no-repeat;
	display:inline-block;
	padding: 0 0 0 25px;
	width: 100%;
	height: 100%;
	cursor: pointer;
	font-size: 12px;
	color: #dbd9d9;
	font-style: italic;
	-webkit-transition: all 0.25s linear;
}

p.security_check_unit input[type=checkbox] + label:hover {
	color: #ffffff;
}

p.security_check_unit input[type=checkbox]:checked + label {
	background: url('res/img/security_filled.png');
	background-size: 20px 20px;
	background-repeat: no-repeat;
	color: #ffffff;
}

p.security_check_unit {
	display: inline-block;
	width: 200px;
	padding: 10px;
	background-color: #383b3f;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
	line-height: 20px;
}

div#security_sectors_array {
	line-height: 45px;
}

select, select option {
	font-size: 12px;
	border: none;
	color: #ffffff;
	background-color: #383b3f;
	padding: 10px 15px;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
}

table#options_table {
	border: none;
	width: 100%;
	border-collapse: separate;
	border-spacing: 5px;
	-webkit-touch-callout: none;
	-webkit-user-select: none;
	-khtml-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

table#options_table tr td {
	vertical-align: top;
}

table#options_table tr td:nth-child(1) {
	width: 30%;
}

table#options_table tr td:nth-child(2) {
	width: 70%;
}

img#swid_state {
	width: 20px;
	height: 20px;
	transform: translate(0, 5px);
}
</style>
<div>
	<table id="options_table">
		<tr>
			<td><?=$_DICTIONARY["id"]?>: </td>
			<td><input id="in_software_address" type="number" /> <img id="swid_state" src="res/img/ok.png" /> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["id"]?></b></p>"+
				"<p><?=$_DICTIONARY["id_desc2"]?></p>"
			);</script></td>
		</tr>
		<tr>
			<td><?=$_DICTIONARY["unit_type"]?>: </td>
			<td>
				<span><input type="radio" id="l_cu_hw_type1" name="cu_hw_type" value="physical" checked /><!--
				--><label for="l_cu_hw_type1"><?=$_DICTIONARY["physical"]?></label></span><!--
				--><span><input type="radio" id="l_cu_hw_type2" name="cu_hw_type" value="virtual" /><!--
				--><label for="l_cu_hw_type2"><?=$_DICTIONARY["virtual"]?></label></span> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["unit_type"]?></b></p>"+
				"<p><?=$_DICTIONARY["unit_type_desc"]?></p>"
			);</script>
			</td>
		</tr>
		<tr>
			<td><?=$_DICTIONARY["hw_addr"]?>: </td>
			<td>
				<div id="cu_hw_physical_address">
					<p><input placeholder="<?=$_DICTIONARY["unit_id"]?>" id="in_cu_ewc_id" type="number" min="1" maxlength="2" max="99" step="1"  onchange="if(parseInt(this.value,10)<10)this.value='0'+this.value; if(parseInt(this.value,10)<0) this.value='0'; if(parseInt(this.value,10)>99) this.value='99';" /></p>
					<p style="margin-top: 5px;">
						<span><input id="l_ewc_version1" type="radio" name="ewc_version" value="2" checked /><label for="l_ewc_version1"><?=$_DICTIONARY["new_version"]?></label></span><!--
						--><span><input id="l_ewc_version2" type="radio" name="ewc_version" value="1" /><label for="l_ewc_version2"><?=$_DICTIONARY["old_version"]?></label></span>
					</p>
					<p style="margin-top: 5px; display: inline-block;">
						<span><input id="l_cu_io_type1" type="radio" name="cu_io_type" value="input" checked /><label for="l_cu_io_type1"><?=$_DICTIONARY["input"]?></label></span><!--
						--><span><input id="l_cu_io_type2"type="radio" name="cu_io_type" value="output" /><label for="l_cu_io_type2"><?=$_DICTIONARY["output"]?></label></span>
					</p>
					<p style="display: inline-block"><select id="cu_io_id">
						<option value="16">16 (<?=$_DICTIONARY["pin"]?> 1)</option>
						<option value="17">17 (<?=$_DICTIONARY["pin"]?> 2)</option>
						<option value="18">18 (<?=$_DICTIONARY["pin"]?> 3)</option>
						<option value="19">19 (<?=$_DICTIONARY["pin"]?> 4)</option>
						<option value="20">20 (<?=$_DICTIONARY["pin"]?> 5)</option>
						<option value="21">21 (<?=$_DICTIONARY["pin"]?> 6)</option>
						<option value="22">22 (<?=$_DICTIONARY["pin"]?> 7)</option>
						<option value="23">23 (<?=$_DICTIONARY["pin"]?> 8)</option>
						<option value="25">25 (<?=$_DICTIONARY["pin"]?> 9)</option>
						<option value="24">24 (<?=$_DICTIONARY["pin"]?> 10)</option>
					</select> <script>SHS.docs(16,
						"<p><b><?=$_DICTIONARY["hw_addr"]?></b></p>"+
						"<p><?=$_DICTIONARY["hw_addr_desc_physical"]?></p>"
					);</script></p>
				</div>
				<div id="cu_hw_virtual_address" class="hidden">
					<p><input id="in_cu_vio_id" type="number" placeholder="<?=$_DICTIONARY["io_id"]?>" min="0" step="1" /> <script>SHS.docs(16,
						"<p><b><?=$_DICTIONARY["hw_addr"]?></b></p>"+
						"<p><?=$_DICTIONARY["hw_addr_desc_virtual"]?></p>"
					);</script></p>
				</div>
			</td>
		</tr>
		<tr>
			<td><?=$_DICTIONARY["value_type"]?>: </td>
			<td><select id="cu_value_type">
				<option value="0"><?=$_DICTIONARY["digital"]?></option>
				<option value="1"><?=$_DICTIONARY["temperature"]?></option>
				<option value="2"><?=$_DICTIONARY["pwm"]?></option>
				<option value="3"><?=$_DICTIONARY["rfid"]?></option>
			</select> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["value_type"]?></b></p>"+
				"<p><?=$_DICTIONARY["value_type_desc2"]?></p>"
			);</script></td>
		</tr>
		<tr id="security_sectors_check_tr">
			<td><?=$_DICTIONARY["security_sectors"]?>: </td>
			<td><div id="security_sectors_array"></div> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["security_sectors"]?></b></p>"+
				"<p><?=$_DICTIONARY["security_sectors_desc"]?></p>"
			);</script></td>
		</tr>
		<tr>
			<td><?=$_DICTIONARY["description"]?>: </td>
			<td><textarea id="cu_description"></textarea> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["description"]?></b></p>"+
				"<p><?=$_DICTIONARY["description_desc"]?></p>"
			);</script></td>
		</tr>
		<tr>
			<td><?=$_DICTIONARY["active_state_value"]?>: </td>
			<td><input id="in_cu_active_state" type="number" value="1" /> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["active_state_value"]?></b></p>"+
				"<p><?=$_DICTIONARY["active_state_value_desc"]?></p>"
			);</script></td>
		</tr>
		<tr>
			<td><?=$_DICTIONARY["enable_holidays_mode"]?>: </td>
			<td><input type="checkbox" id="in_cu_simulate" checked /><label for="in_cu_simulate">&nbsp;</label></td>
		</tr>
		<tr>
			<td><?=$_DICTIONARY["activation_delay"]?>: </td>
			<td><input id="in_cu_activation_delay" type="number" value="0" min="0" step="50" /> ms</td>
		</tr>
		<tr id="tr_metadata_t2_pwm_min">
			<td><?=$_DICTIONARY["pwm_min"]?>: </td>
			<td><input id="in_t2_pwm_min" type="number" min="0" max="100" step="1" value="0" /> % <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["pwm_min"]?></b></p>"+
				"<p><?=$_DICTIONARY["pwm_min_desc"]?></p>"
			);</script></td>
		</tr>
		<tr id="tr_metadata_t2_pwm_max">
			<td><?=$_DICTIONARY["pwm_max"]?>: </td>
			<td><input id="in_t2_pwm_max" type="number" min="0" max="100" step="1" value="100" /> % <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["pwm_max"]?></b></p>"+
				"<p><?=$_DICTIONARY["pwm_max_desc"]?></p>"
			);</script></td>
		</tr>
		<tr>
			<td></td>
			<td><button id="in_save"><?=$_DICTIONARY["save"]?></button><button id="in_cancel"><?=$_DICTIONARY["cancel"]?></button></td>
		</tr>
	</table>
</div>
<script>
$(document).ready(function() {
	for(var i = 0; i <= 3; i++)
		$("tr[id*='tr_metadata_t"+i+"']").addClass("hidden");

	$("select#cu_value_type").change(function() {
		$("tr[id*='tr_metadata_t']").removeClass("hidden");
		for(var i = 0; i <= 3; i++)
			if(Number($(this).val()) != i)
				$("tr[id*='tr_metadata_t"+i+"']").addClass("hidden");
	});

	$.get("phpscript/getLastCUID.php", function(data) {
		$("input#in_software_address").val(Number(data)+1);
	});

	$.get("phpscript/genSecuritySectorsCheckArray.php", function(data) {
		$("div#security_sectors_array").html(data);
	});

	var updateSelects = function() {
		var values = [];
        var printPWM = false;
		if($("input[type=radio][name=\"cu_io_type\"]:checked").val() == "input") {
			if($("tr#security_sectors_check_tr").hasClass("hidden"))
				$("tr#security_sectors_check_tr").removeClass("hidden");

			if($("input[type=radio][name=\"ewc_version\"]:checked").val() == "2")
				values = [16,17,18,19,20,21,22,23,25,24];
			else if($("input[type=radio][name=\"ewc_version\"]:checked").val() == "1")
				values = [24,25,16,17,18,19,20,21,23,22];
		} else if($("input[type=radio][name=\"cu_io_type\"]:checked").val() == "output") {
			if(!$("tr#security_sectors_check_tr").hasClass("hidden"))
				$("tr#security_sectors_check_tr").addClass("hidden");

			if($("input[type=radio][name=\"ewc_version\"]:checked").val() == "2") {
				values = [0,1,2,3,4,5,6,7,8,9];
                printPWM = true;
            } else if($("input[type=radio][name=\"ewc_version\"]:checked").val() == "1")
				values = [0,5,1,6,2,7,3,8,4,9];
		}

		var optionStr = "";
		for(var i = 0; i < 10; i++)
			optionStr += "<option value=\""+values[i]+"\">"+values[i]+" (pin "+(i+1)+ (printPWM && i < 5 ? " / PWM"+(i+1) : "")+")</option>\n";
		$("select#cu_io_id").html(optionStr);
	};

	$("input[type='radio'][name='cu_io_type']").change(updateSelects);
	$("input[type='radio'][name='ewc_version']").change(updateSelects);

	$("input[type='radio'][name='cu_hw_type']").change(function() {
		if($("input[type=radio][name=\"cu_hw_type\"]:checked").val() == "physical") {
			if($("div#cu_hw_physical_address").hasClass("hidden"))
				$("div#cu_hw_physical_address").removeClass("hidden");
			if(!$("div#cu_hw_virtual_address").hasClass("hidden"))
				$("div#cu_hw_virtual_address").addClass("hidden");
			if($("input[type=radio][name=\"cu_io_type\"]:checked").val() == "input")
				if($("tr#security_sectors_check_tr").hasClass("hidden"))
					$("tr#security_sectors_check_tr").removeClass("hidden");
			if($("input[type=radio][name=\"cu_io_type\"]:checked").val() == "output")
				if(!$("tr#security_sectors_check_tr").hasClass("hidden"))
					$("tr#security_sectors_check_tr").addClass("hidden");
		} else if($("input[type=radio][name=\"cu_hw_type\"]:checked").val() == "virtual") {
			if($("div#cu_hw_virtual_address").hasClass("hidden"))
				$("div#cu_hw_virtual_address").removeClass("hidden");
			if(!$("div#cu_hw_physical_address").hasClass("hidden"))
				$("div#cu_hw_physical_address").addClass("hidden");
			if($("tr#security_sectors_check_tr").hasClass("hidden"))
				$("tr#security_sectors_check_tr").removeClass("hidden");
		}
	});

	$("input#in_software_address").change(function() {
		$.get("phpscript/getCUCountWithSWID.php?software_address="+$("input#in_software_address").val(), function(data) {
			var count = Number(data);
			if(count >= 1)
				$("img#swid_state").attr("src", "res/img/occupied.png");
			else
				$("img#swid_state").attr("src", "res/img/ok.png");
		});

	});

	$("button#in_cancel").click(function() {
		location.href = "?page=control_units";
	});
	$("button#in_save").click(function() {
		var software_address = Number($("input#in_software_address").val());
		if($("img#swid_state").attr("src") == "res/img/occupied.png") {
			alert("<?=$_DICTIONARY["duplicite_cuid"]?>");
			return;
		}
		var hardware_address = "";
		if($("input[type=radio][name=\"cu_hw_type\"]:checked").val() == "physical") {
			if(isNaN($("input#in_cu_ewc_id").val()) || $("input#in_cu_ewc_id").val() == "") {
				alert("<?=$_DICTIONARY["invalid_cuid"]?>");
				return;
			}
			var iotype = $("input[type=radio][name=\"cu_io_type\"]:checked").val() == "input" ? "IN" : "OUT";
			hardware_address = "EWC" + $("input#in_cu_ewc_id").val() + "_" + iotype + $("select#cu_io_id").val();
		} else {
			if(isNaN($("input#in_cu_vio_id").val()) || $("input#in_cu_vio_id").val() == "") {
				alert("<?=$_DICTIONARY["invalid_virtid"]?>");
				return;
			}
			hardware_address = "VIRTUAL" + $("input#in_cu_vio_id").val();
		}
		var value_type = Number($("select#cu_value_type").val());
		var metadata = "";
		switch(value_type) {
			case 2:
				var pwmMin = $("#in_t2_pwm_min").val();
				var pwmMax = $("#in_t2_pwm_max").val();
				if(pwmMin != "0") metadata += ";pwm_min=" + pwmMin;
				if(pwmMax != "100") metadata += ";pwm_max=" + pwmMax;
				break;
		}

		if(metadata.length > 0)
			metadata = metadata.substring(1);

		var security = "";
		if($("input[type=radio][name=\"cu_io_type\"]:checked").val() == "input" || $("input[type=radio][name=\"cu_hw_type\"]:checked").val() == "virtual") {
			var securityElements = $("div#security_sectors_array p input[type='checkbox']").toArray();
			for (var i in securityElements) {
				if($(securityElements[i]).is(':checked'))
					security += ","+$(securityElements[i]).data("sectorid");
			}
		}
		if(security.length > 0)
			security = security.substring(1);
		var description = $("textarea#cu_description").val();
		var active_state = Number($("input#in_cu_active_state").val());
		var simulate = $("input#in_cu_simulate").is(':checked') ? "1" : "0";
		var activation_delay = Number($("input#in_cu_activation_delay").val());
		var ewc_version = $("input[type=radio][name=\"cu_hw_type\"]:checked").val() == "virtual" ? 0 : Number($("input[type=radio][name=\"ewc_version\"]:checked").val());

		var urlGetData = "software_address="+software_address+"&hardware_address="+hardware_address+"&value_type="+value_type
						+"&security="+security+"&description="+description+"&active_state="+active_state+"&simulate="+simulate
						+"&activation_delay="+activation_delay+"&ewc_version="+ewc_version+"&metadata="+metadata;
		$.get("phpscript/insertNewCU.php?"+urlGetData, function(data) {
			if(data.trim() == "OK")
				location.href = "?page=control_units";
			else
				alert("<?=$_DICTIONARY["save_ncu_fail"]?>");
		});
	});
});
</script>
