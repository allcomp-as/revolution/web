<?php
$roomID = $httpRequest->getQuery('id');
$row = $dbWeb->fetch("
	SELECT
		name,
		floor
	FROM rooms
	WHERE
		id = ?
	",
	$roomID
);

if(!$row){
	return;
}
?>
<style>
button {
	background-color: #555;
	color: #dbd9d9;
	width: 130px;
	height: 40px;
	line-height: 30px;
	text-align: center;
	border: none;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
	font-size: 15px;
	margin-top: 15px;
	cursor: pointer;
	margin-right: 5px;
	-webkit-transition: all 0.25s linear;
	-webkit-box-shadow: inset 0px -3px 33px 0px rgba(0,0,0,0.75);
	-moz-box-shadow: inset 0px -3px 33px 0px rgba(0,0,0,0.75);
	box-shadow: inset 0px -3px 33px 0px rgba(0,0,0,0.75);
}

button:hover {
	color: #ffffff;
	background-color: #333;
}

input[type="number"], textarea {
	color: #ffffff;
	background-color: #383b3f;
	width: 100px;
	border: none;
	padding: 10px 15px;
	font-size: 12px;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
}

textarea {
	width: 413px;
	height: 50px;
}

table#options_table {
	border: none;
	width: 100%;
	border-collapse: separate;
	border-spacing: 5px;
	-webkit-touch-callout: none;
	-webkit-user-select: none;
	-khtml-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

table#options_table tr td {
	vertical-align: top;
}

table#options_table tr td:nth-child(1) {
	width: 30%;
}

table#options_table tr td:nth-child(2) {
	width: 70%;
}
</style>

<?php
	$name = $row[0];
	$floor = $row[1];
?>
<div>
	<table id="options_table">
		<tr>
			<td><?=$_DICTIONARY["name"]?>: </td>
			<td><textarea id="in_room_name"><?=$name?></textarea></td>
		</tr>
		<tr>
			<td><?=$_DICTIONARY["floor"]?>: </td>
			<td><input id="in_room_floor" type="number" value="<?=$floor?>" /></td>
		</tr>
		<tr>
			<td></td>
			<td><button id="in_save"><?=$_DICTIONARY["save"]?></button><button id="in_cancel"><?=$_DICTIONARY["cancel"]?></button></td>
		</tr>
	</table>
</div>
<script>
$(document).ready(function() {
	$("button#in_cancel").click(function() {
		location.href = "?page=rooms";
	});
	$("button#in_save").click(function() {
		var id = "<?=$roomID?>";
		var name = $("#in_room_name").val();
		var floor = $("#in_room_floor").val();
		var urlGetData = "id="+id+"&name="+name+"&floor="+floor;
		$.get("phpscript/updateRoom.php?"+urlGetData, function(data) {
			if(data.trim() == "OK")
				location.href = "?page=rooms";
			else
				alert("<?=$_DICTIONARY["save_croom_fail"]?>");
		});
	});
});
</script>
