<?php
$macroID = $httpRequest->getQuery('id');
$rowM = $dbWeb->fetch("
	SELECT
		name,
		command
	FROM macros
	WHERE
		id = ?
	",
	$macroID
);

if(!$rowM){
	return;
}

$rsCUs = $dbServer->fetchAll("SELECT software_address, description FROM control_units ORDER BY software_address ASC");
?>
<style>
button {
	background-color: #555;
	color: #dbd9d9;
	width: 130px;
	height: 40px;
	line-height: 30px;
	text-align: center;
	border: none;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
	font-size: 15px;
	margin-top: 15px;
	cursor: pointer;
	margin-right: 5px;
	-webkit-transition: all 0.25s linear;
	-webkit-box-shadow: inset 0px -3px 33px 0px rgba(0,0,0,0.75);
	-moz-box-shadow: inset 0px -3px 33px 0px rgba(0,0,0,0.75);
	box-shadow: inset 0px -3px 33px 0px rgba(0,0,0,0.75);
}

button:hover {
	color: #ffffff;
	background-color: #333;
}

input[type="number"], textarea {
	color: #ffffff;
	background-color: #383b3f;
	width: 100px;
	border: none;
	padding: 10px 15px;
	font-size: 12px;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
}

textarea {
	width: 413px;
	height: 50px;
}

select, select option {
	font-size: 12px;
	border: none;
	color: #ffffff;
	background-color: #383b3f;
	padding: 10px 15px;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
}

input[type="checkbox"] {
	display:none;
}

input[type=checkbox] + label {
	background: url('res/img/occupied.png');
	background-size: 20px 20px;
	background-repeat: no-repeat;
	display: inline-block;
	width: 20px;
	height: 20px;
	cursor: pointer;
	-webkit-transition: all 0.25s linear;
}

input[type=checkbox]:checked + label {
	background: url('res/img/ok.png');
	background-size: 20px 20px;
	background-repeat: no-repeat;
}

table#options_table {
	border: none;
	width: 100%;
	border-collapse: separate;
	border-spacing: 5px;
	-webkit-touch-callout: none;
	-webkit-user-select: none;
	-khtml-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

table#options_table tr td {
	vertical-align: top;
}

table#options_table tr td:nth-child(1) {
	width: 30%;
}

table#options_table tr td:nth-child(2) {
	width: 70%;
}

img.macro_delete {
	cursor: pointer;
}
</style>

<?php
	$name = $rowM[0];
	$command = $rowM[1];
?>
<div>
	<table id="options_table">
		<tr>
			<td><?=$_DICTIONARY["name"]?>: </td>
			<td><textarea id="in_macro_name"><?=$name;?></textarea></td>
		</tr>
		<tr id="command_tr_master" style="display: none;">
			<td><?=$_DICTIONARY["command"]?> #<span class="cmd_id"></span>: <img class="macro_delete" title="delete" src="res/img/close_dark.png" /></td>
			<td>
				<table>
					<tr>
						<td style="width: 500px;"><?=$_DICTIONARY["type"]?></td>
						<td><select class="in_macro_type">
							<option value="value" selected><?=$_DICTIONARY["state_change"]?></option>
							<option value="pulse"><?=$_DICTIONARY["pulse"]?></option>
						</select></td>
					</tr>
					<tr>
						<td><?=$_DICTIONARY["control_unit"]?></td>
						<td><select class="in_cu">
							<?php foreach($rsCUs as $row){ ?>
								<option value="<?=$row[0]?>"><?=$row[0]?><?=($row[1]=="" ? "" : " > ".$row[1])?></option>
							<?php }; ?>
						</select></td>
					</tr>
					<tr class="type_state_change">
						<td><?=$_DICTIONARY["state"]?></td>
						<td><input class="in_state" type="number" value="0" /></td>
					</tr>
					<tr class="type_pulse" style="display: none;">
						<td><?=$_DICTIONARY["state_on"]?></td>
						<td><input class="in_state_on" type="number" value="1" /></td>
					</tr>
					<tr class="type_pulse" style="display: none;">
						<td><?=$_DICTIONARY["state_off"]?></td>
						<td><input class="in_state_off" type="number" value="0" /></td>
					</tr>
					<tr class="type_pulse" style="display: none;">
						<td><?=$_DICTIONARY["repeat_count"]?></td>
						<td><input class="in_repeat_count" type="number" value="1" /></td>
					</tr>
					<tr class="type_pulse" style="display: none;">
						<td><?=$_DICTIONARY["duration"]?></td>
						<td><input class="in_duration" type="number" value="1000" /> <?=$_DICTIONARY["ms"]?></td>
					</tr>
					<tr>
						<td><?=$_DICTIONARY["delay"]?></td>
						<td><input class="in_delay" type="number" value="0" /> <?=$_DICTIONARY["ms"]?></td>
					</tr>
					<tr class="type_pulse" style="display: none;">
						<td><?=$_DICTIONARY["block_motion_sensor"]?></td>
						<td><input class="in_block_motion_sensor" type="checkbox" id="in_block_motion_sensor" checked /><label for="in_block_motion_sensor">&nbsp;</label></td>
					</tr>
				</table>
			</td>
		</tr>
		<?php $cntr = 1; $cmds = explode(";", $command); foreach($cmds as $cmd) :?>
		<?php
			$keyvals = array();
			$pairs = explode(",", $cmd);
			foreach($pairs as $pair) {
				$kvs = explode("=", $pair);
				$keyvals[$kvs[0]] = $kvs[1];
			}
			if(!isset($keyvals["type"]))
				continue;
			$type = $keyvals["type"];
			$cu = isset($keyvals["cu"]) ? $keyvals["cu"] : 0;
			$delay = isset($keyvals["delay"]) ? $keyvals["delay"] : 0;
			$state = isset($keyvals["value"]) ? $keyvals["value"] : 0;
			$stateOn = isset($keyvals["valueOn"]) ? $keyvals["valueOn"] : 1;
			$stateOff = isset($keyvals["valueOff"]) ? $keyvals["valueOff"] : 0;
			$repeat = isset($keyvals["repeat"]) ? $keyvals["repeat"] : 1;
			$duration = isset($keyvals["duration"]) ? $keyvals["duration"] : 1000;
			$blockMotionSensor = isset($keyvals["blockMotionSensor"]) ? $keyvals["blockMotionSensor"] : "true";
		?>
		<tr class="in_command" style="display: table-row;">
			<td><?=$_DICTIONARY["command"]?> #<span class="cmd_id"><?=$cntr?></span>: <img class="macro_delete" title="delete" src="res/img/close_dark.png" /></td>
			<td>
				<table>
					<tr>
						<td style="width: 500px;"><?=$_DICTIONARY["type"]?></td>
						<td><select class="in_macro_type">
							<option value="value"<?=($type == "value" ? " selected" : "")?>><?=$_DICTIONARY["state_change"]?></option>
							<option value="pulse"<?=($type == "pulse" ? " selected" : "")?>><?=$_DICTIONARY["pulse"]?></option>
						</select></td>
					</tr>
					<tr>
						<td><?=$_DICTIONARY["control_unit"]?></td>
						<td><select class="in_cu">
							<?php foreach($rsCUs as $row){ ?>
								<option value="<?=$row[0]?>"<?php if($row[0] == $cu) echo(" selected"); ?>><?=$row[0]?><?=($row[1]=="" ? "" : " > ".$row[1])?></option>
							<?php }; ?>
						</select></td>
					</tr>
					<tr class="type_state_change" style="display: <?=($type == "value" ? "table-row" : "none")?>;">
						<td><?=$_DICTIONARY["state"]?></td>
						<td><input class="in_state" type="number" value="<?=$state?>" /></td>
					</tr>
					<tr class="type_pulse" style="display: <?=($type == "pulse" ? "table-row" : "none")?>;">
						<td><?=$_DICTIONARY["state_on"]?></td>
						<td><input class="in_state_on" type="number" value="<?=$stateOn?>" /></td>
					</tr>
					<tr class="type_pulse" style="display: <?=($type == "pulse" ? "table-row" : "none")?>;">
						<td><?=$_DICTIONARY["state_off"]?></td>
						<td><input class="in_state_off" type="number" value="<?=$stateOff?>" /></td>
					</tr>
					<tr class="type_pulse" style="display: <?=($type == "pulse" ? "table-row" : "none")?>;">
						<td><?=$_DICTIONARY["repeat_count"]?></td>
						<td><input class="in_repeat_count" type="number" value="<?=$repeat?>" /></td>
					</tr>
					<tr class="type_pulse" style="display: <?=($type == "pulse" ? "table-row" : "none")?>;">
						<td><?=$_DICTIONARY["duration"]?></td>
						<td><input class="in_duration" type="number" value="<?=$duration?>" /> <?=$_DICTIONARY["ms"]?></td>
					</tr>
					<tr>
						<td><?=$_DICTIONARY["delay"]?></td>
						<td><input class="in_delay" type="number" value="<?=$delay?>" /> <?=$_DICTIONARY["ms"]?></td>
					</tr>
					<tr class="type_pulse" style="display: <?=($type == "pulse" ? "table-row" : "none")?>;">
						<td><?=$_DICTIONARY["block_motion_sensor"]?></td>
						<td><input class="in_block_motion_sensor" type="checkbox" id="in_block_motion_sensor<?=$cntr?>"<?php if($blockMotionSensor == "true") echo(" checked");?> /><label for="in_block_motion_sensor<?=$cntr?>">&nbsp;</label></td>
					</tr>
				</table>
			</td>
		</tr>
		<?php $cntr++; ?>
		<?php endforeach; ?>
		<tr id="btns_tr">
			<td></td>
			<td><button id="in_add_cmd"><?=$_DICTIONARY["add_command"]?></button><button id="in_save"><?=$_DICTIONARY["save"]?></button><button id="in_cancel"><?=$_DICTIONARY["cancel"]?></button></td>
		</tr>
	</table>
</div>

<script>
var cmd_counter = <?=$cntr?>;
$(document).ready(function() {
	$("button#in_cancel").click(function() {
		location.href = "?page=macros";
	});
	$("button#in_save").click(function() {
		var name = $("#in_macro_name").val();
		var command = "";

		$("#options_table").find(".in_command").each(function(idx) {
			if(idx != 0)
				command += ";";
			var elem = $(this);
			var type = elem.find(".in_macro_type").val();
			var cu = elem.find(".in_cu").val();
			var delay = elem.find(".in_delay").val();
			command += "type=" + type + ",cu=" + cu + ",delay=" + delay + ",";
			if(type == "value") {
				var state = elem.find(".in_state").val();
				command += "value=" + state;
			} else { //pulse
				var stateOn = elem.find(".in_state_on").val();
				var stateOff = elem.find(".in_state_off").val();
				var repeat = elem.find(".in_repeat_count").val();
				var duration = elem.find(".in_duration").val();
				var blockMotionSensor = elem.find(".in_block_motion_sensor").is(":checked") ? "true" : "false";
				command += "valueOn="+stateOn+",valueOff="+stateOff+",repeat="+repeat+",duration="+duration+",blockMotionSensor="+blockMotionSensor;
			}
		});

		var urlGetData = "id=<?=$macroID?>&name="+name+"&command="+encodeURI(command.replaceAll("=", "[eq]"));
		$.get("phpscript/updateMacro.php?"+urlGetData, function(data) {
			if(data.trim() == "OK")
				location.href = "?page=macros";
			else
				alert("<?=$_DICTIONARY["save_cmacro_fail"]?>");
		});
	});

	$("button#in_add_cmd").click(function() {
		var elem = $("#command_tr_master").clone();
		elem.detach();
		elem.removeAttr("id");
		elem.css("display", "table-row");
		elem.addClass("in_command");
		elem.find(".cmd_id").html(cmd_counter);
		elem.insertBefore("#btns_tr");
		elem.find("img.macro_delete").click(function() {
			elem.remove();
		});
		elem.find("select.in_macro_type").change(function() {
			var elem = $(this);
			var parentElem = elem.parent().parent().parent();
			if(elem.val() == "value") {
				parentElem.find(".type_state_change").css("display", "table-row")
				parentElem.find(".type_pulse").css("display", "none");
			} else {
				parentElem.find(".type_state_change").css("display", "none")
				parentElem.find(".type_pulse").css("display", "table-row");
			}
		});
		elem.find("#in_block_motion_sensor").attr("id", "in_block_motion_sensor" + cmd_counter);
		elem.find("label[for='in_block_motion_sensor']").attr("for", "in_block_motion_sensor" + cmd_counter);

		cmd_counter++;
	});

	$("img.macro_delete").click(function() {
		$(this).parent().parent().remove();
	});

	$("select.in_macro_type").change(function() {
		var elem = $(this);
		var parentElem = elem.parent().parent().parent();
		if(elem.val() == "value") {
			parentElem.find(".type_state_change").css("display", "table-row")
			parentElem.find(".type_pulse").css("display", "none");
		} else {
			parentElem.find(".type_state_change").css("display", "none")
			parentElem.find(".type_pulse").css("display", "table-row");
		}
	});
});
</script>
