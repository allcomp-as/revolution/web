<?php
$teID = $httpRequest->getQuery('id');
$rowTE = $dbServer->fetch("
	SELECT
		`trigger`,
		target,
		type,
		metadata,
		group_id,
		activation_conditions,
		deactivation_conditions
	FROM trigger_events
	WHERE
		id = ?
	",
	$teID
);

if(!$rowTE){
	return;
}

$rsCUs = $dbServer->fetchAll("SELECT software_address, description FROM control_units ORDER BY software_address ASC");
?>
<style>
span input[type=radio] {
	position: absolute;
	visibility: hidden;
}

span label {
	display: inline-block;
	font-size: 12px;
	padding: 10px 15px;
	cursor: pointer;
	-webkit-transition: all 0.25s linear;
	background-color: #555;
	color: #dbd9d9;
	font-style: italic;
}

span:first-child label {
	-webkit-border-top-left-radius: 10px;
	-webkit-border-bottom-left-radius: 10px;
	-moz-border-radius-topleft: 10px;
	-moz-border-radius-bottomleft: 10px;
	border-top-left-radius: 10px;
	border-bottom-left-radius: 10px;
}

span:last-child label {
	-webkit-border-top-right-radius: 10px;
	-webkit-border-bottom-right-radius: 10px;
	-moz-border-radius-topright: 10px;
	-moz-border-radius-bottomright: 10px;
	border-top-right-radius: 10px;
	border-bottom-right-radius: 10px;
}

button#in_save, button#in_cancel {
	background-color: #555;
	color: #dbd9d9;
	width: 130px;
	height: 40px;
	line-height: 30px;
	text-align: center;
	border: none;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
	font-size: 15px;
	margin-top: 15px;
	cursor: pointer;
	margin-right: 5px;
	-webkit-transition: all 0.25s linear;
	-webkit-box-shadow: inset 0px -3px 33px 0px rgba(0,0,0,0.75);
	-moz-box-shadow: inset 0px -3px 33px 0px rgba(0,0,0,0.75);
	box-shadow: inset 0px -3px 33px 0px rgba(0,0,0,0.75);
}

span label:hover {
	color: #ffffff;
}

input[type=radio]:checked ~ label{
	background-color: #333;
	color: #ffffff;
}

input[type="number"], input[type="text"], textarea {
	color: #ffffff;
	background-color: #383b3f;
	width: 100px;
	border: none;
	padding: 10px 15px;
	font-size: 12px;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
}

textarea {
	width: 413px;
	height: 50px;
}

input[type="text"] {
	width: 150px;
}

input[type="checkbox"] {
	display:none;
}

input[type=checkbox] + label {
	background: url('res/img/occupied.png');
	background-size: 20px 20px;
	background-repeat: no-repeat;
	display: inline-block;
	width: 20px;
	height: 20px;
	cursor: pointer;
	-webkit-transition: all 0.25s linear;
}

input[type=checkbox]:checked + label {
	background: url('res/img/ok.png');
	background-size: 20px 20px;
	background-repeat: no-repeat;
}

select, select option {
	font-size: 12px;
	border: none;
	color: #ffffff;
	background-color: #383b3f;
	padding: 10px 15px;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
}

table#options_table {
	border: none;
	width: 100%;
	border-collapse: separate;
	border-spacing: 5px;
	-webkit-touch-callout: none;
	-webkit-user-select: none;
	-khtml-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

table#options_table tr td {
	vertical-align: top;
}

table#options_table tr td:nth-child(1) {
	width: 30%;
}

table#options_table tr td:nth-child(2) {
	width: 70%;
}

img#swid_state {
	width: 20px;
	height: 20px;
	transform: translate(0, 5px);
}
</style>

<?php
	$trigger = $rowTE[0];
	$target = $rowTE[1];
	$type = $rowTE[2];
	$metadata = $rowTE[3];
	$group_id = $rowTE[4];
	$activation_conditions = $rowTE[5];
	$deactivation_conditions = $rowTE[6];
?>
<div>
	<table id="options_table">
		<tr>
			<td><?=$_DICTIONARY["input"]?>: </td>
			<td><select id="in_trigger">
				<?php foreach($rsCUs as $row){ ?>
					<option value="<?=$row[0]?>"<?php if($trigger==$row[0]) echo(" selected");?>><?=$row[0]?><?=($row[1]=="" ? "" : " > ".$row[1])?></option>
				<?php }; ?>
			</select></td>
		</tr>
		<tr>
			<td><?=$_DICTIONARY["output"]?>: </td>
			<td><select id="in_target">
				<?php foreach($rsCUs as $row){ ?>
					<option value="<?=$row[0]?>"<?php if($target==$row[0]) echo(" selected");?>><?=$row[0]?><?=($row[1]=="" ? "" : " > ".$row[1])?></option>
				<?php }; ?>
			</select></td>
		</tr>
		<tr>
			<td><?=$_DICTIONARY["group"]?>: </td>
			<td><input id="in_group_id" type="number" value="<?=$group_id?>" /> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["group"]?></b></p>"+
				"<p><?=$_DICTIONARY["group_desc0"]?></p>"+
				"<p><?=$_DICTIONARY["group_desc1"]?></p>"
			);</script></td>
		</tr>
		<tr>
			<td><?=$_DICTIONARY["type"]?>: </td>
			<td><select id="in_type">
				<option value="0"<?php if($type=="0") echo(" selected");?>><?=$_DICTIONARY["button"]?></option>
				<option value="1"<?php if($type=="1") echo(" selected");?>><?=$_DICTIONARY["switch"]?></option>
				<option value="2"<?php if($type=="2") echo(" selected");?>><?=$_DICTIONARY["timer"]?></option>
				<option value="3"<?php if($type=="3") echo(" selected");?>><?=$_DICTIONARY["edge_watcher"]?></option>
				<option value="4"<?php if($type=="4") echo(" selected");?>><?=$_DICTIONARY["attenuator"]?></option>
				<option value="5"<?php if($type=="5") echo(" selected");?>><?=$_DICTIONARY["sunblind_button"]?></option>
				<option value="6"<?php if($type=="6") echo(" selected");?>><?=$_DICTIONARY["thermostat"]?></option>
				<option value="7"<?php if($type=="7") echo(" selected");?>><?=$_DICTIONARY["pwm_power_supply"]?></option>
			</select> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["type"]?></b></p>"+
				"<p><?=$_DICTIONARY["type_desc"]?></p>"+
				"<p style=\"margin-top: 10px;\"><b><?=$_DICTIONARY["type_button"]?></b> <?=$_DICTIONARY["type_button_desc"]?></p>"+
				"<p style=\"margin-top: 10px;\"><b><?=$_DICTIONARY["type_switch"]?></b> <?=$_DICTIONARY["type_switch_desc"]?></p>"+
				"<p style=\"margin-top: 10px;\"><b><?=$_DICTIONARY["type_timer"]?></b> <?=$_DICTIONARY["type_timer_desc"]?></p>"+
				"<p style=\"margin-top: 10px;\"><b><?=$_DICTIONARY["type_edge_watcher"]?></b> <?=$_DICTIONARY["type_edge_watcher_desc"]?></p>"+
				"<p style=\"margin-top: 10px;\"><b><?=$_DICTIONARY["type_attenuator"]?></b> <?=$_DICTIONARY["type_attenuator_desc0"]?></p>"+
				"<p><?=$_DICTIONARY["type_attenuator_desc1"]?></p>"+
				"<p><?=$_DICTIONARY["type_attenuator_desc2"]?></p>"+
				"<p style=\"margin-top: 10px;\"><b><?=$_DICTIONARY["type_sunblind_button"]?></b> <?=$_DICTIONARY["type_sunblind_button_desc0"]?></p>"+
				"<p><?=$_DICTIONARY["type_sunblind_button_desc1"]?></p>"+
				"<p><?=$_DICTIONARY["type_sunblind_button_desc2"]?></p>"+
				"<p style=\"margin-top: 10px;\"><b><?=$_DICTIONARY["type_thermostat"]?></b> <?=$_DICTIONARY["type_thermostat_desc"]?></p>"+
				"<p style=\"margin-top: 10px;\"><b><?=$_DICTIONARY["type_pwm_power_supply"]?></b> <?=$_DICTIONARY["type_pwm_power_supply_desc"]?></p>"
			);</script></td>
		</tr>
		<!-- METADATA TYPE 0 -->
		<tr id="tr_metadata_t0_0">
			<td><?=$_DICTIONARY["edge"]?>: </td>
			<td><select id="in_t0_trigger_edge">
				<option value="1" selected><?=$_DICTIONARY["rising"]?></option>
				<option value="0"><?=$_DICTIONARY["falling"]?></option>
			</select></td>
		</tr>
		<tr id="tr_metadata_t0_1">
			<td><?=$_DICTIONARY["delay_activation"]?>: </td>
			<td><input id="in_t0_delayTurnOn" type="number" value="0" /> <?=$_DICTIONARY["ms"]?></td>
		</tr>
		<tr id="tr_metadata_t0_2">
			<td><?=$_DICTIONARY["delay_deactivation"]?>: </td>
			<td><input id="in_t0_delayTurnOff" type="number" value="0" /> <?=$_DICTIONARY["ms"]?></td>
		</tr>
		<tr id="tr_metadata_t0_3">
			<td><?=$_DICTIONARY["input_blocking_duration"]?>: </td>
			<td><input id="in_t0_triggerBlocking" type="number" value="0" /> <?=$_DICTIONARY["ms"]?></td>
		</tr>
		<tr id="tr_metadata_t0_4">
			<td><?=$_DICTIONARY["output_blocking_duration"]?>: </td>
			<td><input id="in_t0_targetBlocking" type="number" value="0" /> <?=$_DICTIONARY["ms"]?></td>
		</tr>
		<tr id="tr_metadata_t0_5">
			<td><?=$_DICTIONARY["input_blocking_delay"]?>: </td>
			<td><input id="in_t0_triggerBlockingDelay" type="number" value="0" /> <?=$_DICTIONARY["ms"]?></td>
		</tr>
		<tr id="tr_metadata_t0_6">
			<td><?=$_DICTIONARY["output_blocking_delay"]?>: </td>
			<td><input id="in_t0_targetBlockingDelay" type="number" value="0" /> <?=$_DICTIONARY["ms"]?></td>
		</tr>
		<tr id="tr_metadata_t0_7">
			<td><?=$_DICTIONARY["block_motion_sensor"]?>: </td>
			<td><input id="in_t0_blockMotionSensor" type="checkbox" /><label for="in_t0_blockMotionSensor">&nbsp;</label> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["block_motion_sensor"]?></b></p>"+
				"<p><?=$_DICTIONARY["block_motion_sensor_desc"]?>.</p>"
			);</script></td>
		</tr>
		<!-- METADATA TYPE 1 -->
		<tr id="tr_metadata_t1_0">
			<td><?=$_DICTIONARY["reversed_switch"]?>: </td>
			<td><input id="in_t1_revesed" type="checkbox" /><label for="in_t1_revesed">&nbsp;</label> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["reversed_switch"]?></b></p>"+
				"<p><?=$_DICTIONARY["reversed_switch_desc"]?></p>"
			);</script></td>
		</tr>
		<tr id="tr_metadata_t1_1">
			<td><?=$_DICTIONARY["delay_activation"]?>: </td>
			<td><input id="in_t1_delayTurnOn" type="number" value="0" /> <?=$_DICTIONARY["ms"]?></td>
		</tr>
		<tr id="tr_metadata_t1_2">
			<td><?=$_DICTIONARY["delay_deactivation"]?>: </td>
			<td><input id="in_t1_delayTurnOff" type="number" value="0" /> <?=$_DICTIONARY["ms"]?></td>
		</tr>
		<tr id="tr_metadata_t1_3">
			<td><?=$_DICTIONARY["change_delay"]?>: </td>
			<td><input id="in_t1_delayChange" type="number" value="0" /> <?=$_DICTIONARY["ms"]?></td>
		</tr>
		<tr id="tr_metadata_t1_4">
			<td><?=$_DICTIONARY["input_blocking_duration"]?>: </td>
			<td><input id="in_t1_triggerBlocking" type="number" value="0" /> <?=$_DICTIONARY["ms"]?></td>
		</tr>
		<tr id="tr_metadata_t1_5">
			<td><?=$_DICTIONARY["output_blocking_duration"]?>: </td>
			<td><input id="in_t1_targetBlocking" type="number" value="0" /> <?=$_DICTIONARY["ms"]?></td>
		</tr>
		<tr id="tr_metadata_t1_6">
			<td><?=$_DICTIONARY["input_blocking_delay"]?>: </td>
			<td><input id="in_t1_triggerBlockingDelay" type="number" value="0" /> <?=$_DICTIONARY["ms"]?></td>
		</tr>
		<tr id="tr_metadata_t1_7">
			<td><?=$_DICTIONARY["output_blocking_delay"]?>: </td>
			<td><input id="in_t1_targetBlockingDelay" type="number" value="0" /> <?=$_DICTIONARY["ms"]?></td>
		</tr>
		<tr id="tr_metadata_t1_8">
			<td><?=$_DICTIONARY["block_motion_sensor"]?>: </td>
			<td><input id="in_t1_blockMotionSensor" type="checkbox" /><label for="in_t1_blockMotionSensor">&nbsp;</label> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["block_motion_sensor"]?></b></p>"+
				"<p><?=$_DICTIONARY["block_motion_sensor_desc"]?></p>"
			);</script></td>
		</tr>
		<!-- METADATA TYPE 2 -->
		<tr id="tr_metadata_t2_0">
			<td><?=$_DICTIONARY["edge"]?>: </td>
			<td><select id="in_t2_trigger_edge">
				<option value="1" selected><?=$_DICTIONARY["rising"]?></option>
				<option value="0"><?=$_DICTIONARY["falling"]?></option>
			</select></td>
		</tr>
		<tr id="tr_metadata_t2_1">
			<td><?=$_DICTIONARY["state_on"]?>: </td>
			<td><input id="in_t2_targetStateOn" type="number" value="1" /> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["state_on"]?></b></p>"+
				"<p><?=$_DICTIONARY["state_on_desc"]?></p>"
			);</script></td>
		</tr>
		<tr id="tr_metadata_t2_2">
			<td><?=$_DICTIONARY["state_off"]?>: </td>
			<td><input id="in_t2_targetStateOff" type="number" value="0" /> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["state_off"]?></b></p>"+
				"<p><?=$_DICTIONARY["state_off_desc"]?></p>"
			);</script></td>
		</tr>
		<tr id="tr_metadata_t2_3">
			<td><?=$_DICTIONARY["delay"]?>: </td>
			<td><input id="in_t2_delayTurnOn" type="number" value="0" /> <?=$_DICTIONARY["ms"]?></td>
		</tr>
		<tr id="tr_metadata_t2_4">
			<td><?=$_DICTIONARY["duration"]?>: </td>
			<td><input id="in_t2_duration" type="number" value="30000" /> <?=$_DICTIONARY["ms"]?></td>
		</tr>
		<tr id="tr_metadata_t2_5">
			<td><?=$_DICTIONARY["motion_sensor"]?>: </td>
			<td><input id="in_t2_isMotionSensor" type="checkbox" /><label for="in_t2_isMotionSensor">&nbsp;</label> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["motion_sensor"]?></b></p>"+
				"<p><?=$_DICTIONARY["motion_sensor_desc"]?></p>"
			);</script></td>
		</tr>
		<tr id="tr_metadata_t2_6">
			<td><?=$_DICTIONARY["block_motion_sensor"]?>: </td>
			<td><input id="in_t2_blockMotionSensor" type="checkbox" /><label for="in_t2_blockMotionSensor">&nbsp;</label> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["block_motion_sensor"]?></b></p>"+
				"<p><?=$_DICTIONARY["block_motion_sensor_desc"]?></p>"
			);</script></td>
		</tr>
		<tr id="tr_metadata_t2_7">
			<td><?=$_DICTIONARY["input_blocking_duration"]?>: </td>
			<td><input id="in_t2_triggerBlocking" type="number" value="0" /> <?=$_DICTIONARY["ms"]?></td>
		</tr>
		<tr id="tr_metadata_t2_8">
			<td><?=$_DICTIONARY["output_blocking_duration"]?>: </td>
			<td><input id="in_t2_targetBlocking" type="number" value="0" /> <?=$_DICTIONARY["ms"]?></td>
		</tr>
		<tr id="tr_metadata_t2_9">
			<td><?=$_DICTIONARY["input_blocking_delay"]?>: </td>
			<td><input id="in_t2_triggerBlockingDelay" type="number" value="0" /> <?=$_DICTIONARY["ms"]?></td>
		</tr>
		<tr id="tr_metadata_t2_10">
			<td><?=$_DICTIONARY["output_blocking_delay"]?>: </td>
			<td><input id="in_t2_targetBlockingDelay" type="number" value="0" /> <?=$_DICTIONARY["ms"]?></td>
		</tr>
		<tr id="tr_metadata_t2_11">
			<td><?=$_DICTIONARY["button"]?>: </td>
			<td><input id="in_t2_isButton" type="checkbox" /><label for="in_t2_isButton">&nbsp;</label> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["button"]?></b></p>"+
				"<p><?=$_DICTIONARY["button_desc0"]?></p>"+
				"<p><?=$_DICTIONARY["button_desc1"]?></p>"
			);</script></td>
		</tr>
		<tr id="tr_metadata_t2_12">
			<td><?=$_DICTIONARY["ignore_other_timers"]?>: </td>
			<td><input id="in_t2_ignoreOtherTimers" type="checkbox" /><label for="in_t2_ignoreOtherTimers">&nbsp;</label> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["ignore_other_timers"]?></b></p>"+
				"<p><?=$_DICTIONARY["ignore_other_timers_desc"]?></p>"
			);</script></td>
		</tr>
		<tr id="tr_metadata_t2_13">
			<td><?=$_DICTIONARY["time_limit"]?>: </td>
			<td>
				<p><input id="in_t2_timeLimit" type="checkbox" /><label for="in_t2_timeLimit">&nbsp;</label> <script>SHS.docs(16,
					"<p><b><?=$_DICTIONARY["time_limit"]?></b></p>"+
					"<p><?=$_DICTIONARY["time_limit_desc"]?></p>"
				);</script></p>
				<table style="width: 300px; margin-top: 10px;">
					<tr>
						<td><?=$_DICTIONARY["start"]?> <script>SHS.docs(16,
								"<p><b><?=$_DICTIONARY["start"]?></b></p>"+
								"<p><?=$_DICTIONARY["start_desc"]?></p>"+
								"<p style=\"margin-top: 10px;\"><b><?=$_DICTIONARY["notice"]?></b></p>"+
								"<p><?=$_DICTIONARY["notice_desc"]?></p>"
							);</script></td>
						<td>
							<input style="width: 40px;" placeholder="HH" id="in_t2_start_hour" type="number" min="0" max="23" step="1"  onchange="if(parseInt(this.value,10)<10) this.value='0'+this.value; if(parseInt(this.value,10)<0) this.value='0'; if(parseInt(this.value,10)>23) this.value='23';" /><!--
							--><b>:</b><!--
							--><input style="width: 40px;" placeholder="MM" id="in_t2_start_minute" type="number" min="0" max="59" step="1"  onchange="if(parseInt(this.value,10)<10) this.value='0'+this.value; if(parseInt(this.value,10)<0) this.value='00'; if(parseInt(this.value,10)>59) this.value='59';" />
						</td>
					</tr>
					<tr>
						<td><?=$_DICTIONARY["end"]?> <script>SHS.docs(16,
								"<p><b><?=$_DICTIONARY["end"]?></b></p>"+
								"<p><?=$_DICTIONARY["end_desc"]?></p>"+
								"<p style=\"margin-top: 10px;\"><b><?=$_DICTIONARY["notice"]?></b></p>"+
								"<p><?=$_DICTIONARY["notice_desc"]?></p>"
							);</script></td>
						<td><!--
							--><input style="width: 40px;" placeholder="HH" id="in_t2_end_hour" type="number" min="0" max="23" step="1"  onchange="if(parseInt(this.value,10)<10) this.value='0'+this.value; if(parseInt(this.value,10)<0) this.value='0'; if(parseInt(this.value,10)>23) this.value='23';" /><!--
							--><b>:</b><!--
							--><input style="width: 40px;" placeholder="MM" id="in_t2_end_minute" type="number" min="0" max="59" step="1"  onchange="if(parseInt(this.value,10)<10) this.value='0'+this.value; if(parseInt(this.value,10)<0) this.value='00'; if(parseInt(this.value,10)>59) this.value='59';" />
						</td>
					</tr>
					<tr style="height: 10px"><td></td><td></td></tr>
					<tr>
						<td><input id="in_t2_control_by_srss" type="checkbox" /><label for="in_t2_control_by_srss">&nbsp;</label></td>
						<td><?=$_DICTIONARY["control_by_sunrise_sunset"]?></td>
					</tr>
				</table>
			</td>
		</tr>
		<!-- METADATA TYPE 3 -->
		<tr id="tr_metadata_t3_0">
			<td><?=$_DICTIONARY["edge"]?>: </td>
			<td><select id="in_t3_trigger_edge">
				<option value="1" selected><?=$_DICTIONARY["rising"]?></option>
				<option value="0"><?=$_DICTIONARY["falling"]?></option>
			</select></td>
		</tr>
		<tr id="tr_metadata_t3_1">
			<td><?=$_DICTIONARY["state"]?>: </td>
			<td><input id="in_t3_targetValue" type="number" value="0" /> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["state"]?></b></p>"+
				"<p><?=$_DICTIONARY["state_desc"]?></p>"
			);</script></td>
		</tr>
		<tr id="tr_metadata_t3_2">
			<td><?=$_DICTIONARY["input_blocking_duration"]?>: </td>
			<td><input id="in_t3_triggerBlocking" type="number" value="0" /> <?=$_DICTIONARY["ms"]?></td>
		</tr>
		<tr id="tr_metadata_t3_3">
			<td><?=$_DICTIONARY["output_blocking_duration"]?>: </td>
			<td><input id="in_t3_targetBlocking" type="number" value="0" /> <?=$_DICTIONARY["ms"]?></td>
		</tr>
		<tr id="tr_metadata_t3_4">
			<td><?=$_DICTIONARY["input_blocking_delay"]?>: </td>
			<td><input id="in_t3_triggerBlockingDelay" type="number" value="0" /> <?=$_DICTIONARY["ms"]?></td>
		</tr>
		<tr id="tr_metadata_t3_5">
			<td><?=$_DICTIONARY["output_blocking_delay"]?>: </td>
			<td><input id="in_t3_targetBlockingDelay" type="number" value="0" /> <?=$_DICTIONARY["ms"]?></td>
		</tr>
		<tr id="tr_metadata_t3_6">
			<td><?=$_DICTIONARY["delay"]?>: </td>
			<td><input id="in_t3_delay" type="number" value="0" /> <?=$_DICTIONARY["ms"]?></td>
		</tr>
		<!-- METADATA TYPE 4 -->
		<tr id="tr_metadata_t4_0">
			<td><?=$_DICTIONARY["visibility_treshold"]?>: </td>
			<td><input id="in_t4_treshold" type="number" value="10" min="0" max="100" onchange="if(this.value < 0) this.value=0; if(this.value > 100) this.value=100;" /> % <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["visibility_treshold"]?></b></p>"+
				"<p><?=$_DICTIONARY["visibility_treshold_desc"]?></p>"
			);</script></td>
		</tr>
		<tr id="tr_metadata_t4_1">
			<td><?=$_DICTIONARY["step_period"]?>: </td>
			<td><input id="in_t4_changePause" type="number" value="50" /> <?=$_DICTIONARY["ms"]?> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["step_period"]?></b></p>"+
				"<p><?=$_DICTIONARY["step_period_desc"]?></p>"
			);</script></td>
		</tr>
		<tr id="tr_metadata_t4_2">
			<td><?=$_DICTIONARY["step_size"]?>: </td>
			<td><input id="in_t4_changeAmount" type="number" value="1" onchange="if(this.value < 0) this.value=0; if(this.value > 100) this.value=100;" /> % <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["step_size"]?></b></p>"+
				"<p><?=$_DICTIONARY["step_size_desc"]?></p>"
			);</script></td>
		</tr>
		<tr id="tr_metadata_t4_3">
			<td><?=$_DICTIONARY["delay"]?>: </td>
			<td><input id="in_t4_delay" type="number" value="1000" /> <?=$_DICTIONARY["ms"]?></td>
		</tr>
		<tr id="tr_metadata_t4_4">
			<td><?=$_DICTIONARY["marginal_values_hold"]?>: </td>
			<td><input id="in_t4_keepTime" type="number" value="1000" /> <?=$_DICTIONARY["ms"]?> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["marginal_values_hold"]?></b></p>"+
				"<p><?=$_DICTIONARY["marginal_values_hold_desc"]?></p>"
			);</script></td>
		</tr>
		<tr id="tr_metadata_t4_5">
			<td><?=$_DICTIONARY["max_val"]?>: </td>
			<td><input id="in_t4_maxValue" type="number" value="100" min="0" max="100" onchange="if(this.value < 0) this.value=0; if(this.value > 100) this.value=100;" /> % <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["max_val"]?></b></p>"+
				"<p><?=$_DICTIONARY["max_val_desc"]?></p>"
			);</script></td>
		</tr>
		<tr id="tr_metadata_t4_6">
			<td><?=$_DICTIONARY["min_val"]?>: </td>
			<td><input id="in_t4_minValue" type="number" value="0" min="0" max="100" onchange="if(this.value < 0) this.value=0; if(this.value > 100) this.value=100;" /> % <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["min_val"]?></b></p>"+
				"<p><?=$_DICTIONARY["min_val_desc"]?></p>"
			);</script></td>
		</tr>
		<tr id="tr_metadata_t4_7">
			<td><?=$_DICTIONARY["reversed_spectrum"]?>: </td>
			<td><input id="in_t4_revesed" type="checkbox" /><label for="in_t4_revesed">&nbsp;</label> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["reversed_spectrum"]?></b></p>"+
				"<p><?=$_DICTIONARY["reversed_spectrum_desc"]?></p>"
			);</script></td>
		</tr>
		<!-- METADATA TYPE 5 -->
		<tr id="tr_metadata_t5_0">
			<td><?=$_DICTIONARY["edge"]?>: </td>
			<td><select id="in_t5_trigger_edge">
				<option value="1" selected><?=$_DICTIONARY["rising"]?></option>
				<option value="0"><?=$_DICTIONARY["falling"]?></option>
			</select></td>
		</tr>
		<tr id="tr_metadata_t5_1">
			<td><?=$_DICTIONARY["state_on"]?>: </td>
			<td><input id="in_t5_targetValueOn" type="number" value="1" /> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["state_on"]?></b></p>"+
				"<p><?=$_DICTIONARY["state_on_desc"]?></p>"
			);</script></td>
		</tr>
		<tr id="tr_metadata_t5_2">
			<td><?=$_DICTIONARY["state_off"]?>: </td>
			<td><input id="in_t5_targetValueOff" type="number" value="0" /> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["state_off"]?></b></p>"+
				"<p><?=$_DICTIONARY["state_off_desc"]?></p>"
			);</script></td>
		</tr>
		<tr id="tr_metadata_t5_3">
			<td><?=$_DICTIONARY["delay"]?>: </td>
			<td><input id="in_t5_delay" type="number" value="5000" /> <?=$_DICTIONARY["ms"]?></td>
		</tr>
		<tr id="tr_metadata_t5_4">
			<td><?=$_DICTIONARY["duration"]?>: </td>
			<td><input id="in_t5_duration" type="number" value="120000" /> <?=$_DICTIONARY["ms"]?> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["duration"]?></b></p>"+
				"<p><?=$_DICTIONARY["duration_desc"]?></p>"
			);</script></td>
		</tr>
		<!-- METADATA TYPE 6 -->
		<tr id="tr_metadata_t6_0">
			<td><?=$_DICTIONARY["hysteresis_upper_limit"]?>: </td>
			<td><input id="in_t6_hystUp" type="number" value="2" step=".1" /> °C</td>
		</tr>
		<tr id="tr_metadata_t6_1">
			<td><?=$_DICTIONARY["hysteresis_lower_limit"]?>: </td>
			<td><input id="in_t6_hystDown" type="number" value="2" step=".1" /> °C</td>
		</tr>
		<!-- METADATA TYPE 7 -->
		<tr id="tr_metadata_t7_0">
			<td><?=$_DICTIONARY["supply_treshold"]?>: </td>
			<td><input id="in_t7_treshold" type="number" value="10" min="0" max="100" onchange="if(this.value < 0) this.value=0; if(this.value > 100) this.value=100;" /> % <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["supply_treshold"]?></b></p>"+
				"<p><?=$_DICTIONARY["supply_treshold_desc"]?></p>"
			);</script></td>
		</tr>
		<tr id="tr_metadata_t7_1">
			<td><?=$_DICTIONARY["supply_value_off"]?>: </td>
			<td><input id="in_t7_offValue" type="number" value="0" min="0" max="100" onchange="if(this.value < 0) this.value=0; if(this.value > 100) this.value=100;" /> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["supply_value_off"]?></b></p>"+
				"<p><?=$_DICTIONARY["supply_value_off_desc"]?></p>"
			);</script></td>
		</tr>
		<tr id="tr_metadata_t7_2">
			<td><?=$_DICTIONARY["supply_value_on"]?>: </td>
			<td><input id="in_t7_onValue" type="number" value="1" min="0" max="100" onchange="if(this.value < 0) this.value=0; if(this.value > 100) this.value=100;" /> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["supply_value_on"]?></b></p>"+
				"<p><?=$_DICTIONARY["supply_value_on_desc"]?></p>"
			);</script></td>
		</tr>
		<tr id="tr_metadata_t7_3">
			<td><?=$_DICTIONARY["reversed_spectrum"]?>: </td>
			<td><input id="in_t7_reversed" type="checkbox" /><label for="in_t7_reversed">&nbsp;</label> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["reversed_spectrum"]?></b></p>"+
				"<p><?=$_DICTIONARY["reversed_spectrum_desc"]?></p>"
			);</script></td>
		</tr>
		<!-- ----------------------------- -->
		<tr>
			<td><?=$_DICTIONARY["activation_conditions"]?>: </td>
			<td><textarea id="in_activation_conditions"><?=$activation_conditions?></textarea></td>
		</tr>
		<tr>
			<td><?=$_DICTIONARY["deactivation_conditions"]?>: </td>
			<td><textarea id="in_deactivation_conditions"><?=$deactivation_conditions?></textarea></td>
		</tr>
		<tr>
			<td></td>
			<td><button id="in_save"><?=$_DICTIONARY["save"]?></button><button id="in_cancel"><?=$_DICTIONARY["cancel"]?></button></td>
		</tr>
	</table>
</div>
<script>
$(document).ready(function() {
	var initialMetadata = "<?=$metadata?>";
	var imetadataArray = initialMetadata.split(";");
	var type = "<?=$type?>";

	for(var i = 0; i <= 7; i++)
		$("tr[id*='tr_metadata_t"+i+"']").addClass("hidden");

	$("tr[id*='tr_metadata_t"+type+"']").removeClass("hidden");

	var t2_timeStart = null;
	var t2_timeEnd = null;
	switch(type) {
		case "0":
			for(var i in imetadataArray) {
				var pair = imetadataArray[i].split("=");
				if(pair[0] == "triggerEdge")
					$("#in_t0_trigger_edge").val(pair[1]);
				else if(pair[0] == "delayTurnOn")
					$("#in_t0_delayTurnOn").val(pair[1]);
				else if(pair[0] == "delayTurnOff")
					$("#in_t0_delayTurnOff").val(pair[1]);
				else if(pair[0] == "triggerBlocking")
					$("#in_t0_triggerBlocking").val(pair[1]);
				else if(pair[0] == "targetBlocking")
					$("#in_t0_targetBlocking").val(pair[1]);
				else if(pair[0] == "triggerBlockingDelay")
					$("#in_t0_triggerBlockingDelay").val(pair[1]);
				else if(pair[0] == "targetBlockingDelay")
					$("#in_t0_targetBlockingDelay").val(pair[1]);
				else if(pair[0] == "blockMotionSensor")
					$("#in_t0_blockMotionSensor").prop('checked', pair[1] == "true");
			}
			break;
		case "1":
			for(var i in imetadataArray) {
				var pair = imetadataArray[i].split("=");
				if(pair[0] == "reversed")
					$("#in_t1_revesed").prop('checked', pair[1] == "true");
				else if(pair[0] == "delayTurnOn")
					$("#in_t1_delayTurnOn").val(pair[1]);
				else if(pair[0] == "delayTurnOff")
					$("#in_t1_delayTurnOff").val(pair[1]);
				else if(pair[0] == "delayChange")
					$("#in_t1_delayChange").val(pair[1]);
				else if(pair[0] == "triggerBlocking")
					$("#in_t1_triggerBlocking").val(pair[1]);
				else if(pair[0] == "targetBlocking")
					$("#in_t1_targetBlocking").val(pair[1]);
				else if(pair[0] == "triggerBlockingDelay")
					$("#in_t1_triggerBlockingDelay").val(pair[1]);
				else if(pair[0] == "targetBlockingDelay")
					$("#in_t1_targetBlockingDelay").val(pair[1]);
				else if(pair[0] == "blockMotionSensor")
					$("#in_t1_blockMotionSensor").prop('checked', pair[1] == "true");
			}
			break;
		case "2":
			for(var i in imetadataArray) {
				var pair = imetadataArray[i].split("=");
				if(pair[0] == "triggerEdge")
					$("#in_t2_trigger_edge").val(pair[1]);
				else if(pair[0] == "targetStateOn")
					$("#in_t2_targetStateOn").val(pair[1]);
				else if(pair[0] == "targetStateOff")
					$("#in_t2_targetStateOff").val(pair[1]);
				else if(pair[0] == "delayTurnOn")
					$("#in_t2_delayTurnOn").val(pair[1]);
				else if(pair[0] == "duration")
					$("#in_t2_duration").val(pair[1]);
				else if(pair[0] == "isMotionSensor")
					$("#in_t2_isMotionSensor").prop('checked', pair[1] == "true");
				else if(pair[0] == "blockMotionSensor")
					$("#in_t2_blockMotionSensor").prop('checked', pair[1] == "true");
				else if(pair[0] == "triggerBlocking")
					$("#in_t2_triggerBlocking").val(pair[1]);
				else if(pair[0] == "targetBlocking")
					$("#in_t2_targetBlocking").val(pair[1]);
				else if(pair[0] == "triggerBlockingDelay")
					$("#in_t2_triggerBlockingDelay").val(pair[1]);
				else if(pair[0] == "targetBlockingDelay")
					$("#in_t2_targetBlockingDelay").val(pair[1]);
				else if(pair[0] == "isButton")
					$("#in_t2_isButton").prop('checked', pair[1] == "true");
				else if(pair[0] == "ignoreOtherTimers")
					$("#in_t2_ignoreOtherTimers").prop('checked', pair[1] == "true");
				else if(pair[0] == "ignoreOtherTimers")
					$("#in_t2_ignoreOtherTimers").prop('checked', pair[1] == "true");
				else if(pair[0] == "activeTimeStart")
					t2_timeStart = pair[1];
				else if(pair[0] == "activeTimeEnd")
					t2_timeEnd = pair[1];
			}
			if(t2_timeStart != null && t2_timeEnd != null) {
				$("#in_t2_timeLimit").prop('checked', true);
				if(t2_timeStart == "ss" && t2_timeEnd == "sr")
					$("#in_t2_control_by_srss").prop('checked', true);
				else {
					$("#in_t2_start_hour").val(t2_timeStart.substring(0, 2));
					$("#in_t2_start_minute").val(t2_timeStart.substring(3, 5));
					$("#in_t2_end_hour").val(t2_timeEnd.substring(0, 2));
					$("#in_t2_end_minute").val(t2_timeEnd.substring(3, 5));
				}
			}
			break;
		case "3":
			for(var i in imetadataArray) {
				var pair = imetadataArray[i].split("=");
				if(pair[0] == "triggerEdge")
					$("#in_t3_trigger_edge").val(pair[1]);
				else if(pair[0] == "targetValue")
					$("#in_t3_targetValue").val(pair[1]);
				else if(pair[0] == "triggerBlocking")
					$("#in_t3_triggerBlocking").val(pair[1]);
				else if(pair[0] == "targetBlocking")
					$("#in_t3_targetBlocking").val(pair[1]);
				else if(pair[0] == "triggerBlockingDelay")
					$("#in_t3_triggerBlockingDelay").val(pair[1]);
				else if(pair[0] == "targetBlockingDelay")
					$("#in_t3_targetBlockingDelay").val(pair[1]);
				else if(pair[0] == "delay")
					$("#in_t3_delay").val(pair[1]);
			}
			break;
		case "4":
			for(var i in imetadataArray) {
				var pair = imetadataArray[i].split("=");
				if(pair[0] == "treshold")
					$("#in_t4_treshold").val(pair[1]);
				else if(pair[0] == "changePause")
					$("#in_t4_changePause").val(pair[1]);
				else if(pair[0] == "changeAmount")
					$("#in_t4_changeAmount").val(pair[1]);
				else if(pair[0] == "delay")
					$("#in_t4_delay").val(pair[1]);
				else if(pair[0] == "keepTime")
					$("#in_t4_keepTime").val(pair[1]);
				else if(pair[0] == "maxValue")
					$("#in_t4_maxValue").val(pair[1]);
				else if(pair[0] == "minValue")
					$("#in_t4_minValue").val(pair[1]);
				else if(pair[0] == "reversed")
					$("#in_t4_revesed").prop('checked', pair[1] == "true");
				else if(pair[0] == "powerSupply")
					$("#in_t4_powerSupply").val(pair[1]);
				else if(pair[0] == "powerSupplyTreshold")
					$("#in_t4_powerSupplyTreshold").val(pair[1]);
				else if(pair[0] == "powerSupplyOnValue")
					$("#in_t4_powerSupplyOnValue").val(pair[1]);
				else if(pair[0] == "powerSupplyOffValue")
					$("#in_t4_powerSupplyOffValue").val(pair[1]);
			}
			break;
		case "5":
			for(var i in imetadataArray) {
				var pair = imetadataArray[i].split("=");
				if(pair[0] == "triggerEdge")
					$("#in_t5_trigger_edge").val(pair[1]);
				else if(pair[0] == "targetValueOn")
					$("#in_t5_targetValueOn").val(pair[1]);
				else if(pair[0] == "targetValueOff")
					$("#in_t5_targetValueOff").val(pair[1]);
				else if(pair[0] == "delay")
					$("#in_t5_delay").val(pair[1]);
				else if(pair[0] == "duration")
					$("#in_t5_duration").val(pair[1]);
			}
			break;
		case "6":
			for(var i in imetadataArray) {
				var pair = imetadataArray[i].split("=");
				if(pair[0] == "hystUp")
					$("#in_t6_hystUp").val(pair[1].replace(/./g, ','));
				else if(pair[0] == "hystDown")
					$("#in_t6_hystDown").val(pair[1].replace(/./g, ','));
			}
			break;
		case "7":
			for(var i in imetadataArray) {
				var pair = imetadataArray[i].split("=");
				if(pair[0] == "reversed")
					$("#in_t7_reversed").prop('checked', pair[1] == "true");
				else if(pair[0] == "treshold")
					$("#in_t7_treshold").val(pair[1]);
				else if(pair[0] == "onValue")
					$("#in_t7_onValue").val(pair[1]);
				else if(pair[0] == "offValue")
					$("#in_t7_offValue").val(pair[1]);
			}
			break;
	}

	$("select#in_type").change(function() {
		$("tr[id*='tr_metadata_t']").removeClass("hidden");
		for(var i = 0; i <= 7; i++)
			if(Number($(this).val()) != i)
				$("tr[id*='tr_metadata_t"+i+"']").addClass("hidden");
	});

	$("button#in_cancel").click(function() {
		location.href = "?page=trigger_events";
	});

	$("button#in_save").click(function() {
		var trigger = $("select#in_trigger").val();
		var target = $("select#in_target").val();
		var group_id = $("input#in_group_id").val();
		var type =  $("select#in_type").val();
		var metadata = "";
		switch(type) {
			case "0":
				var triggerEdge = $("#in_t0_trigger_edge").val();
				var delayTurnOn = $("#in_t0_delayTurnOn").val();
				var delayTurnOff = $("#in_t0_delayTurnOff").val();
				var triggerBlocking = $("#in_t0_triggerBlocking").val();
				var targetBlocking = $("#in_t0_targetBlocking").val();
				var triggerBlockingDelay = $("#in_t0_triggerBlockingDelay").val();
				var targetBlockingDelay = $("#in_t0_targetBlockingDelay").val();
				var blockMotionSensor = $("#in_t0_blockMotionSensor").is(":checked") ? "true" : "false";
				if(triggerEdge != "1") metadata += ";triggerEdge=" + triggerEdge;
				if(delayTurnOn != "0") metadata += ";delayTurnOn=" + delayTurnOn;
				if(delayTurnOff != "0") metadata += ";delayTurnOff=" + delayTurnOff;
				if(triggerBlocking != "0") metadata += ";triggerBlocking=" + triggerBlocking;
				if(targetBlocking != "0") metadata += ";targetBlocking=" + targetBlocking;
				if(triggerBlockingDelay != "0") metadata += ";triggerBlockingDelay=" + triggerBlockingDelay;
				if(targetBlockingDelay != "0") metadata += ";targetBlockingDelay=" + targetBlockingDelay;
				if(blockMotionSensor != "false") metadata += ";blockMotionSensor=" + blockMotionSensor;
				break;
			case "1":
				var reversed = $("#in_t1_revesed").is(":checked") ? "true" : "false";
				var delayTurnOn = $("#in_t1_delayTurnOn").val();
				var delayTurnOff = $("#in_t1_delayTurnOff").val();
				var delayChange = $("#in_t1_delayChange").val();
				var triggerBlocking = $("#in_t1_triggerBlocking").val();
				var targetBlocking = $("#in_t1_targetBlocking").val();
				var triggerBlockingDelay = $("#in_t1_triggerBlockingDelay").val();
				var targetBlockingDelay = $("#in_t1_targetBlockingDelay").val();
				var blockMotionSensor = $("#in_t1_blockMotionSensor").is(":checked") ? "true" : "false";
				if(reversed != "false") metadata += ";reversed=" + reversed;
				if(delayTurnOn != "0") metadata += ";delayTurnOn=" + delayTurnOn;
				if(delayTurnOff != "0") metadata += ";delayTurnOff=" + delayTurnOff;
				if(delayChange != "0") metadata += ";delayChange=" + delayChange;
				if(triggerBlocking != "0") metadata += ";triggerBlocking=" + triggerBlocking;
				if(targetBlocking != "0") metadata += ";targetBlocking=" + targetBlocking;
				if(triggerBlockingDelay != "0") metadata += ";triggerBlockingDelay=" + triggerBlockingDelay;
				if(targetBlockingDelay != "0") metadata += ";targetBlockingDelay=" + targetBlockingDelay;
				if(blockMotionSensor != "false") metadata += ";blockMotionSensor=" + blockMotionSensor;
				break;
			case "2":
				var triggerEdge = $("#in_t2_trigger_edge").val();
				var targetStateOn = $("#in_t2_targetStateOn").val();
				var targetStateOff = $("#in_t2_targetStateOff").val();
				var delayTurnOn = $("#in_t2_delayTurnOn").val();
				var duration = $("#in_t2_duration").val();
				var isMotionSensor = $("#in_t2_isMotionSensor").is(":checked") ? "true" : "false";
				var blockMotionSensor = $("#in_t2_blockMotionSensor").is(":checked") ? "true" : "false";
				var triggerBlocking = $("#in_t2_triggerBlocking").val();
				var targetBlocking = $("#in_t2_targetBlocking").val();
				var triggerBlockingDelay = $("#in_t2_triggerBlockingDelay").val();
				var targetBlockingDelay = $("#in_t2_targetBlockingDelay").val();
				var isButton = $("#in_t2_isButton").is(":checked") ? "true" : "false";
				var ignoreOtherTimers = $("#in_t2_ignoreOtherTimers").is(":checked") ? "true" : "false";
				var startTime = null;
				var endTime = null;
				if($("#in_t2_timeLimit").is(":checked")) {
					if($("#in_t2_control_by_srss").is(":checked")) {
						startTime = "ss";
						endTime = "sr";
					} else {
						startTime = $("#in_t2_start_hour").val().slice(-2) + ":" + $("#in_t2_start_minute").val().slice(-2);
						endTime = $("#in_t2_end_hour").val().slice(-2) + ":" + $("#in_t2_end_minute").val().slice(-2);
					}
				}
				if(triggerEdge != "1") metadata += ";triggerEdge=" + triggerEdge;
				if(targetStateOn != "1") metadata += ";targetStateOn=" + targetStateOn;
				if(targetStateOff != "0") metadata += ";targetStateOff=" + targetStateOff;
				if(delayTurnOn != "0") metadata += ";delayTurnOn=" + delayTurnOn;
				if(duration != "30000") metadata += ";duration=" + duration;
				if(isMotionSensor != "false") metadata += ";isMotionSensor=" + isMotionSensor;
				if(blockMotionSensor != "false") metadata += ";blockMotionSensor=" + blockMotionSensor;
				if(triggerBlocking != "0") metadata += ";triggerBlocking=" + triggerBlocking;
				if(targetBlocking != "0") metadata += ";targetBlocking=" + targetBlocking;
				if(triggerBlockingDelay != "0") metadata += ";triggerBlockingDelay=" + triggerBlockingDelay;
				if(targetBlockingDelay != "0") metadata += ";targetBlockingDelay=" + targetBlockingDelay;
				if(isButton != "false") metadata += ";isButton=" + isButton;
				if(ignoreOtherTimers != "false") metadata += ";ignoreOtherTimers=" + ignoreOtherTimers;
				if(startTime != null) metadata += ";activeTimeStart=" + startTime;
				if(endTime != null) metadata += ";activeTimeEnd=" + endTime;
				break;
			case "3":
				var triggerEdge = $("#in_t3_trigger_edge").val();
				var targetValue = $("#in_t3_targetValue").val();
				var triggerBlocking = $("#in_t3_triggerBlocking").val();
				var targetBlocking = $("#in_t3_targetBlocking").val();
				var triggerBlockingDelay = $("#in_t3_triggerBlockingDelay").val();
				var targetBlockingDelay = $("#in_t3_targetBlockingDelay").val();
				var delay = $("#in_t3_delay").val();
				if(triggerEdge != "1") metadata += ";triggerEdge=" + triggerEdge;
				if(targetValue != "0") metadata += ";targetValue=" + targetValue;
				if(triggerBlocking != "0") metadata += ";triggerBlocking=" + triggerBlocking;
				if(targetBlocking != "0") metadata += ";targetBlocking=" + targetBlocking;
				if(triggerBlockingDelay != "0") metadata += ";triggerBlockingDelay=" + triggerBlockingDelay;
				if(targetBlockingDelay != "0") metadata += ";targetBlockingDelay=" + targetBlockingDelay;
				if(delay != "0") metadata += ";delay=" + delay;
				break;
			case "4":
				var treshold = $("#in_t4_treshold").val();
				var changePause = $("#in_t4_changePause").val();
				var changeAmount = $("#in_t4_changeAmount").val();
				var delay = $("#in_t4_delay").val();
				var keepTime = $("#in_t4_keepTime").val();
				var maxValue = $("#in_t4_maxValue").val();
				var minValue = $("#in_t4_minValue").val();
				var reversed = $("#in_t4_revesed").is(":checked") ? "true" : "false";
				var powerSupply = $("#in_t4_powerSupply").val();
				var powerSupplyTreshold = $("#in_t4_powerSupplyTreshold").val();
				var powerSupplyOnValue = $("#in_t4_powerSupplyOnValue").val();
				var powerSupplyOffValue = $("#in_t4_powerSupplyOffValue").val();
				if(treshold != "10") metadata += ";treshold=" + treshold;
				if(changePause != "50") metadata += ";changePause=" + changePause;
				if(changeAmount != "1") metadata += ";changeAmount=" + changeAmount;
				if(delay != "1000") metadata += ";delay=" + delay;
				if(keepTime != "1000") metadata += ";keepTime=" + keepTime;
				if(maxValue != "100") metadata += ";maxValue=" + maxValue;
				if(minValue != "0") metadata += ";minValue=" + minValue;
				if(reversed != "false") metadata += ";reversed=" + reversed;
				if(powerSupply != "0") metadata += ";powerSupply=" + powerSupply;
				if(powerSupplyTreshold != "10") metadata += ";powerSupplyTreshold=" + powerSupplyTreshold;
				if(powerSupplyOnValue != "1") metadata += ";powerSupplyOnValue=" + powerSupplyOnValue;
				if(powerSupplyOffValue != "0") metadata += ";powerSupplyOffValue=" + powerSupplyOffValue;
				break;
			case "5":
				var triggerEdge = $("#in_t5_trigger_edge").val();
				var targetValueOn = $("#in_t5_targetValueOn").val();
				var targetValueOff = $("#in_t5_targetValueOff").val();
				var delay = $("#in_t5_delay").val();
				var duration = $("#in_t5_duration").val();
				if(triggerEdge != "1") metadata += ";triggerEdge=" + triggerEdge;
				if(targetValueOn != "1") metadata += ";targetValueOn=" + targetValueOn;
				if(targetValueOff != "0") metadata += ";targetValueOff=" + targetValueOff;
				if(delay != "5000") metadata += ";delay=" + delay;
				if(duration != "120000") metadata += ";duration=" + duration;
				break;
			case "6":
				var hystUp = $("#in_t6_hystUp").val();
				var hystDown = $("#in_t6_hystDown").val();
				if(hystUp != "2") metadata += ";hystUp=" + hystUp.replace(/,/g, '.');
				if(hystDown != "2") metadata += ";hystDown=" + hystDown.replace(/,/g, '.');
				break;
			case "7":
				var reversed = $("#in_t7_reversed").is(":checked") ? "true" : "false";
				var treshold = $("#in_t7_treshold").val();
				var onValue = $("#in_t7_onValue").val();
				var offValue = $("#in_t7_offValue").val();
				if(reversed != "false") metadata += ";reversed=" + reversed;
				if(treshold != "10") metadata += ";treshold=" + treshold;
				if(onValue != "1") metadata += ";onValue=" + onValue;
				if(offValue != "0") metadata += ";offValue=" + offValue;
				break;
        }

		if(metadata.length > 0)
			metadata = metadata.substring(1);

		var activation_conditions =  $("textarea#in_activation_conditions").val();
        var deactivation_conditions =  $("textarea#in_deactivation_conditions").val();

		var urlGetData = "id=<?=$teID?>&trigger="+trigger+"&target="+target+"&group_id="+group_id
						+"&type="+type+"&metadata="+metadata+"&activation_conditions="+activation_conditions
						+"&deactivation_conditions="+deactivation_conditions;
		$.get("phpscript/updateTriggerEvent.php?"+urlGetData, function(data) {
			if(data.trim() == "OK")
				location.href = "?page=trigger_events";
			else
				alert("<?=$_DICTIONARY["save_ctge_fail"]?>");
		});
	});
});
</script>
