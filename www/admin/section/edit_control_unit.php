<?php
	$security_sectors = "";

$cuID = $httpRequest->getQuery('id');
$row = $dbServer->fetch("
	SELECT
		software_address,
		hardware_address,
		value_type,
		security,
		description,
		active_state,
		simulate,
		last_state,
		activation_delay,
		ewc_version,
		metadata
	FROM control_units
	WHERE
		id = ?
	",
	$cuID
);

if(!$row){
	return;
}
?>
<style>
span input[type=radio] {
	position: absolute;
	visibility: hidden;
}

span label {
	display: inline-block;
	font-size: 12px;
	padding: 10px 15px;
	cursor: pointer;
	-webkit-transition: all 0.25s linear;
	background-color: #555;
	color: #dbd9d9;
	font-style: italic;
}

span:first-child label {
	-webkit-border-top-left-radius: 10px;
	-webkit-border-bottom-left-radius: 10px;
	-moz-border-radius-topleft: 10px;
	-moz-border-radius-bottomleft: 10px;
	border-top-left-radius: 10px;
	border-bottom-left-radius: 10px;
}

span:last-child label {
	-webkit-border-top-right-radius: 10px;
	-webkit-border-bottom-right-radius: 10px;
	-moz-border-radius-topright: 10px;
	-moz-border-radius-bottomright: 10px;
	border-top-right-radius: 10px;
	border-bottom-right-radius: 10px;
}

button {
	background-color: #555;
	color: #dbd9d9;
	width: 130px;
	height: 40px;
	line-height: 30px;
	text-align: center;
	border: none;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
	font-size: 15px;
	margin-top: 15px;
	cursor: pointer;
	margin-right: 5px;
	-webkit-transition: all 0.25s linear;
	-webkit-box-shadow: inset 0px -3px 33px 0px rgba(0,0,0,0.75);
	-moz-box-shadow: inset 0px -3px 33px 0px rgba(0,0,0,0.75);
	box-shadow: inset 0px -3px 33px 0px rgba(0,0,0,0.75);
}

button:hover {
	color: #ffffff;
	background-color: #333;
}

span label:hover {
	color: #ffffff;
}

input[type=radio]:checked ~ label{
	background-color: #333;
	color: #ffffff;
}

input[type="number"], input[type="text"], textarea {
	color: #ffffff;
	background-color: #383b3f;
	width: 100px;
	border: none;
	padding: 10px 15px;
	font-size: 12px;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
}

textarea {
	width: 413px;
	height: 50px;
}

input[type="text"] {
	width: 150px;
}

input[type="checkbox"] {
	display:none;
}

input[type=checkbox]#in_cu_simulate + label {
	background: url('res/img/occupied.png');
	background-size: 20px 20px;
	background-repeat: no-repeat;
	display: inline-block;
	width: 20px;
	height: 20px;
	cursor: pointer;
	-webkit-transition: all 0.25s linear;
}

input[type=checkbox]#in_cu_simulate:checked + label {
	background: url('res/img/ok.png');
	background-size: 20px 20px;
	background-repeat: no-repeat;
}

p.security_check_unit input[type=checkbox] + label {
	background: url('res/img/security.png');
	background-size: 20px 20px;
	background-repeat: no-repeat;
	display:inline-block;
	padding: 0 0 0 25px;
	width: 100%;
	height: 100%;
	cursor: pointer;
	font-size: 12px;
	color: #dbd9d9;
	font-style: italic;
	-webkit-transition: all 0.25s linear;
}

p.security_check_unit input[type=checkbox] + label:hover {
	color: #ffffff;
}

p.security_check_unit input[type=checkbox]:checked + label {
	background: url('res/img/security_filled.png');
	background-size: 20px 20px;
	background-repeat: no-repeat;
	color: #ffffff;
}

p.security_check_unit {
	display: inline-block;
	width: 200px;
	padding: 10px;
	background-color: #383b3f;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
	line-height: 20px;
}

div#security_sectors_array {
	line-height: 45px;
}

select, select option {
	font-size: 12px;
	border: none;
	color: #ffffff;
	background-color: #383b3f;
	padding: 10px 15px;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
}

table#options_table {
	border: none;
	width: 100%;
	border-collapse: separate;
	border-spacing: 5px;
	-webkit-touch-callout: none;
	-webkit-user-select: none;
	-khtml-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

table#options_table tr td {
	vertical-align: top;
}

table#options_table tr td:nth-child(1) {
	width: 30%;
}

table#options_table tr td:nth-child(2) {
	width: 70%;
}

img#swid_state {
	width: 20px;
	height: 20px;
	transform: translate(0, 5px);
}
</style>

<?php
	$ewc_version = $row[9];
	$isVirtual = !(substr($row[1], 0, 3) === "EWC");
	$ewcID = $isVirtual ? "" : substr($row[1], 3, 2);
	$ioType = $isVirtual ? "IN" : (substr($row[1], 6, 3) === "OUT" ? "OUT" : "IN");
	$ioID = $isVirtual ? substr($row[1], 7) : ($ioType == "IN" ? substr($row[1],8) : substr($row[1],9));
	$value_type = $row[2];
	$security_sectors = $row[3];
	$description = $row[4];
	$active_state = $row[5];
	$simulate = $row[6];
	$activation_delay = $row[8];
	$metadata = $row[10];
?>
<div>
	<table id="options_table">
		<tr>
			<td><?=$_DICTIONARY["id"]?>: </td>
			<td><b><?=$row[0]?></b> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["id"]?></b></p>"+
				"<p><?=$_DICTIONARY["id_desc2"]?></p>"
			);</script></td>
		</tr>
		<tr>
			<td><?=$_DICTIONARY["state"]?>: </td>
			<td><b><?=$row[7]?></b> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["state"]?></b></p>"+
				"<p><?=$_DICTIONARY["state_desc"]?></p>"
			);</script></td>
		</tr>
		<tr>
			<td><?=$_DICTIONARY["unit_type"]?>: </td>
			<td>
				<span><input type="radio" id="l_cu_hw_type1" name="cu_hw_type" value="physical" <?php echo(!$isVirtual ? "checked " : ""); ?>/><!--
				--><label for="l_cu_hw_type1"><?=$_DICTIONARY["physical"]?></label></span><!--
				--><span><input type="radio" id="l_cu_hw_type2" name="cu_hw_type" value="virtual" <?php echo($isVirtual ? "checked " : ""); ?>/><!--
				--><label for="l_cu_hw_type2"><?=$_DICTIONARY["virtual"]?></label></span> <script>SHS.docs(16,
					"<p><b><?=$_DICTIONARY["unit_type"]?></b></p>"+
					"<p><?=$_DICTIONARY["unit_type_desc"]?></p>"
				);</script>
			</td>
		</tr>
		<tr>
			<td><?=$_DICTIONARY["hw_addr"]?>: </td>
			<td>
				<?php if(!$isVirtual) :?>
				<div id="cu_hw_physical_address">
					<p><input placeholder="EWC ID" value="<?=$ewcID?>" id="in_cu_ewc_id" type="number" min="1" maxlength="2" max="99" step="1"  onchange="if(parseInt(this.value,10)<10)this.value='0'+this.value; if(parseInt(this.value,10)<0) this.value='0'; if(parseInt(this.value,10)>99) this.value='99';" /></p>
					<p style="margin-top: 5px;">
						<span><input id="l_ewc_version1" type="radio" name="ewc_version" value="2"<?php if($ewc_version!="1") echo(" checked"); ?> /><label for="l_ewc_version1"><?=$_DICTIONARY["new_version"]?></label></span><!--
						--><span><input id="l_ewc_version2" type="radio" name="ewc_version" value="1"<?php if($ewc_version=="1") echo(" checked"); ?>  /><label for="l_ewc_version2"><?=$_DICTIONARY["old_version"]?></label></span>
					</p>
					<p style="margin-top: 5px; display: inline-block;">
						<span><input id="l_cu_io_type1" type="radio" name="cu_io_type" value="input"<?php if($ioType=="IN") echo(" checked"); ?> /><label for="l_cu_io_type1"><?=$_DICTIONARY["input"]?></label></span><!--
						--><span><input id="l_cu_io_type2"type="radio" name="cu_io_type" value="output"<?php if($ioType=="OUT") echo(" checked"); ?> /><label for="l_cu_io_type2"><?=$_DICTIONARY["output"]?></label></span>
					</p>
					<p style="display: inline-block"><select id="cu_io_id">
						<?php
							$pinvalues = array();
							if($ioType == "OUT") {
								if($ewc_version == "1")
									$pinvalues = array(0,5,1,6,2,7,3,8,4,9);
								else
									$pinvalues = array(0,1,2,3,4,5,6,7,8,9);
							} else {
								if($ewc_version == "1")
									$pinvalues = array(24,25,16,17,18,19,20,21,23,22);
								else
									$pinvalues = array(16,17,18,19,20,21,22,23,25,24);
							}
						?>
						<?php foreach($pinvalues as $key => $value) :?>
							<option value="<?=$value?>"<?php if($ioID==$value) echo(" selected"); ?>><?=$value?> (pin <?=($key+1)?><?php if($ewc_version != "1" && $ioType == "OUT" && $value < 5) echo(" / PWM".($value+1)); ?>)</option>
						<?php endforeach; ?>
					</select> <script>SHS.docs(16,
						"<p><b><?=$_DICTIONARY["hw_addr"]?></b></p>"+
						"<p><?=$_DICTIONARY["hw_addr_desc_physical"]?></p>"
					);</script></p>
				</div>
				<div id="cu_hw_virtual_address" class="hidden">
					<p><input id="in_cu_vio_id" type="number" placeholder="<?=$_DICTIONARY["io_id"]?>" min="0" step="1" /> <script>SHS.docs(16,
						"<p><b><?=$_DICTIONARY["hw_addr"]?></b></p>"+
						"<p><?=$_DICTIONARY["hw_addr_desc_virtual"]?></p>"
					);</script></p>
				</div>
				<?php else :?>
				<div id="cu_hw_physical_address" class="hidden">
					<p><input placeholder="<?=$_DICTIONARY["unit_id"]?>" id="in_cu_ewc_id" type="number" min="1" maxlength="2" max="99" step="1"  onchange="if(parseInt(this.value,10)<10)this.value='0'+this.value; if(parseInt(this.value,10)<0) this.value='0'; if(parseInt(this.value,10)>99) this.value='99';" /></p>
					<p style="margin-top: 5px;">
						<span><input id="l_ewc_version1" type="radio" name="ewc_version" value="2" checked /><label for="l_ewc_version1"><?=$_DICTIONARY["new_version"]?></label></span><!--
						--><span><input id="l_ewc_version2" type="radio" name="ewc_version" value="1" /><label for="l_ewc_version2"><?=$_DICTIONARY["old_version"]?></label></span>
					</p>
					<p style="margin-top: 5px; display: inline-block;">
						<span><input id="l_cu_io_type1" type="radio" name="cu_io_type" value="input" checked /><label for="l_cu_io_type1"><?=$_DICTIONARY["input"]?></label></span><!--
						--><span><input id="l_cu_io_type2"type="radio" name="cu_io_type" value="output" /><label for="l_cu_io_type2"><?=$_DICTIONARY["output"]?></label></span>
					</p>
					<p style="display: inline-block"><select id="cu_io_id">
						<option value="16">16 (<?=$_DICTIONARY["pin"]?> 1)</option>
						<option value="17">17 (<?=$_DICTIONARY["pin"]?> 2)</option>
						<option value="18">18 (<?=$_DICTIONARY["pin"]?> 3)</option>
						<option value="19">19 (<?=$_DICTIONARY["pin"]?> 4)</option>
						<option value="20">20 (<?=$_DICTIONARY["pin"]?> 5)</option>
						<option value="21">21 (<?=$_DICTIONARY["pin"]?> 6)</option>
						<option value="22">22 (<?=$_DICTIONARY["pin"]?> 7)</option>
						<option value="23">23 (<?=$_DICTIONARY["pin"]?> 8)</option>
						<option value="25">25 (<?=$_DICTIONARY["pin"]?> 9)</option>
						<option value="24">24 (<?=$_DICTIONARY["pin"]?> 10)</option>
					</select> <script>SHS.docs(16,
						"<p><b><?=$_DICTIONARY["hw_addr"]?></b></p>"+
						"<p><?=$_DICTIONARY["hw_addr_desc_physical"]?></p>"
					);</script></p>
				</div>
				<div id="cu_hw_virtual_address">
					<p><input id="in_cu_vio_id" type="number" placeholder="<?=$_DICTIONARY["io_id"]?>" min="0" step="1" value="<?=$ioID?>" /> <script>SHS.docs(16,
						"<p><b><?=$_DICTIONARY["hw_addr"]?></b></p>"+
						"<p><?=$_DICTIONARY["hw_addr_desc_virtual"]?></p>"
					);</script></p>
				</div>
				<?php endif; ?>
			</td>
		</tr>
		<tr>
			<td><?=$_DICTIONARY["value_type"]?>: </td>
			<td><select id="cu_value_type">
				<option value="0"<?php if($value_type=="0") echo(" selected"); ?>><?=$_DICTIONARY["digital"]?></option>
				<option value="1"<?php if($value_type=="1") echo(" selected"); ?>><?=$_DICTIONARY["temperature"]?></option>
				<option value="2"<?php if($value_type=="2") echo(" selected"); ?>><?=$_DICTIONARY["pwm"]?></option>
				<option value="3"<?php if($value_type=="3") echo(" selected"); ?>><?=$_DICTIONARY["rfid"]?></option>
			</select> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["value_type"]?></b></p>"+
				"<p><?=$_DICTIONARY["value_type_desc2"]?></p>"
			);</script></td>
		</tr>
		<tr id="security_sectors_check_tr"<?php if($isVirtual || $ioType == "OUT") echo(" class=\"hidden\""); ?>>
			<td><?=$_DICTIONARY["security_sectors"]?>: </td>
			<td><div id="security_sectors_array"></div> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["security_sectors"]?></b></p>"+
				"<p><?=$_DICTIONARY["security_sectors_desc"]?></p>"
			);</script></td>
		</tr>
		<tr>
			<td><?=$_DICTIONARY["description"]?>: </td>
			<td><textarea id="cu_description"><?=$description?></textarea> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["description"]?></b></p>"+
				"<p><?=$_DICTIONARY["description_desc"]?></p>"
			);</script></td>
		</tr>
		<tr>
			<td><?=$_DICTIONARY["active_state_value"]?>: </td>
			<td><input id="in_cu_active_state" type="number" value="<?=$active_state?>" /> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["active_state_value"]?></b></p>"+
				"<p><?=$_DICTIONARY["active_state_value_desc"]?></p>"
			);</script></td>
		</tr>
		<tr>
			<td><?=$_DICTIONARY["enable_holidays_mode"]?>: </td>
			<td><input type="checkbox" id="in_cu_simulate"<?php if($simulate == "1") echo(" checked"); ?> /><label for="in_cu_simulate">&nbsp;</label></td>
		</tr>
		<tr>
			<td><?=$_DICTIONARY["activation_delay"]?>: </td>
			<td><input id="in_cu_activation_delay" type="number" min="0" step="50" value="<?=$activation_delay?>" /> ms</td>
		</tr>
		<tr id="tr_metadata_t2_pwm_min">
			<td><?=$_DICTIONARY["pwm_min"]?>: </td>
			<td><input id="in_t2_pwm_min" type="number" min="0" max="100" step="1" value="0" /> % <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["pwm_min"]?></b></p>"+
				"<p><?=$_DICTIONARY["pwm_min_desc"]?></p>"
			);</script></td>
		</tr>
		<tr id="tr_metadata_t2_pwm_max">
			<td><?=$_DICTIONARY["pwm_max"]?>: </td>
			<td><input id="in_t2_pwm_max" type="number" min="0" max="100" step="1" value="100" /> % <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["pwm_max"]?></b></p>"+
				"<p><?=$_DICTIONARY["pwm_max_desc"]?></p>"
			);</script></td>
		</tr>
		<tr>
			<td></td>
			<td><button id="in_save"><?=$_DICTIONARY["save"]?></button><button id="in_cancel"><?=$_DICTIONARY["cancel"]?></button></td>
		</tr>
	</table>
</div>
<script>
$(document).ready(function() {
	var initialMetadata = "<?=$metadata?>";
	var imetadataArray = initialMetadata.split(";");
	var type = "<?=$value_type?>";

	for(var i = 0; i <= 3; i++)
		$("tr[id*='tr_metadata_t"+i+"']").addClass("hidden");

	$("tr[id*='tr_metadata_t"+type+"']").removeClass("hidden");

	switch(type) {
		case "2":
			for(var i in imetadataArray) {
				var pair = imetadataArray[i].split("=");
				if(pair[0] == "pwm_min")
					$("#in_t2_pwm_min").val(pair[1]);
				else if(pair[0] == "pwm_max")
					$("#in_t2_pwm_max").val(pair[1]);
			}
			break;
	}

	$("select#cu_value_type").change(function() {
		$("tr[id*='tr_metadata_t']").removeClass("hidden");
		for(var i = 0; i <= 3; i++)
			if(Number($(this).val()) != i)
				$("tr[id*='tr_metadata_t"+i+"']").addClass("hidden");
	});

	$.get("phpscript/genSecuritySectorsCheckArray.php", function(data) {
		$("div#security_sectors_array").html(data);
		var sectors = "<?php echo($security_sectors); ?>";
		var sectorsArray = sectors.split(",");
		for(var i in sectorsArray)
			$("div#security_sectors_array p input[type='checkbox']#check_security_sector_"+sectorsArray[i]).prop('checked', true);
	});

	var updateSelects = function() {
		var values = [];
        var printPWM = false;

		if($("input[type=radio][name=\"cu_io_type\"]:checked").val() == "input") {
			if($("tr#security_sectors_check_tr").hasClass("hidden"))
				$("tr#security_sectors_check_tr").removeClass("hidden");

			if($("input[type=radio][name=\"ewc_version\"]:checked").val() == "2")
				values = [16,17,18,19,20,21,22,23,25,24];
			else if($("input[type=radio][name=\"ewc_version\"]:checked").val() == "1")
				values = [24,25,16,17,18,19,20,21,23,22];
		} else if($("input[type=radio][name=\"cu_io_type\"]:checked").val() == "output") {
			if(!$("tr#security_sectors_check_tr").hasClass("hidden"))
				$("tr#security_sectors_check_tr").addClass("hidden");

			if($("input[type=radio][name=\"ewc_version\"]:checked").val() == "2") {
				values = [0,1,2,3,4,5,6,7,8,9];
                printPWM = true;
            } else if($("input[type=radio][name=\"ewc_version\"]:checked").val() == "1")
				values = [0,5,1,6,2,7,3,8,4,9];
		}

		var optionStr = "";
		for(var i = 0; i < 10; i++)
			optionStr += "<option value=\""+values[i]+"\">"+values[i]+" (pin "+(i+1)+ (printPWM && i < 5 ? " / PWM"+(i+1) : "") +")</option>\n";
		$("select#cu_io_id").html(optionStr);
	};

	$("input[type='radio'][name='cu_io_type']").change(updateSelects);
	$("input[type='radio'][name='ewc_version']").change(updateSelects);

	$("input[type='radio'][name='cu_hw_type']").change(function() {
		if($("input[type=radio][name=\"cu_hw_type\"]:checked").val() == "physical") {
			if($("div#cu_hw_physical_address").hasClass("hidden"))
				$("div#cu_hw_physical_address").removeClass("hidden");
			if(!$("div#cu_hw_virtual_address").hasClass("hidden"))
				$("div#cu_hw_virtual_address").addClass("hidden");
			if($("input[type=radio][name=\"cu_io_type\"]:checked").val() == "input")
				if($("tr#security_sectors_check_tr").hasClass("hidden"))
					$("tr#security_sectors_check_tr").removeClass("hidden");
			if($("input[type=radio][name=\"cu_io_type\"]:checked").val() == "output")
				if(!$("tr#security_sectors_check_tr").hasClass("hidden"))
					$("tr#security_sectors_check_tr").addClass("hidden");
		} else if($("input[type=radio][name=\"cu_hw_type\"]:checked").val() == "virtual") {
			if($("div#cu_hw_virtual_address").hasClass("hidden"))
				$("div#cu_hw_virtual_address").removeClass("hidden");
			if(!$("div#cu_hw_physical_address").hasClass("hidden"))
				$("div#cu_hw_physical_address").addClass("hidden");
			if($("tr#security_sectors_check_tr").hasClass("hidden"))
				$("tr#security_sectors_check_tr").removeClass("hidden");
		}
	});

	$("button#in_cancel").click(function() {
		location.href = "?page=control_units";
	});
	$("button#in_save").click(function() {
		var id = "<?=$cuID?>";
		var hardware_address = "";
		if($("input[type=radio][name=\"cu_hw_type\"]:checked").val() == "physical") {
			if(isNaN($("input#in_cu_ewc_id").val()) || $("input#in_cu_ewc_id").val() == "") {
				alert("<?=$_DICTIONARY["invalid_cuid"]?>");
				return;
			}
			var iotype = $("input[type=radio][name=\"cu_io_type\"]:checked").val() == "input" ? "IN" : "OUT";
			hardware_address = "EWC" + $("input#in_cu_ewc_id").val() + "_" + iotype + $("select#cu_io_id").val();
		} else {
			if(isNaN($("input#in_cu_vio_id").val()) || $("input#in_cu_vio_id").val() == "") {
				alert("<?=$_DICTIONARY["invalid_virtid"]?>");
				return;
			}
			hardware_address = "VIRTUAL" + $("input#in_cu_vio_id").val();
		}
		var value_type = Number($("select#cu_value_type").val());
		var metadata = "";
		switch(value_type) {
			case 2:
				var pwmMin = $("#in_t2_pwm_min").val();
				var pwmMax = $("#in_t2_pwm_max").val();
				if(pwmMin != "0") metadata += ";pwm_min=" + pwmMin;
				if(pwmMax != "100") metadata += ";pwm_max=" + pwmMax;
				alert(pwmMin + " " + pwmMax);
				break;
		}

		if(metadata.length > 0)
			metadata = metadata.substring(1);

		var security = "";
		if($("input[type=radio][name=\"cu_io_type\"]:checked").val() == "input" || $("input[type=radio][name=\"cu_hw_type\"]:checked").val() == "virtual") {
			var securityElements = $("div#security_sectors_array p input[type='checkbox']").toArray();
			for (var i in securityElements) {
				if($(securityElements[i]).is(':checked'))
					security += ","+$(securityElements[i]).data("sectorid");
			}
		}
		if(security.length > 0)
			security = security.substring(1);
		var description = $("textarea#cu_description").val();
		var active_state = Number($("input#in_cu_active_state").val());
		var simulate = $("input#in_cu_simulate").is(':checked') ? "1" : "0";
		var activation_delay = Number($("input#in_cu_activation_delay").val());
		var ewc_version = $("input[type=radio][name=\"cu_hw_type\"]:checked").val() == "virtual" ? 0 : Number($("input[type=radio][name=\"ewc_version\"]:checked").val());

		var urlGetData = "id="+id+"&hardware_address="+hardware_address+"&value_type="+value_type
						+"&security="+security+"&description="+description+"&active_state="+active_state+"&simulate="+simulate
						+"&activation_delay="+activation_delay+"&ewc_version="+ewc_version+"&metadata="+metadata;
		$.get("phpscript/updateCU.php?"+urlGetData, function(data) {
			if(data.trim() == "OK")
				location.href = "?page=control_units";
			else
				alert("<?=$_DICTIONARY["save_ccu_fail"]?>");
		});
	});
});
</script>
