<style>
span#server_status, span#server_version, span#server_date, span#server_time {
	font-weight: bold;
}

button {
	background-color: #555;
	color: #dbd9d9;
	padding: 0 15px;
	height: 40px;
	line-height: 30px;
	text-align: center;
	border: none;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
	font-size: 15px;
	margin-top: 15px;
	cursor: pointer;
	margin-right: 5px;
	-webkit-transition: all 0.25s linear;
	-webkit-box-shadow: inset 0px -3px 33px 0px rgba(0,0,0,0.75);
	-moz-box-shadow: inset 0px -3px 33px 0px rgba(0,0,0,0.75);
	box-shadow: inset 0px -3px 33px 0px rgba(0,0,0,0.75);
}

button:hover {
	color: #ffffff;
	background-color: #333;
}

button:disabled {
	background-color: #999;
	cursor: default;
}

table#server-info-tbl tr {
	line-height: 20px;
}

table#server-info-tbl tr td:first-child {
	width: 120px;
}
</style>
<table id="server-info-tbl">
	<tr>
		<td><?=$_DICTIONARY["server_status"]?>:</td>
		<td><span id="server_status">offline</span></td>
	</tr>
	<tr>
		<td><?=$_DICTIONARY["server_version"]?>:</td>
		<td><span id="server_version">-</span></td>
	</tr>
	<tr>
		<td><?=$_DICTIONARY["server_date"]?>:</td>
		<td><span id="server_date">-</span></td>
	</tr>
	<tr>
		<td><?=$_DICTIONARY["server_time"]?>:</td>
		<td><span id="server_time">-</span></td>
	</tr>
</table>
<p><button id="btn_restart"><?=$_DICTIONARY["restart_server"]?></button><button id="btn_reboot"><?=$_DICTIONARY["reboot"]?></button> <script>SHS.docs(16,
	"<p><b><?=$_DICTIONARY["restart_server"]?></b></p>"+
	"<p><?=$_DICTIONARY["restart_server_desc"]?></p>"+
	"<p style=\"margin-top: 10px;\"><b><?=$_DICTIONARY["reboot"]?></b></p>"+
	"<p><?=$_DICTIONARY["reboot_desc"]?></p>"
);</script></p>

<script>
var lastAnswer = new Date().getTime();
function updateServerVersion(res) {
	lastAnswer = new Date().getTime();
	$("button#btn_restart").prop("disabled", false);
	$("button#btn_reboot").prop("disabled", false);
	$("span#server_version").html(res);
	$("span#server_status").html("online");
}
function updateServerDate(res) {
	$("span#server_date").html(res.replace(/\//g, "."));
}
function updateServerTime(res) {
	$("span#server_time").html(res);
}
function voidCatch(res) {}
$(document).ready(function() {
	SHS.query("updateServerVersion", "getversionandname");
	SHS.query("updateServerDate", "getdate");
	SHS.query("updateServerTime", "gettime");
	setInterval(function() {
		var time = new Date().getTime();
		SHS.query("updateServerVersion", "getversionandname");
		SHS.query("updateServerDate", "getdate");
		if(time - lastAnswer > 10000) {
			$("span#server_status").html("offline");
			$("span#server_version").html("-");
			$("span#server_date").html("-");
			$("span#server_time").html("-");
			$("button#btn_restart").prop("disabled", true);
			$("button#btn_reboot").prop("disabled", true);
		}
	}, 5000);
	setInterval(function() {
		answerGot = false;
		SHS.query("updateServerTime", "gettime");
	}, 500);
	
	$("button#btn_restart").click(function() {
		SHS.query("voidCatch", "stoperr");
		$("span#server_status").html("offline");
		$("span#server_version").html("-");
		$("span#server_date").html("-");
		$("span#server_time").html("-");
		$(this).prop("disabled", true);
		$("button#btn_reboot").prop("disabled", true);
	});
	
	$("button#btn_reboot").click(function() {
		SHS.query("voidCatch", "reboot");
		$(this).prop("disabled", true);
		$("button#btn_restart").prop("disabled", true);
	});
});
</script>
