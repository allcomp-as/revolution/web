<style>
button {
	background-color: #555;
	color: #dbd9d9;
	width: 130px;
	height: 40px;
	line-height: 30px;
	text-align: center;
	border: none;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
	font-size: 15px;
	margin-top: 15px;
	cursor: pointer;
	margin-right: 5px;
	-webkit-transition: all 0.25s linear;
	-webkit-box-shadow: inset 0px -3px 33px 0px rgba(0,0,0,0.75);
	-moz-box-shadow: inset 0px -3px 33px 0px rgba(0,0,0,0.75);
	box-shadow: inset 0px -3px 33px 0px rgba(0,0,0,0.75);
}

button:hover {
	color: #ffffff;
	background-color: #333;
}

input[type="text"], input[type="password"] {
	color: #ffffff;
	background-color: #383b3f;
	width: 235px;
	border: none;
	padding: 10px 15px;
	font-size: 12px;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
}

table#options_table {
	border: none;
	width: 100%;
	border-collapse: separate;
	border-spacing: 5px;
	-webkit-touch-callout: none;
	-webkit-user-select: none;
	-khtml-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

table#options_table tr td {
	vertical-align: top;
}

table#options_table tr td:nth-child(1) {
	width: 30%;
}

table#options_table tr td:nth-child(2) {
	width: 70%;
}
</style>
<div>
	<table id="options_table">
		<tr>
			<td><?=$_DICTIONARY["username"]?>: </td>
			<td><input id="in_username" type="text" value="" /></td>
		</tr>
		<tr>
			<td><?=$_DICTIONARY["password"]?>: </td>
			<td><input id="in_password" type="password" value="" /></td>
		</tr>
		<tr>
			<td></td>
			<td><button id="in_save"><?=$_DICTIONARY["save"]?></button><button id="in_cancel"><?=$_DICTIONARY["cancel"]?></button></td>
		</tr>
	</table>
</div>
<script>
$(document).ready(function() {
	$("button#in_cancel").click(function() {
		location.href = "?page=rooms";
	});
	$("button#in_save").click(function() {
		var username = $("#in_username").val();
		var password = $("#in_password").val();
		var urlGetData = "username="+username+"&password="+password;
		$.get("phpscript/insertNewUser.php?"+urlGetData, function(data) {
			if(data.trim() == "OK")
				location.href = "?page=users";
			else if(data.trim() == "ERR")
				alert("<?=$_DICTIONARY["username_not_available"]?>");
			else
				alert("<?=$_DICTIONARY["save_nuser_fail"]?>");
		});
	});
});
</script>
