<style>
button#tbl_first, button#tbl_prev, button#tbl_next, button#tbl_last {
	width: 50px;
	height: 31px;
	color: #ffffff;
	background-color: #36383a;
	border: none;
	cursor: pointer;
	margin: 0;
}

button#tbl_first:disabled, button#tbl_prev:disabled, button#tbl_next:disabled, button#tbl_last:disabled {
	background-color: #6e7175;
}

button#tbl_first:hover:enabled, button#tbl_prev:hover:enabled, button#tbl_next:hover:enabled, button#tbl_last:hover:enabled {
	font-weight: bold;
}

div#tbl_pages {
	display: inline-block;
	height: 31px;
	background-color: #dbdbdb;
	line-height: 30px;
	text-align: center;
	width: -moz-calc(100% - 200px);
	width: -webkit-calc(100% - 200px);
	width: -o-calc(100% - 200px);
	width: calc(100% - 200px);
}

button.pageBtn, button.currentPageBtn {
	width: 26px;
	height: 26px;
	color: #ffffff;
	background-color: #36383a;
	border: none;
	cursor: pointer;
	margin-left: 3px;
	font-weight: bold;
	transform: translate(0,-1px);
}

button.pageBtn:first-child {
	margin-left: 0
}

table#macros_table {
	border: none;
	width: 100%;
	border-collapse: collapse;
}

table#macros_table tr th {
	-webkit-touch-callout: none;
	-webkit-user-select: none;
	-khtml-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

table#macros_table tr td:nth-child(2)  {
	-webkit-touch-callout: none;
	-webkit-user-select: none;
	-khtml-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

table#macros_table th, table#macros_table td {
	text-align: left;
	vertical-align: center;
	padding: 5px 10px;
}

table#macros_table tr td:first-child {
	font-weight: bold;
}

table#macros_table th, table#macros_table tr:last-child td {
	border-bottom: 2px outset #f9fbff;
}

table#macros_table tr:nth-child(2n) {
	background-color: rgba(96, 96, 96, 0.7);
	color: #ffffff;
}

table#macros_table tr:nth-child(2n-1) {
	background-color: rgba(76, 76, 76, 0.7);
	color: #ffffff;
}

table#macros_table tr:first-child {
	background-color: #000000;
	color: #f7f7f7;
	font-weight: bold;
}

div#tableControls {
	padding: 0;
	width: 100%;
	text-align: center;
	border-top: 3px solid #333232;
	-webkit-touch-callout: none;
	-webkit-user-select: none;
	-khtml-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

div#tableHeader {
	width: 100%;
	height: 30px;
	background-color: #7a7a7a;
	margin-bottom: 2px;
	line-height: 30px;
	padding: 0 5px;
	color: #ffffff;
	font-size: 10px;
	-webkit-touch-callout: none;
	-webkit-user-select: none;
	-khtml-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
	box-sizing: border-box;
}

div#tableContent {
	display: block;
}

div#tableContainer {
	display: block;
	width: 100%;
	margin: 0 auto;
}

input[type="number"], input[type="text"] {
	color: #ffffff;
	background-color: #383b3f;
	width: 30px;
	border: none;
	padding: 3px;
	font-size: 10px;
}

input[type="text"] {
	width: 150px;
}

select, select option {
	font-size: 10px;
	border: none;
	color: #ffffff;
	background-color: #383b3f;
	padding: 3px;
}

select:focus > option:checked {
	background: #000 !important;
}

select option:checked,
select option:hover {
	box-shadow: 0 0 10px 100px #000 inset;
}

select:active, select:hover {
	outline: none
}

select:active, select:hover {
	outline-color: #666666;
}

img.macro_add, img.macro_edit, img.macro_delete {
	cursor: pointer;
}
</style>
<div id="tableContainer">
<div id="tableHeader">
	<div style="display: inline-block; float: left">
		<?=$_DICTIONARY["show"]?> <input id="tbl_num_rows" type="number" value="10" min="1" /> <?=$_DICTIONARY["lines"]?><!-- 
		--> | <?=$_DICTIONARY["sort_by"]?> <select id="tbl_order_by"><!--
			--><option value="name"><?=$_DICTIONARY["name"]?></option><!--
		--></select><select id="tbl_sort_select"><!--
			--><option value="ASC">↑</option><!--
			--><option value="DESC">↓</option><!--
		--></select>
	</div>
	<div  style="display: inline-block; float: right;">
		<input type="text" id="tbl_search" placeholder="<?=$_DICTIONARY["search"]?>" />
	</div>
	<div style="display: none; clear: both;"></div>
</div>
<div id="tableContent"></div>
<div id="tableControls"><!--
	--><button id="tbl_first">&lt;&lt;</button><!--
	--><button id="tbl_prev">&lt;</button><!--
	--><div id="tbl_pages"></div><!--
	--><button id="tbl_next">&gt;</button><!--
	--><button id="tbl_last">&gt;&gt;</button><!--
--></div>
</div>
<script>
var tbl_limit_start = 0;
var tbl_limit_count = 10;
var tbl_num_rows = 0;
var pageRadius = 8;
var tbl_sort = "ASC";
var tbl_order = "name";
var tbl_search = "";

function genTable() {
	var tbl_search_text = tbl_search != "" ? ("&search=" + tbl_search) : "";
	$.get("phpscript/genMacrosTable.php?limit_start="+tbl_limit_start+"&limit_count="+tbl_limit_count+"&sort="+tbl_sort+"&order="+tbl_order + tbl_search_text, function(data) {
		$("div#tableContent").html(data);
		
		$("img.macro_add").off();
		$("img.macro_delete").off();
		$("img.macro_edit").off();
	
		$("img.macro_add").click(function() {
			location.href = "?page=new_macro";
		});
		
		$("img.macro_delete").click(function() {
			var r = confirm("<?=$_DICTIONARY["macro_del_ack"]?>");
			if (r == true) {
				$.get("phpscript/deleteMacro.php?id="+$(this).data("dbid"), function(data) {
					genTable();
					genTableControls();
				});
			}
		});
		
		$("img.macro_edit").click(function() {
			location.href = "?page=edit_macro&id="+$(this).data("dbid");
		});
	});
}

function genTableControls() {
	var tbl_search_text = tbl_search != "" ? ("&search=" + tbl_search) : "";
	$.get("phpscript/genMacrosTable.php?gen_count=true" + tbl_search_text, function(data) {
		tbl_num_rows = Number(data);
		var numPages = Math.ceil(tbl_num_rows / tbl_limit_count);
		var controlsString = "";
		var currentPage = Math.floor(tbl_limit_start / tbl_limit_count);
		if(numPages <= 17) {
			for(var pgid = 0; pgid < numPages; pgid++) {
				if(currentPage == pgid)
					controlsString += "<button class='currentPageBtn' disabled style='background-color: #000; color: #FFF;' data-pgid='"+pgid+"'>"+(pgid+1)+"</button>";
				else
					controlsString += "<button class='pageBtn' data-pgid='"+pgid+"'>"+(pgid+1)+"</button>";
			}
		} else {
			for(var i = -pageRadius; i <= pageRadius; i++) {
				var pgid = i < 0 ? (currentPage - i*i) : (currentPage + i*i);
				if(pgid < 0)
					continue;
				if(pgid >= numPages)
					continue;
				if(currentPage == pgid)
					controlsString += "<button class='currentPageBtn' disabled style='background-color: #000; color: #FFF;' data-pgid='"+pgid+"'>"+(pgid+1)+"</button>";
				else
					controlsString += "<button class='pageBtn' data-pgid='"+pgid+"'>"+(pgid+1)+"</button>";
			}
		}
		$("div#tableControls div#tbl_pages").html(controlsString.trim());
		
		if(currentPage <= 0) {
			$("button#tbl_first").attr("disabled", true);
			$("button#tbl_prev").attr("disabled", true);
		} else {
			$("button#tbl_first").attr("disabled", false);
			$("button#tbl_prev").attr("disabled", false);
		}
		
		if(currentPage >= numPages-1) {
			$("button#tbl_last").attr("disabled", true);
			$("button#tbl_next").attr("disabled", true);
		} else {
			$("button#tbl_last").attr("disabled", false);
			$("button#tbl_next").attr("disabled", false);
		}
		
		$("button.pageBtn").off();
		$("button#tbl_first").off();
		$("button#tbl_prev").off();
		$("button#tbl_next").off();
		$("button#tbl_last").off();
		
		$("button.pageBtn").click(function() {
			tbl_limit_start = Number($(this).data("pgid"))*tbl_limit_count;
			genTable();
			genTableControls();
		});

		$("button#tbl_first").click(function() {
			tbl_limit_start = 0;
			genTable();
			genTableControls();
		});

		$("button#tbl_prev").click(function() {
			tbl_limit_start -= tbl_limit_count;
			if(tbl_limit_start < 0)
				tbl_limit_start = 0;
			genTable();
			genTableControls();
		});
		
		$("button#tbl_next").click(function() {
			tbl_limit_start += tbl_limit_count;
			genTable();
			genTableControls();
		});
		
		$("button#tbl_last").click(function() {
			tbl_limit_start = (numPages-1) * tbl_limit_count;
			genTable();
			genTableControls();
		});
	});
}

$(document).ready(function() {
	genTable();
	genTableControls();
	
	$("input#tbl_num_rows").change(function() {
		var value = Number($("input#tbl_num_rows").val());
		if(value < 1)
			return;
		tbl_limit_start = 0;
		tbl_limit_count = value;
		genTable();
		genTableControls();
	});
	
	$("select#tbl_order_by").change(function() {
		tbl_order = $("select#tbl_order_by").val();
		tbl_limit_start = 0;
		genTable();
		genTableControls();
	});
	
	$("select#tbl_sort_select").change(function() {
		tbl_sort = $("select#tbl_sort_select").val();
		tbl_limit_start = 0;
		genTable();
		genTableControls();
	});
	
	$("input#tbl_search").change(function() {
		tbl_search = $("input#tbl_search").val();
		tbl_limit_start = 0;
		genTable();
		genTableControls();
	});
});
</script>
