<style>
button {
	background-color: #555;
	color: #dbd9d9;
	width: 130px;
	height: 40px;
	line-height: 30px;
	text-align: center;
	border: none;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
	font-size: 15px;
	margin-top: 15px;
	cursor: pointer;
	margin-right: 5px;
	-webkit-transition: all 0.25s linear;
	-webkit-box-shadow: inset 0px -3px 33px 0px rgba(0,0,0,0.75);
	-moz-box-shadow: inset 0px -3px 33px 0px rgba(0,0,0,0.75);
	box-shadow: inset 0px -3px 33px 0px rgba(0,0,0,0.75);
}

button:hover {
	color: #ffffff;
	background-color: #333;
}

input[type="number"], textarea {
	color: #ffffff;
	background-color: #383b3f;
	width: 100px;
	border: none;
	padding: 10px 15px;
	font-size: 12px;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
}

textarea {
	width: 413px;
	height: 50px;
}

select, select option {
	font-size: 12px;
	border: none;
	color: #ffffff;
	background-color: #383b3f;
	padding: 10px 15px;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
}

input[type="checkbox"] {
	display:none;
}

input[type=checkbox] + label {
	background: url('res/img/occupied.png');
	background-size: 20px 20px;
	background-repeat: no-repeat;
	display: inline-block;
	width: 20px;
	height: 20px;
	cursor: pointer;
	-webkit-transition: all 0.25s linear;
}

input[type=checkbox]:checked + label {
	background: url('res/img/ok.png');
	background-size: 20px 20px;
	background-repeat: no-repeat;
}

table#options_table {
	border: none;
	width: 100%;
	border-collapse: separate;
	border-spacing: 5px;
	-webkit-touch-callout: none;
	-webkit-user-select: none;
	-khtml-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

table#options_table tr td {
	vertical-align: top;
}

table#options_table tr td:nth-child(1) {
	width: 30%;
}

table#options_table tr td:nth-child(2) {
	width: 70%;
}

img.macro_delete {
	cursor: pointer;
}
</style>
<div>
	<table id="options_table">
		<tr>
			<td><?=$_DICTIONARY["name"]?>: </td>
			<td><textarea id="in_macro_name"></textarea></td>
		</tr>
		<tr id="command_tr_master" style="display: none;">
			<td><?=$_DICTIONARY["command"]?> #<span class="cmd_id"></span>: <img class="macro_delete" title="delete" src="res/img/close_dark.png" /></td>
			<td>
				<table>
					<tr>
						<td style="width: 500px;"><?=$_DICTIONARY["type"]?></td>
						<td><select class="in_macro_type">
							<option value="value" selected><?=$_DICTIONARY["state_change"]?></option>
							<option value="pulse"><?=$_DICTIONARY["pulse"]?></option>
						</select></td>
					</tr>
					<tr>
						<td><?=$_DICTIONARY["control_unit"]?></td>
						<td><select class="in_cu">
							<?php $rsCUs = $dbServer->query("SELECT software_address, description FROM control_units ORDER BY software_address ASC"); ?>
							<?php foreach($rsCUs as $row){ ?>
								<option value="<?=$row[0]?>"><?=$row[0]?><?=($row[1]=="" ? "" : " > ".$row[1])?></option>
							<?php }; ?>
						</select></td>
					</tr>
					<tr class="type_state_change">
						<td><?=$_DICTIONARY["state"]?></td>
						<td><input class="in_state" type="number" value="0" /></td>
					</tr>
					<tr class="type_pulse" style="display: none;">
						<td><?=$_DICTIONARY["state_on"]?></td>
						<td><input class="in_state_on" type="number" value="1" /></td>
					</tr>
					<tr class="type_pulse" style="display: none;">
						<td><?=$_DICTIONARY["state_off"]?></td>
						<td><input class="in_state_off" type="number" value="0" /></td>
					</tr>
					<tr class="type_pulse" style="display: none;">
						<td><?=$_DICTIONARY["repeat_count"]?></td>
						<td><input class="in_repeat_count" type="number" value="1" /></td>
					</tr>
					<tr class="type_pulse" style="display: none;">
						<td><?=$_DICTIONARY["duration"]?></td>
						<td><input class="in_duration" type="number" value="1000" /> <?=$_DICTIONARY["ms"]?></td>
					</tr>
					<tr>
						<td><?=$_DICTIONARY["delay"]?></td>
						<td><input class="in_delay" type="number" value="0" /> <?=$_DICTIONARY["ms"]?></td>
					</tr>
					<tr class="type_pulse" style="display: none;">
						<td><?=$_DICTIONARY["block_motion_sensor"]?></td>
						<td><input class="in_block_motion_sensor" type="checkbox" id="in_block_motion_sensor" checked /><label for="in_block_motion_sensor">&nbsp;</label></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr id="btns_tr">
			<td></td>
			<td><button id="in_add_cmd"><?=$_DICTIONARY["add_command"]?></button><button id="in_save"><?=$_DICTIONARY["save"]?></button><button id="in_cancel"><?=$_DICTIONARY["cancel"]?></button></td>
		</tr>
	</table>
</div>
<script>
var cmd_counter = 1;
$(document).ready(function() {
	$("button#in_cancel").click(function() {
		location.href = "?page=macros";
	});
	$("button#in_save").click(function() {
		var name = $("#in_macro_name").val();
		var command = "";

		$("#options_table").find(".in_command").each(function(idx) {
			if(idx != 0)
				command += ";";
			var elem = $(this);
			var type = elem.find(".in_macro_type").val();
			var cu = elem.find(".in_cu").val();
			var delay = elem.find(".in_delay").val();
			command += "type=" + type + ",cu=" + cu + ",delay=" + delay + ",";
			if(type == "value") {
				var state = elem.find(".in_state").val();
				command += "value=" + state;
			} else { //pulse
				var stateOn = elem.find(".in_state_on").val();
				var stateOff = elem.find(".in_state_off").val();
				var repeat = elem.find(".in_repeat_count").val();
				var duration = elem.find(".in_duration").val();
				var blockMotionSensor = elem.find(".in_block_motion_sensor").is(":checked") ? "true" : "false";
				command += "valueOn="+stateOn+",valueOff="+stateOff+",repeat="+repeat+",duration="+duration+",blockMotionSensor="+blockMotionSensor;
			}
		});

		var urlGetData = "name="+name+"&command="+encodeURI(command.replaceAll("=", "[eq]"));
		$.get("phpscript/insertNewMacro.php?"+urlGetData, function(data) {
			if(data.trim() == "OK")
				location.href = "?page=macros";
			else
				alert("<?=$_DICTIONARY["save_nmacro_fail"]?>");
		});
	});

	$("button#in_add_cmd").click(function() {
		var elem = $("#command_tr_master").clone();
		elem.detach();
		elem.removeAttr("id");
		elem.css("display", "table-row");
		elem.addClass("in_command");
		elem.find(".cmd_id").html(cmd_counter);
		elem.insertBefore("#btns_tr");
		elem.find("img.macro_delete").click(function() {
			elem.remove();
		});
		elem.find("select.in_macro_type").change(function() {
			var elem = $(this);
			var parentElem = elem.parent().parent().parent();
			if(elem.val() == "value") {
				parentElem.find(".type_state_change").css("display", "table-row")
				parentElem.find(".type_pulse").css("display", "none");
			} else {
				parentElem.find(".type_state_change").css("display", "none")
				parentElem.find(".type_pulse").css("display", "table-row");
			}
		});
		elem.find("#in_block_motion_sensor").attr("id", "in_block_motion_sensor" + cmd_counter);
		elem.find("label[for='in_block_motion_sensor']").attr("for", "in_block_motion_sensor" + cmd_counter);

		cmd_counter++;
	});
});
</script>
