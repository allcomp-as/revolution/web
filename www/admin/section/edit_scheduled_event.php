<?php
$seID = $httpRequest->getQuery('id');
$rowSE = $dbServer->fetch("
	SELECT
		id,
		control_unit,
		days,
		time_start,
		duration_minutes,
		metadata,
		conditions,
		type
	FROM scheduled_events
	WHERE
		id = ?
	",
	$seID
);

if(!$rowSE){
	return;
}
?>
<style>
span input[type=radio] {
	position: absolute;
	visibility: hidden;
}

span label {
	display: inline-block;
	font-size: 12px;
	padding: 10px 15px;
	cursor: pointer;
	-webkit-transition: all 0.25s linear;
	background-color: #555;
	color: #dbd9d9;
	font-style: italic;
}

span:first-child label {
	-webkit-border-top-left-radius: 10px;
	-webkit-border-bottom-left-radius: 10px;
	-moz-border-radius-topleft: 10px;
	-moz-border-radius-bottomleft: 10px;
	border-top-left-radius: 10px;
	border-bottom-left-radius: 10px;
}

span:last-child label {
	-webkit-border-top-right-radius: 10px;
	-webkit-border-bottom-right-radius: 10px;
	-moz-border-radius-topright: 10px;
	-moz-border-radius-bottomright: 10px;
	border-top-right-radius: 10px;
	border-bottom-right-radius: 10px;
}

button#in_save, button#in_cancel {
	background-color: #555;
	color: #dbd9d9;
	width: 130px;
	height: 40px;
	line-height: 30px;
	text-align: center;
	border: none;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
	font-size: 15px;
	margin-top: 15px;
	cursor: pointer;
	margin-right: 5px;
	-webkit-transition: all 0.25s linear;
	-webkit-box-shadow: inset 0px -3px 33px 0px rgba(0,0,0,0.75);
	-moz-box-shadow: inset 0px -3px 33px 0px rgba(0,0,0,0.75);
	box-shadow: inset 0px -3px 33px 0px rgba(0,0,0,0.75);
}

button#in_every_day, button#in_every_wday, button#in_no_day, button#in_w_day {
	background-color: #555;
	color: #dbd9d9;
	padding: 0 15px;
	height: 30px;
	line-height: 30px;
	text-align: center;
	border: none;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
	font-size: 12px;
	cursor: pointer;
	margin-right: 5px;
	-webkit-transition: all 0.25s linear;
}

button#in_every_day:hover, button#in_every_wday:hover, button#in_no_day:hover, button#in_w_day:hover {
	color: #ffffff;
	background-color: #333;
}

span label:hover {
	color: #ffffff;
}

input[type=radio]:checked ~ label{
	background-color: #333;
	color: #ffffff;
}

input[type="number"], input[type="text"], textarea {
	color: #ffffff;
	background-color: #383b3f;
	width: 100px;
	border: none;
	padding: 10px 15px;
	font-size: 12px;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
}

textarea {
	width: 413px;
	height: 50px;
}

input[type="text"] {
	width: 150px;
}

input[type="checkbox"] {
	display:none;
}

p.in_days {
	padding: 5px;
	display: inline-block;
	width: 100px;
}

p.in_days input[type=checkbox] + label {
	background: url('res/img/occupied.png');
	background-size: 20px 20px;
	background-repeat: no-repeat;
	display: inline-block;
	width: 20px;
	height: 20px;
	cursor: pointer;
	-webkit-transition: all 0.25s linear;
	padding-left: 25px;
}

p.in_days input[type=checkbox]:checked + label {
	background: url('res/img/ok.png');
	background-size: 20px 20px;
	background-repeat: no-repeat;
}

p.security_check_unit input[type=checkbox] + label {
	background: url('res/img/security.png');
	background-size: 20px 20px;
	background-repeat: no-repeat;
	display:inline-block;
	padding: 0 0 0 25px;
	width: 100%;
	height: 100%;
	cursor: pointer;
	font-size: 12px;
	color: #dbd9d9;
	font-style: italic;
	-webkit-transition: all 0.25s linear;
}

p.security_check_unit input[type=checkbox] + label:hover {
	color: #ffffff;
}

p.security_check_unit input[type=checkbox]:checked + label {
	background: url('res/img/security_filled.png');
	background-size: 20px 20px;
	background-repeat: no-repeat;
	color: #ffffff;
}

p.security_check_unit {
	display: inline-block;
	width: 200px;
	padding: 10px;
	background-color: #383b3f;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
	line-height: 20px;
}

div#security_sectors_array {
	line-height: 45px;
}

select, select option {
	font-size: 12px;
	border: none;
	color: #ffffff;
	background-color: #383b3f;
	padding: 10px 15px;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
}

table#options_table {
	border: none;
	width: 100%;
	border-collapse: separate;
	border-spacing: 5px;
	-webkit-touch-callout: none;
	-webkit-user-select: none;
	-khtml-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

table#options_table tr td {
	vertical-align: top;
}

table#options_table tr td:nth-child(1) {
	width: 30%;
}

table#options_table tr td:nth-child(2) {
	width: 70%;
}

img#swid_state {
	width: 20px;
	height: 20px;
	transform: translate(0, 5px);
}
</style>
<?php
	$id = $rowSE[0];
	$cu = $rowSE[1];
	$days = $rowSE[2];
	$time_start = $rowSE[3];
	$duration_minutes = $rowSE[4];
	$metadata = $rowSE[5];
	$conditions = $rowSE[6];
	$type = $rowSE[7];
?>
<div>
	<table id="options_table">
		<tr>
			<td><?=$_DICTIONARY["control_unit"]?>: </td>
			<td><select id="in_control_unit">
				<?php $rsCUs = $dbServer->query("SELECT software_address, description FROM control_units ORDER BY software_address ASC"); ?>
				<?php foreach($rsCUs as $row){ ?>
					<option value="<?=$row[0]?>"<?php if($cu==$row[0]) echo(" selected");?>><?=$row[0]?><?=($row[1]=="" ? "" : " > ".$row[1])?></option>
				<?php }; ?>
			</select></td>
		</tr>
		<tr>
			<td><?=$_DICTIONARY["days"]?>: </td>
			<td>
				<p class="in_days"><input type="checkbox" id="in_days_0" <?php if(substr($days, 0, 1) == "1") echo(" checked");?>/> <label for="in_days_0"><?=$_DICTIONARY["monday"]?></label></p>
				<p class="in_days"><input type="checkbox" id="in_days_1" <?php if(substr($days, 1, 1) == "1") echo(" checked");?>/> <label for="in_days_1"><?=$_DICTIONARY["tuesday"]?></label></p>
				<p class="in_days"><input type="checkbox" id="in_days_2" <?php if(substr($days, 2, 1) == "1") echo(" checked");?>/> <label for="in_days_2"><?=$_DICTIONARY["wednesday"]?></label></p>
				<p class="in_days"><input type="checkbox" id="in_days_3" <?php if(substr($days, 3, 1) == "1") echo(" checked");?>/> <label for="in_days_3"><?=$_DICTIONARY["thursday"]?></label></p>
				<p class="in_days"><input type="checkbox" id="in_days_4" <?php if(substr($days, 4, 1) == "1") echo(" checked");?>/> <label for="in_days_4"><?=$_DICTIONARY["friday"]?></label></p>
				<p class="in_days"><input type="checkbox" id="in_days_5" <?php if(substr($days, 5, 1) == "1") echo(" checked");?>/> <label for="in_days_5"><?=$_DICTIONARY["saturday"]?></label></p>
				<p class="in_days"><input type="checkbox" id="in_days_6" <?php if(substr($days, 6, 1) == "1") echo(" checked");?>/> <label for="in_days_6"><?=$_DICTIONARY["sunday"]?></label></p>
				<p><button id="in_every_day"><?=$_DICTIONARY["every_day"]?></button><button id="in_every_wday"><?=$_DICTIONARY["working_days"]?></button><button id="in_w_day"><?=$_DICTIONARY["weekends"]?></button><button id="in_no_day"><?=$_DICTIONARY["never"]?></button></p>
			</td>
		</tr>
		<tr>
			<td><?=$_DICTIONARY["start"]?>: </td>
			<td><!--
				--><input style="width: 40px;" placeholder="HH" id="in_start_hour" type="number" min="0" max="23" step="1" value="<?=substr($time_start, 0, 2)?>" onchange="if(parseInt(this.value,10)<10) this.value='0'+this.value; if(parseInt(this.value,10)<0) this.value='0'; if(parseInt(this.value,10)>23) this.value='23';" /><!--
				--><b>:</b><!--
				--><input style="width: 40px;" placeholder="MM" id="in_start_minute" type="number" min="0" max="59" step="1" value="<?=substr($time_start, 3, 2)?>"  onchange="if(parseInt(this.value,10)<10) this.value='0'+this.value; if(parseInt(this.value,10)<0) this.value='00'; if(parseInt(this.value,10)>59) this.value='59';" /><!--
			--> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["start"]?></b></p>"+
				"<p><?=$_DICTIONARY["start_desc"]?></p>"+
				"<p style=\"margin-top: 10px;\"><b><?=$_DICTIONARY["notice"]?></b></p>"+
				"<p><?=$_DICTIONARY["notice_desc"]?></p>"
			);</script></td>
		</tr>
		<tr>
			<td><?=$_DICTIONARY["duration"]?>: </td>
			<td><input placeholder="<?=$_DICTIONARY["duration"]?>" id="in_duration" type="number" min="1" step="1" value="<?=$duration_minutes?>"  onchange="if(parseInt(this.value,10)<1) this.value='1';" /> <?=$_DICTIONARY["minutes"]?></td>
		</tr>
		<tr>
			<td><?=$_DICTIONARY["type"]?>: </td>
			<td><select id="in_type">
				<option value="0" <?php if($type == "0") echo(" selected");?>><?=$_DICTIONARY["ordinary"]?></option>
				<option value="1" <?php if($type == "1") echo(" selected");?>><?=$_DICTIONARY["periodic"]?></option>
			</select> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["type"]?></b></p>"+
				"<p><?=$_DICTIONARY["type_desc"]?></p>"+
				"<p style=\"margin-top: 10px;\"><b><?=$_DICTIONARY["ordinary_type"]?></b> <?=$_DICTIONARY["ordinary_type_desc"]?></p>"+
				"<p style=\"margin-top: 10px;\"><b><?=$_DICTIONARY["periodic_type"]?></b> <?=$_DICTIONARY["periodic_type_desc"]?></p>"
			);</script></td>
		</tr>
		<tr id="metadata_type_0">
			<td><?=$_DICTIONARY["active_state"]?>: </td>
			<td><input placeholder="<?=$_DICTIONARY["active_state"]?>" id="in_active_state" type="number" step="1" value="1" /> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["active_state"]?></b></p>"+
				"<p><?=$_DICTIONARY["active_state_desc"]?></p>"
			);</script></td>
		</tr>
		<tr id="metadata_type_1">
			<td><?=$_DICTIONARY["passive_state"]?>: </td>
			<td><input placeholder="<?=$_DICTIONARY["passive_state"]?>" id="in_inactive_state" type="number" step="1" value="0" /> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["passive_state"]?></b></p>"+
				"<p><?=$_DICTIONARY["passive_state_desc"]?></p>"
			);</script></td>
		</tr>
		<tr id="metadata_type_2">
			<td><?=$_DICTIONARY["active_state_duration"]?>: </td>
			<td><input style="width: 140px;" placeholder="<?=$_DICTIONARY["active_state_duration"]?>" id="in_active_state_duration" type="number" step="50" value="1000" /> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["active_state_duration"]?></b></p>"+
				"<p><?=$_DICTIONARY["active_state_duration_desc"]?></p>"+
				"<p style=\"margin-top: 10px;\">1 s = 1000 ms.</p>"+
				"<p>1 m = 60000 ms.</p>"+
				"<p>1 h = 3600000 ms.</p>"
			);</script></i></td>
		</tr>
		<tr id="metadata_type_3">
			<td><?=$_DICTIONARY["passive_state_duration"]?>: </td>
			<td><input style="width: 140px;" placeholder="<?=$_DICTIONARY["passive_state_duration"]?>" id="in_inactive_state_duration" type="number" step="50" value="1000" /> <script>SHS.docs(16,
				"<p><b><?=$_DICTIONARY["passive_state_duration"]?></b></p>"+
				"<p><?=$_DICTIONARY["passive_state_duration_desc"]?></p>"+
				"<p style=\"margin-top: 10px;\">1 s = 1000 ms.</p>"+
				"<p>1 m = 60000 ms.</p>"+
				"<p>1 h = 3600000 ms.</p>"
			);</script></td>
		</tr>
		<tr>
			<td><?=$_DICTIONARY["conditions"]?>: </td>
			<td><textarea id="in_conditions"><?=$conditions?></textarea></td>
		</tr>
		<tr>
			<td></td>
			<td><button id="in_save"><?=$_DICTIONARY["save"]?></button><button id="in_cancel"><?=$_DICTIONARY["cancel"]?></button></td>
		</tr>
	</table>
</div>
<script>
$(document).ready(function() {
	var initialMetadata = "<?=$metadata?>";
	var imetadataArray = initialMetadata.split(";");
	var type = "<?=$type?>";
	
	for(var i = 0; i <= 3; i++)
		$("tr#metadata_type_"+i).addClass("hidden");
		
	switch(type) {
		case "0":
			$("tr#metadata_type_0").removeClass("hidden");
			$("tr#metadata_type_1").removeClass("hidden");
			for(var i in imetadataArray) {
				var pair = imetadataArray[i].split("=");
				if(pair[0] == "stateOn")
					$("#in_active_state").val(pair[1]);
				else if(pair[0] == "stateOff")
					$("#in_inactive_state").val(pair[1]);
			}
			break;
		case "1":
			$("tr#metadata_type_0").removeClass("hidden");
			$("tr#metadata_type_1").removeClass("hidden");
			$("tr#metadata_type_2").removeClass("hidden");
			$("tr#metadata_type_3").removeClass("hidden");
			for(var i in imetadataArray) {
				var pair = imetadataArray[i].split("=");
				if(pair[0] == "stateOn")
					$("#in_active_state").val(pair[1]);
				else if(pair[0] == "stateOff")
					$("#in_inactive_state").val(pair[1]);
				else if(pair[0] == "timeOn")
					$("#in_active_state_duration").val(pair[1]);
				else if(pair[0] == "timeOff")
					$("#in_inactive_state_duration").val(pair[1]);
			}
			break;
	}
	
	$("select#in_type").change(function() {
		if($(this).val() == "0") {
			if(!$("tr#metadata_type_2").hasClass("hidden"))
				$("tr#metadata_type_2").addClass("hidden");
			if(!$("tr#metadata_type_3").hasClass("hidden"))
				$("tr#metadata_type_3").addClass("hidden");
		} else {
			if($("tr#metadata_type_2").hasClass("hidden"))
				$("tr#metadata_type_2").removeClass("hidden");
			if($("tr#metadata_type_3").hasClass("hidden"))
				$("tr#metadata_type_3").removeClass("hidden");
		}
	});
	
	$("button#in_cancel").click(function() {
		location.href = "?page=scheduled_events";
	});
	
	$("button#in_save").click(function() {
		var cu = $("select#in_control_unit").val();
		var days = "";
		for(var i = 0; i < 7; i++)
			days += $("input#in_days_"+i).is(":checked") ? "1" : "0";
		var time = $("input#in_start_hour").val() + ":" + $("input#in_start_minute").val();
		var duration = $("input#in_duration").val();
		var type = $("select#in_type").val();
		var conditions = $("textarea#in_conditions").val();
		var metadata = "";
		switch(type) {
			case "0":
				var stateOn = $("input#in_active_state").val();
				var stateOff = $("input#in_inactive_state").val();
				if(stateOn != "1") metadata += ";stateOn=" + stateOn;
				if(stateOff != "0") metadata += ";stateOff=" + stateOff;
				break;
			case "1":
				var stateOn = $("input#in_active_state").val();
				var stateOff = $("input#in_inactive_state").val();
				var timeOn = $("input#in_active_state_duration").val();
				var timeOff  = $("input#in_inactive_state_duration").val();
				if(stateOn != "1") metadata += ";stateOn=" + stateOn;
				if(stateOff != "0") metadata += ";stateOff=" + stateOff;
				if(timeOn != "1000") metadata += ";timeOn=" + timeOn;
				if(timeOff != "1000") metadata += ";timeOff=" + timeOff;
				break;
		}
		
		if(metadata.length > 0)
			metadata = metadata.substring(1);
		
		//alert("cu:"+cu+", days:" + days + ", time:" + time + ", duration:" + duration + ", type:" + type + ", metadata:" + metadata + ", conditions:" + conditions);
		
		var urlGetData = "id=<?=$id?>&cu="+cu+"&days="+days+"&time="+time+"&duration="+duration+"&type="+type+"&metadata="+metadata+"&conditions="+conditions;
		
		//alert(urlGetData);
		$.get("phpscript/updateScheduledEvent.php?"+urlGetData, function(data) {
			if(data.trim() == "OK")
				location.href = "?page=scheduled_events";
			else
				alert("<?=$_DICTIONARY["save_csche_fail"]?>");
		});
	});
});
</script>
