<?php
require_once __DIR__ . '/../../../app/admin-loggedIn.php';


$dbServer->query("INSERT INTO control_units", [
	'software_address' => $httpRequest->getQuery('software_address'),
	'hardware_address' => $httpRequest->getQuery('hardware_address'),
	'value_type' => $httpRequest->getQuery('value_type'),
	'security' => $httpRequest->getQuery('security'),
	'description' => $httpRequest->getQuery('description'),
	'active_state' => $httpRequest->getQuery('active_state'),
	'simulate' => $httpRequest->getQuery('simulate'),
	'activation_delay' => $httpRequest->getQuery('activation_delay'),
	'ewc_version' => $httpRequest->getQuery('ewc_version'),
	'metadata' => $httpRequest->getQuery('metadata')
]);

echo 'OK';
