<?php
require_once __DIR__ . '/../../../app/admin-loggedIn.php';


$dbServer->query(
	"UPDATE trigger_events SET", [
		'trigger' => $httpRequest->getQuery('trigger'),
		'target' => $httpRequest->getQuery('target'),
		'group_id' => $httpRequest->getQuery('group_id'),
		'type' => $httpRequest->getQuery('type'),
		'metadata' => $httpRequest->getQuery('metadata'),
		'activation_conditions' => $httpRequest->getQuery('activation_conditions'),
		'deactivation_conditions' => $httpRequest->getQuery('deactivation_conditions')
	],
	"WHERE id=?", $httpRequest->getQuery('id')
);

echo 'OK';
