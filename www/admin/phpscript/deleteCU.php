<?php
require_once __DIR__ . '/../../../app/admin-loggedIn.php';


$id = $httpRequest->getQuery('software_address');
$dbServer->query("DELETE FROM control_units WHERE software_address=?", $id);
$dbServer->query("DELETE FROM trigger_events WHERE trigger=? OR target=?", $id, $id);
$dbServer->query("DELETE FROM scheduled_events WHERE control_unit=?", $id);
$dbServer->query("DELETE FROM security_events WHERE control_unit=?", $id);
$dbServer->query("DELETE FROM output_states WHERE software_id=?", $id);
$dbServer->query("DELETE FROM thermostats WHERE control_unit=?", $id);

echo $id;
