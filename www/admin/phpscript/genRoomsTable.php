<?php
require_once __DIR__ . '/../../../app/admin-loggedIn.php';

$db = $dbWeb;

$select = '
	id,
	name,
	floor
';
$from = 'rooms';
$joins = '';
$where = [];

$orderDefault = 'id';
$orderTable = [
	'id' => 'id',
	'name' => 'name',
	'floor' => 'floor'
];

$searchCols = [
	'name',
	'floor'
];

$rs = require DIR_APP . '/admin-genX.php';

if($rs):
?>
	<table id="rooms_table">
	<tr>
		<th><?=$_DICTIONARY["name"]?></th>
		<th><?=$_DICTIONARY["floor"]?></th>
		<th><img class="room_add" src="res/img/add.png" /></th>
	</tr>
	<?php while($row = $rs->fetch()):?>
	<tr>
		<td><?=($row[1])?></td>
		<td><?=($row[2])?></td>
		<td><img class="room_edit" src="res/img/edit.png" data-dbid="<?=($row[0])?>" /> <img class="room_delete" src="res/img/close.png" data-dbid="<?=($row[0])?>" /></td>
	</tr>
	<?php endwhile; ?>
	</table>
<?php endif; ?>
