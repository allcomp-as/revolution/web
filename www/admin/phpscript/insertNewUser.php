<?php
require_once __DIR__ . '/../../../app/admin-loggedIn.php';


try {
	$dbWeb->query("INSERT INTO users", [
		'username' => $httpRequest->getQuery('username'),
		'password' => md5($httpRequest->getQuery('password')),
		'permission_level' => 1,
		'last_ip' => $httpRequest->remoteAddress
	]);
	
	echo 'OK';
}
catch(Nette\Database\UniqueConstraintViolationException $e){
	echo 'ERR';
}
