<?php
require_once __DIR__ . '/../../../app/admin-loggedIn.php';

$db = $dbWeb;

$select = '
	id,
	name
';
$from = 'macros';
$joins = '';
$where = [];

$orderDefault = 'id';
$orderTable = [
	'id' => 'id',
	'name' => 'name'
];

$searchCols = [
	'name'
];

$rs = require DIR_APP . '/admin-genX.php';

if($rs):
?>
	<table id="macros_table">
	<tr>
		<th><?=$_DICTIONARY["name"]?></th>
		<th><img class="macro_add" src="res/img/add.png" /></th>
	</tr>
	<?php while($row = $rs->fetch()):?>
	<tr>
		<td><?=($row[1])?></td>
		<td><img class="macro_edit" src="res/img/edit.png" data-dbid="<?=($row[0])?>" /> <img class="macro_delete" src="res/img/close.png" data-dbid="<?=($row[0])?>" /></td>
	</tr>
	<?php endwhile; ?>
	</table>
<?php endif; ?>
