<?php
require_once __DIR__ . '/../../../app/admin-loggedIn.php';


$dbWeb->query(
	"UPDATE controls SET", [
		'room' => $httpRequest->getQuery('room'),
		'name' => $httpRequest->getQuery('name'),
		'outputs' => $httpRequest->getQuery('outputs'),
		'type' => $httpRequest->getQuery('type'),
		'icon' => $httpRequest->getQuery('icon')
	],
	"WHERE id=?", $httpRequest->getQuery('id')
);

echo 'OK';
