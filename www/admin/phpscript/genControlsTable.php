<?php
require_once __DIR__ . '/../../../app/admin-loggedIn.php';

$db = $dbWeb;

$select = '
	controls.id,
	controls.name,
	controls.outputs,
	rooms.name AS room
';
$from = 'controls';
$joins = '
	LEFT JOIN rooms
		ON (controls.room = rooms.id)
';
$where = [];

$orderDefault = 'id';
$orderTable = [
	'id' => 'controls.id',
	'name' => 'controls.name',
	'outputs̈́' => 'controls.outputs',
	'room' => 'rooms.name'
];

$searchCols = [
	'controls.name',
	'controls.outputs',
	'rooms.name'
];

$rs = require DIR_APP . '/admin-genX.php';

if($rs):
?>
	<table id="controls_table">
	<tr>
		<th><?=$_DICTIONARY["name"]?></th>
		<th><?=$_DICTIONARY["room"]?></th>
		<th><?=$_DICTIONARY["output_units"]?></th>
		<th><img class="control_add" src="res/img/add.png" /></th>
	</tr>
	<?php while($row = $rs->fetch()):?>
	<tr>
		<td><?=($row['name'])?></td>
		<td><?=($row['room'])?></td>
		<td><?=($row['outputs'])?></td>
		<td><img class="control_edit" src="res/img/edit.png" data-dbid="<?=($row['id'])?>" /> <img class="control_delete" src="res/img/close.png" data-dbid="<?=($row['id'])?>" /></td>
	</tr>
	<?php endwhile; ?>
	</table>
<?php endif; ?>
