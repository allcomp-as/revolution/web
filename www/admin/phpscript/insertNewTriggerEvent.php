<?php
require_once __DIR__ . '/../../../app/admin-loggedIn.php';


$dbServer->query("INSERT INTO trigger_events", [
	'trigger' => $httpRequest->getQuery('trigger'),
	'target' => $httpRequest->getQuery('target'),
	'type' => $httpRequest->getQuery('type'),
	'metadata' => $httpRequest->getQuery('metadata'),
	'group_id' => $httpRequest->getQuery('group_id'),
	'activation_conditions' => $httpRequest->getQuery('activation_conditions'),
	'deactivation_conditions' => $httpRequest->getQuery('deactivation_conditions')
]);

echo 'OK';
