<?php
require_once __DIR__ . '/../../../app/admin-loggedIn.php';

$db = $dbServer;

$select = '
	id,
	software_address,
	hardware_address,
	value_type,
	description,
	ewc_version
';
$from = 'control_units';
$joins = '';
$where = [];

$orderDefault = 'id';
$orderTable = [
	'id' => 'id',
	'swa' => 'software_address',
	'hwa' => 'hardware_address',
	'vt' => 'value_type',
	'desc' => 'description'
];

$searchCols = [
	'software_address',
	'hardware_address',
	'description',
	'value_type'
];

$rs = require DIR_APP . '/admin-genX.php';

if($rs):
	$value_types = array($_DICTIONARY["digital"], $_DICTIONARY["temperature"], $_DICTIONARY["pwm"]);
?>
	<table id="cu_table">
	<tr>
		<th><?=$_DICTIONARY["id"]?></th>
		<th><?=$_DICTIONARY["hw_addr"]?></th>
		<th><?=$_DICTIONARY["value_type"]?></th>
		<th><?=$_DICTIONARY["description"]?></th>
		<th><img class="cu_add" src="res/img/add.png" /></th>
	</tr>
	<?php while($row = $rs->fetch()):?>
	<?php
		$ewc_version = $row[5];
		$isVirtual = !(substr($row[2], 0, 3) === "EWC");
		$ewcID = $isVirtual ? "" : substr($row[2], 3, 2);
		$ioType = $isVirtual ? "IN" : (substr($row[2], 6, 3) === "OUT" ? "OUT" : "IN");
		$ioID = $isVirtual ? substr($row[2], 7) : ($ioType == "IN" ? substr($row[2],8) : substr($row[2],9));
		$pinvalues = array();
		if($ioType == "OUT") {
			if($ewc_version == "1")
				$pinvalues = array(0,5,1,6,2,7,3,8,4,9);
			else
				$pinvalues = array(0,1,2,3,4,5,6,7,8,9);
		} else {
			if($ewc_version == "1")
				$pinvalues = array(24,25,16,17,18,19,20,21,23,22);
			else
				$pinvalues = array(16,17,18,19,20,21,22,23,25,24);
		}
	?>
	<tr>
		<td><?=($row[1])?></td>
		<td><?=($row[2])?><?php if(!$isVirtual) { echo(" (pin ".(array_search($ioID, $pinvalues)+1).")"); } ?></td>
		<td><?=($row[3] . " <i>(" . $value_types[$row[3]] . ")</i>")?></td>
		<td><?=($row[4])?></td>
		<td><img class="cu_edit" src="res/img/edit.png" data-dbid="<?=($row[0])?>" /> <img class="cu_delete" src="res/img/close.png" data-software_address="<?=($row[1])?>" /></td>
	</tr>
	<?php endwhile; ?>
	</table>
<?php endif; ?>
