<?php
require_once __DIR__ . '/../../../app/admin-loggedIn.php';

$db = $dbServer;

$select = '
	trigger_events.id,
	trigger_events.trigger,
	trigger_events.target,
	trigger_events.group_id,
	trigger_info.description AS trigger_description,
	target_info.description AS target_description
';
$from = 'trigger_events';
$joins = '
	LEFT JOIN control_units AS trigger_info
		ON (trigger_info.software_address = trigger_events.trigger)
	LEFT JOIN control_units AS target_info
		ON (target_info.software_address = trigger_events.target)
';
$where = [];

$orderDefault = 'id';
$orderTable = [
	'id' => 'trigger_events.id',
	'trigger' => 'trigger_events.trigger',
	'trigger_desc' => 'trigger_info.description',
	'target' => 'trigger_events.target',
	'target_desc' => 'target_info.description',
	'group' => 'trigger_events.group_id'
];

$searchCols = [
	'trigger_events.trigger',
	'trigger_events.target',
	'trigger_info.description',
	'target_info.description',
	'trigger_events.group_id'
];

$rs = require DIR_APP . '/admin-genX.php';

if($rs):
?>
	<table id="se_table">
	<tr>
		<th colspan="2"><?=$_DICTIONARY["input"]?></th>
		<th colspan="2"><?=$_DICTIONARY["output"]?></th>
		<th rowspan="2"><?=$_DICTIONARY["group"]?></th>
		<th rowspan="2"><img class="te_add" src="res/img/add.png" /></th>
	</tr>
	<tr>
		<th><?=$_DICTIONARY["id"]?></th>
		<th><?=$_DICTIONARY["description"]?></th>
		<th><?=$_DICTIONARY["id"]?></th>
		<th><?=$_DICTIONARY["description"]?></th>
	</tr>
	<?php while($row = $rs->fetch()):?>
	<tr>
		<td><?=($row[1])?></td>
		<td><?=($row[4])?></td>
		<td><?=($row[2])?></td>
		<td><?=($row[5])?></td>
		<td><?=($row[3])?></td>
		<td><img class="te_edit" src="res/img/edit.png" data-dbid="<?=($row[0])?>" /> <img class="te_delete" src="res/img/close.png" data-dbid="<?=($row[0])?>" /> <img class="te_add" src="res/img/add.png" data-group_id="<?=($row[3])?>" /></td>
	</tr>
	<?php endwhile; ?>
	</table>
<?php endif; ?>
