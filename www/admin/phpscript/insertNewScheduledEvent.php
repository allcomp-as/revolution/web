<?php
require_once __DIR__ . '/../../../app/admin-loggedIn.php';


$dbServer->query("INSERT INTO scheduled_events", [
	'control_unit' => $httpRequest->getQuery('cu'),
	'days' => $httpRequest->getQuery('days'),
	'time_start' => $httpRequest->getQuery('time'),
	'duration_minutes' => $httpRequest->getQuery('duration'),
	'metadata' => $httpRequest->getQuery('metadata'),
	'conditions' => $httpRequest->getQuery('conditions'),
	'type' => $httpRequest->getQuery('type')
]);

echo 'OK';
