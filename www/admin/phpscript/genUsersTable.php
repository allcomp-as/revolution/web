<?php
require_once __DIR__ . '/../../../app/admin-loggedIn.php';

$db = $dbWeb;

$select = '
	id,
	username,
	last_ip
';
$from = 'users';
$joins = '';
$where = [];

$orderDefault = 'id';
$orderTable = [
	'id' => 'id',
	'username' => 'username'
];

$searchCols = [
	'username',
	'last_ip'
];

$rs = require DIR_APP . '/admin-genX.php';

if($rs):
?>
	<table id="users_table">
	<tr>
		<th><?=$_DICTIONARY["username"]?></th>
		<th><?=$_DICTIONARY["last_ip"]?></th>
		<th><img class="user_add" src="res/img/add.png" /></th>
	</tr>
	<?php while($row = $rs->fetch()):?>
	<tr>
		<td><?=($row[1])?></td>
		<td><?=($row[2])?></td>
<td><img class="user_edit" src="res/img/edit.png" data-dbid="<?=($row[0])?>" /><?php if($row[1] != "Admin") :?> <img class="user_delete" src="res/img/close.png" data-dbid="<?=($row[0])?>" /><?php endif; ?></td>
	</tr>
	<?php endwhile; ?>
	</table>
<?php endif; ?>
