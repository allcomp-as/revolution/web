<?php
require_once __DIR__ . '/../../../app/admin-loggedIn.php';

$db = $dbServer;

$select = '
	scheduled_events.id,
	scheduled_events.control_unit,
	control_units.description,
	scheduled_events.days,
	scheduled_events.time_start,
	scheduled_events.duration_minutes
';
$from = 'scheduled_events';
$joins = '
	LEFT JOIN control_units
		ON (control_units.software_address = scheduled_events.control_unit)
';
$where = [];

$orderDefault = 'cu';
$orderTable = [
	'cu' => 'scheduled_events.control_unit',
	'desc' => 'control_units.description',
	'days̈́' => 'scheduled_events.days',
	'starẗ́' => 'scheduled_events.time_start',
	'duration' => 'scheduled_events.duration_minutes'
];

$searchCols = [
	'scheduled_events.control_unit',
	'control_units.description',
	'scheduled_events.days',
	'scheduled_events.duration_minutes'
];

$rs = require DIR_APP . '/admin-genX.php';

if($rs):
?>
	<table id="se_table">
	<tr>
		<th colspan="2"><?=$_DICTIONARY["control_unit"]?></th>
		<th rowspan="2"><?=$_DICTIONARY["days"]?></th>
		<th rowspan="2"><?=$_DICTIONARY["start"]?></th>
		<th rowspan="2"><?=$_DICTIONARY["duration"]?></th>
		<th rowspan="2"><img class="se_add" src="res/img/add.png" /></th>
	</tr>
	<tr>
		<th><?=$_DICTIONARY["id"]?></th>
		<th><?=$_DICTIONARY["description"]?></th>
	</tr>
	<?php while($row = $rs->fetch()):?>
	<?php
		$days = $row[3];
		$daysString = "";
		$daysArray = array(
			$_DICTIONARY["sh_monday"],
			$_DICTIONARY["sh_tuesday"],
			$_DICTIONARY["sh_wednesday"],
			$_DICTIONARY["sh_thursday"],
			$_DICTIONARY["sh_friday"],
			$_DICTIONARY["sh_saturday"],
			$_DICTIONARY["sh_sunday"]
		);
		for($i = 0; $i <= 6; $i++)
			if(substr($days,$i,1) == "1")
				$daysString .= ", " . $daysArray[$i];
		if(strlen($daysString) > 0)
			$daysString = substr($daysString,2);
		if($days == "1111111") $daysString = $_DICTIONARY["every_day"];
		if($days == "1111100") $daysString = $_DICTIONARY["working_days"];
		if($days == "0000011") $daysString = $_DICTIONARY["weekends"];
	?>
	<tr>
		<td><?=($row[1])?></td>
		<td><?=($row[2])?></td>
		<td><?=($daysString)?></td>
		<td><?=($row[4])?></td>
		<td><?=($row[5])?> <?=$_DICTIONARY["minutes"]?></td>
		<td><img class="se_edit" src="res/img/edit.png" data-dbid="<?=($row[0])?>" /> <img class="se_delete" src="res/img/close.png" data-dbid="<?=($row[0])?>" /></td>
	</tr>
	<?php endwhile; ?>
	</table>
<?php endif; ?>
