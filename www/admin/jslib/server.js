var SHS = {};

SHS.init = function() {
};

SHS.query = function(callbackFunctionName, params) {
	var script = document.createElement('script');
	script.src = 'proxy.php/'+callbackFunctionName+'/'+params;
	document.getElementsByTagName('head')[0].appendChild(script);
};

SHS.docsGID = 0; //global ID
SHS.docsTexts = {};

SHS.docs = function(size, content) {
	SHS.docsTexts[SHS.docsGID] = content;
	document.write("<img src=\"./res/img/help"+size+".png\" data-docsid=\""+SHS.docsGID+"\" />");
	SHS.docsGID++;
};

var Decoder = {};

Decoder.map = {};
Decoder.map["?chid=0?"] = "á";
Decoder.map["?chid=0L?"] = "á";
Decoder.map["?chid=1?"] = "č";
Decoder.map["?chid=1L?"] = "Č";
Decoder.map["?chid=2?"] = "ď";
Decoder.map["?chid=2L?"] = "Ď";
Decoder.map["?chid=3?"] = "ě";
Decoder.map["?chid=3L?"] = "Ě";
Decoder.map["?chid=4?"] = "é";
Decoder.map["?chid=4L?"] = "É";
Decoder.map["?chid=5?"] = "í";
Decoder.map["?chid=5L?"] = "Í";
Decoder.map["?chid=6?"] = "ň";
Decoder.map["?chid=6L?"] = "Ň";
Decoder.map["?chid=7?"] = "ó";
Decoder.map["?chid=7L?"] = "Ó";
Decoder.map["?chid=8?"] = "ř";
Decoder.map["?chid=8L?"] = "Ř";
Decoder.map["?chid=9?"] = "š";
Decoder.map["?chid=9L?"] = "Š";
Decoder.map["?chid=10?"] = "ť";
Decoder.map["?chid=10L?"] = "Ť";
Decoder.map["?chid=11?"] = "ú";
Decoder.map["?chid=11L?"] = "Ú";
Decoder.map["?chid=12?"] = "ů";
Decoder.map["?chid=12L?"] = "Ů";
Decoder.map["?chid=13?"] = "ž";
Decoder.map["?chid=13L?"] = "Ž";
Decoder.map["?chid=14?"] = "ý";
Decoder.map["?chid=14L?"] = "Ý";

Decoder.decode = function(str) {
	for(var i = 0; i < 10; i++)
		for(var key in Decoder.map)
			str = str.replace(key, Decoder.map[key]);
	return str;
};
