var Main = {};

Main.requestUpdateHeaderTime = function() {
	SHS.query("Main.updateHeaderTime", "gettime");
};

Main.updateHeaderTime = function(result) {
	$("body header span[id='header_time']").html(result);
};

$(document).ready(function() {
	Main.requestUpdateHeaderTime();
	setInterval(function() {
		Main.requestUpdateHeaderTime();
	}, 1000);
});
