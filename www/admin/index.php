<?php
require __DIR__ . '/../../app/admin.php';


	$pages = array(
		"main",
		"control_units", "new_control_unit", "edit_control_unit",
		"scheduled_events", "new_scheduled_event", "edit_scheduled_event",
		"trigger_events", "new_trigger_event", "edit_trigger_event",
		"rooms", "new_room", "edit_room",
		"macros", "new_macro", "edit_macro",
		"controls", "new_control", "edit_control",
		"users", "new_user", "edit_user"
	);
?>

<!DOCTYPE html>
<html>
<head>
	<title><?=$_DICTIONARY["title"]?></title>
	<meta charset="utf-8" />
	<link rel="stylesheet" type="text/css" href="css/reset.css" />
	<link rel="stylesheet" type="text/css" href="css/main.css" />
	<link rel="shortcut icon" href="./favicon.ico" type="image/x-icon">
	<link rel="icon" href="./favicon.ico" type="image/x-icon">
	<meta content='width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<script src="jslib/jquery-2.1.4.min.js"></script>
<?php if(isset($_SESSION["userid"])) :?>
	<script src="jslib/jquery-ui.min.js"></script>
	<script>
		var _DICTIONARY = {};
		<?php foreach($_DICTIONARY as $key => $value) :?>
			_DICTIONARY["<?=$key?>"] = "<?=$value?>";
		<?php endforeach; ?>

		String.prototype.replaceAll = function(search, replacement) {
			var target = this;
			return target.split(search).join(replacement);
		};
	</script>
	<script src="jslib/server.js"></script>
	<script src="jslib/main.js"></script>
	<script>
		SHS.init();
		$(document).ready(function() {
			
			$(document).on("mousemove", function(event) {
				SHS.mouseX = event.pageX;
				SHS.mouseY = event.pageY;
			});
	
			$("img[data-docsid]").mouseenter(function() {
				var docsId = $(this).data("docsid");
				$("div#infoBox").html(SHS.docsTexts[docsId]);
				$("div#infoBox").css("display", "block");
				$("div#infoBox").css("left", (this.x+this.width)+"px");
				$("div#infoBox").css("top", (this.y+this.height)+"px");
			});
	
			$("img[data-docsid]").mouseleave(function() {
				$("div#infoBox").css("display", "none");
			});
			
			$("#in_btn_logout").click(function() {
				$.get("phpscript/logout.php");
				location.reload();
			});
		});
	</script>
<?php endif; ?>
</head>
<body>
<?php if(isset($_SESSION["userid"])) :?>
	<div id="infoBox"></div>
	<header>
		<?php require_once("inc/header.php"); ?>
	</header>
	<nav>
		<ul>
			<li><a href="?"><?=$_DICTIONARY["home"]?></a></li><!--
			--><li><a href="#"><?=$_DICTIONARY["server_conf"]?></a>
				<ul>
					<li style="width: 170px;"><a href="?page=control_units"><?=$_DICTIONARY["control_units"]?></a></li>
				</ul>
			</li><!--
			--><li><a href="#"><?=$_DICTIONARY["event_conf"]?></a>
				<ul>
					<li style="width: 280px;"><a href="?page=scheduled_events"><?=$_DICTIONARY["scheduled_events"]?></a></li><!--
					--><li><a href="?page=trigger_events"><?=$_DICTIONARY["trigger_events"]?></a></li>
				</ul>
			</li><!--
			--><li><a href="#"><?=$_DICTIONARY["web_conf"]?></a>
				<ul>
					<li style="width: 170px;"><a href="?page=macros"><?=$_DICTIONARY["macros"]?></a></li>
					<li><a href="?page=rooms"><?=$_DICTIONARY["rooms"]?></a></li>
					<li><a href="?page=controls"><?=$_DICTIONARY["controls"]?></a></li>
					<li><a href="?page=users"><?=$_DICTIONARY["users"]?></a></li>
				</ul>
			</li><!--
			--><li><a href="#" id="in_btn_logout"><?=$_DICTIONARY["logout"]?></a></li>
		</ul>
	</nav>
	<main>
		<?php
			$_page = $httpRequest->getQuery('page', 'main');
			if(in_array($_page, $pages, true)){
				require 'section/'.$_page.'.php';
			}
		?>
	</main>
	<footer>
		<?php require_once("inc/footer.php"); ?>
	</footer>
<?php else :?>
	<div class="login-form">
		<table class="login-table">
			<tr>
				<td><input type="text" class="login-username" id="in_username" placeholder="<?=$_DICTIONARY["username"]?>" /></td>
				<td rowspan="2" class="login-btn-tr"><img src="res/img/key.png" class="login-btn" id="in_btn_login" /></td>
			</tr>
			<tr>
				<td><input type="password" class="login-passwd" id="in_passwd" placeholder="<?=$_DICTIONARY["password"]?>" /></td>
			</tr>
		</table>
		<script>
			function login_func() {
				$.post("phpscript/login.php", {
					username: $("#in_username").val(),
					password: $("#in_passwd").val()
				}).done(function(data) {
					if(data == "ERR")
						alert("<?=$_DICTIONARY["login_fail"]?>");
					else if(data == "OK")
						location.reload();
				});
			}

			$("#in_btn_login").click(login_func);
			$(document).keypress(function(e) {
				if(e.which == 13)
					login_func();
			});
		</script>
	</div>
<?php endif; ?>
</body>
</html>
